Unit formAttendance;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ResizeKit, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDSimplePanel, Grids, ExtCtrls,
  StdCtrls, Mask, ActnList, ActnMan, ImgList, LibData, LibForms, LibUtil,
  LMDControl, LMDBaseControl, LMDBaseGraphicControl, LMDGraphicControl,
  LMDCustomGraphicLabel, LMDGraphicLabel, db33, {InspCtrl, CompInsp,}
  {PropIntf,} Buttons, LMDCustomComponent, LMDCustomHint, LMDHint, LMDCustomButton, LMDButton,
  XPStyleActnCtrls, STSTRS, vListBox, CSDef, dBase, YesNoBoxu, Util1, Util2, PVIO,
  ClubFees, TxtView1, mtables, ovcbase, ovcef, ovcsf,hh;

Type
  _DateString = String[8];

Type
  TfrmAttendance = Class(TForm)
    ResizeKit1: TResizeKit;
    StatusBar1: TStatusBar;
    LMDSimplePanel1: TLMDSimplePanel;
    LMDSimplePanel2: TLMDSimplePanel;
    sgEvents: TStringGrid;
    Label1: TLabel;
    MELastName: TMaskEdit;
    Label2: TLabel;
    MEFirstName: TMaskEdit;
    Shape2: TShape;
    Label3: TLabel;
    MEPlayerNo: TMaskEdit;
    Shape1: TShape;
    Shape3: TShape;
    Label4: TLabel;
    MEEventLink: TMaskEdit;
    Shape4: TShape;
    MERecordNumber: TMaskEdit;
    Label5: TLabel;
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    Button8: TButton;
    StaticText9: TStaticText;
    MEShowLink: TMaskEdit;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F4Key: TAction;
    F5Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    ImageList1: TImageList;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    ActionManager2: TActionManager;
    ButAttendDone: TAction;
    LMDGraphicLabel1: TLMDGraphicLabel;
    StatusBar2: TStatusBar;
    LMDHint1: TLMDHint;
    LMDButAttendDone: TLMDButton;
    Action1: TAction;
    Action2: TAction;
    OSFEventSessNo: TOvcSimpleField;
    OSFSection: TOvcSimpleField;
    OSFColorNo: TOvcSimpleField;
    LabelColorText: TLabel;
    LabelTypeText: TLabel;
    OSFTypeNo: TOvcSimpleField;
    OSFMPs: TOvcSimpleField;
    OSFPercent: TOvcSimpleField;
    OSFStratum: TOvcSimpleField;
    OSFDirection: TOvcSimpleField;
    LabelDirectionText: TLabel;
    OSFPairNo: TOvcSimpleField;
    OSFRankLow: TOvcSimpleField;
    OSFRankHigh: TOvcSimpleField;
    OSFBoards: TOvcSimpleField;
    Procedure FormActivate(Sender: TObject);
    Procedure pFillEvents;
    Procedure pClearRecords;
    Procedure pColHeadings;
    Procedure pFillFields;
    Function fGetGameDescript(Const sLink: String): String;
    Procedure pGetAttendRecord;
    Procedure pGetNextAttendRec;
    Procedure pMoveToRecord;
    Procedure sgEventsKeyPress(Sender: TObject; Var Key: Char);
    Procedure sgEventsClick(Sender: TObject);
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure F4KeyExecute(Sender: TObject);
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure pMaskEditOnEnter(Sender: TObject);
    Procedure pMaskEditOnExit(Sender: TObject);
    Procedure ButAttendDoneExecute(Sender: TObject);
    Procedure pSaveAttend;
    Procedure pAbortAttendChanges;
    Procedure pUpdateAttendStatusBar;
    Procedure pCheckAttendEditFields;
    Procedure pCopyAttendRecData;
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    Function fCheckForChanges: Boolean;
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure sgEventsDblClick(Sender: TObject);
    Function fPickTournEvent: String;
    Procedure pClearMEFields;
    Procedure pFillMEFields;
    Procedure pAttendGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pAttendGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure FormKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pPrintAttend(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    Procedure pMEFieldAvail(bAvail: Boolean; oOSField: TMaskEdit);
    Procedure pResetFldAttrib;
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure F5KeyExecute(Sender: TObject);
    Procedure Button8Click(Sender: TObject);
    Procedure pSetColsWidth(sgName: tStringGrid);
    Procedure OSFEventSessNoExit(Sender: TObject);
    Procedure OSFEventSessNoEnter(Sender: TObject);
    Procedure OSFSectionEnter(Sender: TObject);
    Procedure OSFSectionExit(Sender: TObject);
    Procedure OSFColorNoEnter(Sender: TObject);
    Procedure OSFColorNoExit(Sender: TObject);
    Procedure OSFColorNoChange(Sender: TObject);
    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    Procedure OSFTypeNoChange(Sender: TObject);
    Procedure OSFTypeNoEnter(Sender: TObject);
    Procedure OSFTypeNoExit(Sender: TObject);
    Procedure OSFMPsEnter(Sender: TObject);
    Procedure OSFMPsExit(Sender: TObject);
    Procedure OSFPercentEnter(Sender: TObject);
    Procedure OSFPercentExit(Sender: TObject);
    Procedure OSFStratumEnter(Sender: TObject);
    Procedure OSFStratumExit(Sender: TObject);
    Procedure OSFDirectionEnter(Sender: TObject);
    Procedure OSFDirectionExit(Sender: TObject);
    Procedure OSFPairNoEnter(Sender: TObject);
    Procedure OSFPairNoExit(Sender: TObject);
    Procedure OSFRankLowEnter(Sender: TObject);
    Procedure OSFRankLowExit(Sender: TObject);
    Procedure OSFRankHighEnter(Sender: TObject);
    Procedure OSFRankHighExit(Sender: TObject);
    Procedure OSFBoardsEnter(Sender: TObject);
    Procedure OSFBoardsExit(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
  private
    { Private declarations }
  public
    { Public declarations }
    ksTRecNoKey, ksTTempKey: KeyStr; // Tournaments (Tourn.dat)
    sPickEvent: String;
    iStartDate: tDateTime;
    iEndDate: tDateTime;
  End;

Var
  frmAttendance: TfrmAttendance;
  iRow: integer; // String Grid Row Number
  iChildRecs: Integer;
  bIsLinked: Boolean;

Const
  SideMFact = 0.65; {M factor for side games}
  ContMFact = 0.80; {M factor for side game series}

Const
  mp_types = 2;
  mp_type_text: Array[0..mp_types] Of PChar =
  {ranking type text}
  ('None',
    'Sess/Match',
    'Overall');

Const
  MaxViewLines = 25;
  MaxViewBufLen = $FFC0;
  UseViewBufLen: LongInt = MaxViewBufLen;
  HorizScrollAmount = 10;
  AnyKeyPrompt = ' Press Any Key ';
  AnyExit: Boolean = false;
  ViewCentered: Boolean = false;
  ViewOverFlow: boolean = false; {true=buffer overflow}

Implementation

Uses formPickEvent, formPlayers, formGetDateRange, formPercentages,
  formOARank;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$I STSTRS.dcl}
{$I Showpts.inc}
{$I addform.inc}
{$I tdrank.inc}
{$I fixdb.inc}
{$I DFNDFORM.INC}
{$I i_update.inc}

Procedure PleaseWait;
Begin
  PlsWait;
End;

{$R *.dfm}

Var
  ksERecNoKey, ksETempKey: keystr; // Tournament Events (TournEv.dat)
  //ksTRecNoKey, ksTTempKey: keystr; // Tournaments (Tourn.dat)
  ksARecNoKey, ksATempKey: keystr; // Attendance Records (Attend.dat)
  aOK: Boolean;

Procedure TfrmAttendance.FormActivate(Sender: TObject);
Begin
  Filno := Attend_no;
  { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
  UseDBLinkages := True;
  Scrno := 2;
  HelpContext := 207;
  // ---------------------------------------------------------------------
  LMDGraphicLabel1.Caption := '';
  If CSDef.CFG.Tournament Then Begin
    StatusBar1.Panels[0].Text := 'Tournament';
    Button2.Visible := True;
    Button2.Enabled := True;
    StaticText2.Visible := True;
  End
  Else Begin
    StatusBar1.Panels[0].Text := 'Club';
    ButAdd.Enabled := False;
    ButAdd.Visible := False;
    ButCopy.Enabled := False;
    ButCopy.Visible := False;
    ButEdit.Enabled := False;
    ButEdit.Visible := False;
    Label4.Caption := 'Game Link';
    Button2.Visible := False;
    StaticText2.Visible := False;
  End;
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is made to a "Record",
    dbase re-writes the table header and in Playern.dat, updates dbLastImpDate entry. Set them to
    False when you Delete a record, then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  { Check to make sure the drive letter is used in the Prepend variable. If it is not, the program
    will use the drive that the executable is running from! }
  //If Copy(Prepend, 1, 1) = '\' Then Prepend := 'C:' + Prepend;
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'ATTEND.DAT');
  { Store the number of records in PLAYERN.DAT to iReccount }
  iChildRecs := db33.UsedRecs(DatF[Attend_no]^);
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iChildRecs)) + ' Records.';
  pFillFields;
  { Fill the StringGrids (sgEvents) with the Tournament Events for this Player }
  pFillEvents;
  // =======================================================================
  StatusBar2.Panels[0].Text := IntToStr(sgEvents.RowCount - 1) + ' Events';
End;

Procedure TfrmAttendance.pFillEvents;
Begin
  aOK := False;
  bIsLinked := False;
  { Fill the StringGrids (sgEvents) with the Tournament Events for this Player }
  { pClearGroups: Clears the entries in the StringGrid SGGroups }
  pClearRecords;
  { Clear entries in all MaskEdit Fields }
  pClearMEFields;
  // ---------------------------------------------------------------------------
  { Store the current records data to the string grid }
  iRow := 0;

  //==============================================================================================
  // A T T E N D A N C E   R E C O R D S
  ksARecNoKey := GetKey(Playern_no, 3); { Key 3 = RECORD_NO }
  ksATempKey := ksARecNoKey;
  { Search for records in the Attendance table that match the Player Number key of the current
    Player Number record }
  //ClearKey(IdxKey[Attend_no, 1]^);
  SearchKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksATempKey);
  //FindKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksATempKey);
  aOK := (OK And CompareKey(ksATempKey, ksARecNoKey));
  //==============================================================================================

  pColHeadings;
  If aOK Then Begin
    //==============================================================================================
    // T O U R N A M E N T   E V E N T S
    ksERecNoKey := GetKey(Attend_no, 3); { Key 3 = HASH+RECORD_NO+SESSION_NO }
    ksETempKey := ksERecNoKey;
    { Key 3 = HASH }
    SearchKey(IdxKey[frmPlayers.iPlayEvent, 3]^, Recno[frmPlayers.iPlayEvent], ksETempKey);
    //==============================================================================================

    If ok And CompareKey(ksETempKey, ksERecNoKey) Then Begin
      // Increment the Row Count for the next available Row in the string grid
      iRow := iRow + 1;
      If sgEvents.RowCount < iRow + 1 Then sgEvents.RowCount := sgEvents.RowCount + 1;
      bIsLinked := True;
    End;

    { While the Player Number Key matches, store this players groups to the string grid for viewing }
    {
      0  Slash(TOURNEV.EVENT_DATE)
      1  TOURNEV.SANCTION_NO
      2  ATTEND.SESSION_NO
      3  ATTEND.MPS
      4  fGetMPColor(ATTEND.MP_COLOR)
      5  ATTEND.PERCENT
      6  TOURNEV.EVENT_CODE
      7  TOURNEV.EVENT_NAME
    }
    While ok And CompareKey(ksETempKey, ksERecNoKey) Do Begin
      GetARec(frmPlayers.iPlayEvent); // Store the record
      If frmPlayers.sPlayMode = 'T' Then Begin
        { Tournament Mode }
        sgEvents.Cells[0, iRow] := Slash(TOURNEV.EVENT_DATE);
        sgEvents.Cells[1, iRow] := TOURNEV.SANCTION_NO;
        sgEvents.Cells[2, iRow] := ATTEND.SESSION_NO;
        sgEvents.Cells[3, iRow] := ATTEND.MPS;
        sgEvents.Cells[4, iRow] := LibData.fGetMPColor(ATTEND.MP_COLOR);
        sgEvents.Cells[5, iRow] := ATTEND.PERCENT;
        sgEvents.Cells[6, iRow] := TOURNEV.EVENT_CODE;
        sgEvents.Cells[7, iRow] := TOURNEV.EVENT_NAME;
        sgEvents.Cells[8, iRow] := Key2Str(ATTEND.HASH, 4);
      End
      Else Begin
        { Club Mode }
        sgEvents.Cells[0, iRow] := Slash(CLUBGAME.EVENT_DATE);
        sgEvents.Cells[1, iRow] := CLUBGAME.CLUB_NO;
        sgEvents.Cells[2, iRow] := CLUBGAME.CLUB_SESS_NO;
        sgEvents.Cells[3, iRow] := ATTEND.MPS;
        sgEvents.Cells[4, iRow] := ATTEND.PERCENT;
        sgEvents.Cells[5, iRow] := ATTEND.SECTION;
        sgEvents.Cells[6, iRow] := CLUBGAME.EVENT_NCODE;
        sgEvents.Cells[7, iRow] := CLUBGAME.EVENT_NAME;
        sgEvents.Cells[8, iRow] := Key2Str(ATTEND.HASH, 4);
      End;

      // Go to the next record in the table
      pGetNextAttendRec;
      If (aOK) And (CompareKey(ksATempKey, ksARecNoKey)) Then Begin
        GetARec(Attend_no); // Store the record
        ksERecNoKey := GetKey(Attend_no, 3);
        ksETempKey := ksERecNoKey;
        { Search for records in the Attendance table that match the Player Number key of the current
          Player Number record }
        db33.SearchKey(IdxKey[frmPlayers.iPlayEvent, 3]^, Recno[frmPlayers.iPlayEvent], ksETempKey);
        If ok And CompareKey(ksETempKey, ksERecNoKey) Then Begin
          // Increment the Row Count for the next available Row in the string grid
          iRow := iRow + 1;
          If sgEvents.RowCount < iRow + 1 Then sgEvents.RowCount := sgEvents.RowCount + 1;
        End;
      End
      Else Begin
        //==============================================================================================
        // A T T E N D A N C E   R E C O R D S
        { Need to place record pointer back on the first Attendance record. Move String Grid back
          to row 1 also. }
        { Reset ksATempKey back to the intial value to go to the first linked record for the
          child record }
        ksATempKey := ksARecNoKey;
        db33.SearchKey(IdxKey[Attend_no, 3]^, Recno[Attend_no], ksATempKey);
        GetARec(Attend_no); // Store the record
        //==============================================================================================
        //==============================================================================================
        // T O U R N A M E N T   E V E N T S
        ksERecNoKey := GetKey(Attend_no, 3);
        ksETempKey := ksERecNoKey;
        { Search for records in the Attendance table that match the Player Number key of the current
          Player Number record }
        db33.SearchKey(IdxKey[frmPlayers.iPlayEvent, 3]^, Recno[frmPlayers.iPlayEvent], ksETempKey);
        GetARec(frmPlayers.iPlayEvent); // Store the record
        //==============================================================================================
        Break;
      End;
    End;
    pSetColsWidth(sgEvents);
  End
  Else MEShowLink.Text := 'No Link';
  //==============================================================================================
  // T O U R N A M E N T S   R E C O R D S
  pButtOn(bIsLinked);
  If bIsLinked Then sgEvents.Row := 1;
  pGetLinkedRecord(ksTRecNoKey, ksTTempKey, frmPlayers.iPlayEvent, 1, frmPlayers.iSanctions, 1);
  //==============================================================================================
End;

Procedure TfrmAttendance.pGetNextAttendRec;
Begin
  NextKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksATempKey);
  aOK := OK;
  OK := True;
End;

Procedure TfrmAttendance.pClearRecords;
Var
  iForLoop: integer;
Begin
  { Clear out all previous entries in the String Grid for Groups in preparation
    for any entries from the current player record }
  For iForLoop := 0 To (sgEvents.RowCount - 1) Do Begin
    sgEvents.Rows[iForLoop].Clear;
  End;
  sgEvents.RowCount := 1;
End;

Procedure TfrmAttendance.pColHeadings;
Begin
  sgEvents.Cells[0, iRow] := 'DATE';
  If CSDef.CFG.Tournament Then Begin
    sgEvents.Cells[1, iRow] := 'SANCTION NO';
  End
  Else Begin
    sgEvents.Cells[1, iRow] := 'CLUB NO';
  End;
  sgEvents.Cells[2, iRow] := 'SESSION';
  If CSDef.CFG.Tournament Then Begin
    sgEvents.Cells[3, iRow] := 'MPs';
    sgEvents.Cells[4, iRow] := 'COLOR';
    sgEvents.Cells[5, iRow] := 'PERCENT';
    sgEvents.Cells[6, iRow] := 'CODE';
  End
  Else Begin
    sgEvents.Cells[3, iRow] := 'MPs';
    sgEvents.Cells[4, iRow] := 'PERCENT';
    sgEvents.Cells[5, iRow] := 'SECTION';
    sgEvents.Cells[6, iRow] := '';
  End;
  sgEvents.Cells[7, iRow] := 'NAME';
End;

Procedure TfrmAttendance.pFillFields;
Begin
  { Load Player information to the appropriate MaskEdit field from the current record }
  MELastName.Text := Playern.LAST_NAME;
  MEFirstName.Text := Playern.FIRST_NAME;
  MEPlayerNo.Text := Playern.PLAYER_NO;
  pGetAttendRecord;
  MERecordNumber.Text := IntToStr(Key2Num(PLAYERN.RECORD_NO));
  If UpperCase(MEEventLink.Text) = 'T' Then
    MEShowLink.Text := fGetTournDescript(ATTEND.HASH)
  Else If UpperCase(MEEventLink.Text) = 'C' Then
    MEShowLink.Text := fGetGameDescript(ATTEND.HASH);
  OSFEventSessNo.Text := ATTEND.SESSION_NO;
  OSFSection.Text := Trim(ATTEND.SECTION);
  OSFColorNo.Text := ATTEND.MP_COLOR;
  LabelColorText.Caption := LibData.fGetFullMPColor(ATTEND.MP_COLOR);
  OSFTypeNo.Text := ATTEND.MP_Type;
  LabelTypeText.Caption := LibData.fGetType(ATTEND.MP_Type);
  OSFMPs.Text := ATTEND.MPS;
  OSFPercent.Text := ATTEND.PERCENT;
  OSFStratum.Text := ATTEND.Stratum;
  OSFDirection.Text := ATTEND.Direction;
  LabelDirectionText.Caption := LibData.fShowdir(ATTEND.Direction);
  OSFPairNo.Text := ATTEND.Pair_no;
  OSFRankLow.Text := ATTEND.rank_low;
  OSFRankHigh.Text := ATTEND.rank_high;
  OSFBoards.Text := ATTEND.boards;
End;

Function TfrmAttendance.fGetGameDescript(Const sLink: String): String;
Var
  ksRNKey, ksTKey: keystr;
Begin
  ksRNKey := GetKey(Attend_no, 3);
  ksTKey := ksRNKey;
  db33.SearchKey(IdxKey[CLUBGAME_no, 3]^, Recno[CLUBGAME_no], ksTKey);
  ok := ok And CompareKey(ksTKey, ksRNKey);
  If ok Then Begin
    { Must call GetARec to display the record if it is found }
    GetARec(frmPlayers.iPlayEvent);
    { Return the Event Information }
    result := CLUBGAME.EVENT_NAME;
  End
  Else Result := 'No Link';
End;

Procedure TfrmAttendance.pGetAttendRecord;
Var
  ksXRecNoKey, ksXTempKey: keystr;
Begin
  ksXRecNoKey := GetKey(Playern_no, 3);
  ksXTempKey := ksXRecNoKey;
  { Search for records in the Attendance table that match the Player Number key of the current
    Player Number record }
  SearchKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksXTempKey);

  { LINK_FILE - Will contain either a "T" for Tournament game or a "C" for a Club Game.
    If it is "T" the tables Tourn.dat and Tournev.dat are used for Tournament game information,
    If it is "C" the tables ClubGame.dat and ClubSanc.dat are used for Club game information. }
  If (ok) And (CompareKey(ksXTempKey, ksXRecNoKey)) Then Begin
    GetARec(Attend_no); // Store the record
    MEEventLink.Text := ATTEND.LINK_FILE;
  End
  Else
    MEEventLink.Text := '';
End;

Procedure TfrmAttendance.pMoveToRecord;
Var
  iMove, iCurrRow: Integer;
Begin
  {
    If the grid row changes, then use the stored "HASH" field in the string grid and the
    "RECORD NUMBER" field from the main player table to find the correct Attendance and
    Tournament Event record to display details on.

    In Tournev_no use key 3, HASH
    In Attend_no use key 1, Record_no+Hash+Session_no
  }
  ClearKey(IdxKey[Attend_no, 1]^);
  //==============================================================================================
  // A T T E N D A N C E   R E C O R D S
  iCurrRow := sgEvents.Row;
  ksARecNoKey := GetKey(Playern_no, 3);
  ksATempKey := ksARecNoKey;
  { Search for records in the Attendance table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksATempKey);
  iMove := 1;
  If iCurrRow > 0 Then Begin
    While iMove < sgEvents.Row Do Begin
      pGetNextAttendRec;
      Inc(iMove);
    End;
    If (aOK) And (CompareKey(ksATempKey, ksARecNoKey)) Then Begin
      ClearKey(IdxKey[frmPlayers.iPlayEvent, 3]^);
      GetARec(Attend_no); // Store the record
      ksERecNoKey := GetKey(Attend_no, 3);
      ksETempKey := ksERecNoKey;
      { Search for records in the Attendance table that match the Player Number key of the current
        Player Number record }
      db33.SearchKey(IdxKey[frmPlayers.iPlayEvent, 3]^, Recno[frmPlayers.iPlayEvent], ksETempKey);
      If ok And CompareKey(ksETempKey, ksERecNoKey) Then
        GetARec(frmPlayers.iPlayEvent); // Store the record
      { Load Tournment Player information to the appropriate MaskEdit field from the current record }
      MEEventLink.Text := ATTEND.LINK_FILE;
      If frmPlayers.sPlayMode = 'T' Then
        MEShowLink.Text := Slash(TOURNEV.EVENT_DATE) + '  ' + TOURNEV.SANCTION_NO +
          '  ' + ATTEND.SESSION_NO + '  ' + ATTEND.MPS +
          LibData.fGetMPColor(ATTEND.MP_COLOR) + '  ' + ATTEND.PERCENT + '  ' +
          TOURNEV.EVENT_CODE + '  ' + TOURNEV.EVENT_NAME
      Else
        MEShowLink.Text := Slash(CLUBGAME.EVENT_DATE) + '  ' + CLUBGAME.CLUB_NO +
          '  ' + CLUBGAME.CLUB_SESS_NO + '  ' + ATTEND.MPS +
          ATTEND.SECTION + '  ' + ATTEND.PERCENT + '  ' +
          CLUBGAME.EVENT_NCODE + '  ' + CLUBGAME.EVENT_NAME;
      OSFEventSessNo.Text := ATTEND.SESSION_NO;
      OSFSection.Text := ATTEND.SECTION;
      OSFColorNo.Text := ATTEND.MP_COLOR;
      LabelColorText.Caption := LibData.fGetFullMPColor(ATTEND.MP_COLOR);
      OSFTypeNo.Text := ATTEND.MP_Type;
      LabelTypeText.Caption := LibData.fGetType(ATTEND.MP_Type);
      OSFMPs.Text := ATTEND.MPS;
      OSFPercent.Text := ATTEND.PERCENT;
      OSFStratum.Text := ATTEND.Stratum;
      OSFDirection.Text := ATTEND.Direction;
      LabelDirectionText.Caption := fShowdir(ATTEND.Direction);
      OSFPairNo.Text := ATTEND.Pair_no;
      OSFRankLow.Text := ATTEND.rank_low;
      OSFRankHigh.Text := ATTEND.rank_high;
      OSFBoards.Text := ATTEND.boards;
    End;
  End;
  //==============================================================================================
  //==============================================================================================
  // T O U R N A M E N T S   R E C O R D S
  LibData.pGetLinkedRecord(ksTRecNoKey, ksTTempKey, frmPlayers.iPlayEvent, 1,
    frmPlayers.iSanctions, 1);
  //==============================================================================================
End;

Procedure TfrmAttendance.sgEventsKeyPress(Sender: TObject; Var Key: Char);
Begin
  pMoveToRecord;
End;

Procedure TfrmAttendance.sgEventsClick(Sender: TObject);
Begin
  pMoveToRecord;
End;

Procedure TfrmAttendance.ButNextActionExecute(Sender: TObject);
Begin
  If sgEvents.Row <> (sgEvents.RowCount - 1) Then sgEvents.Row := sgEvents.Row + 1;
  pMoveToRecord;
End;

Procedure TfrmAttendance.ButPrevActionExecute(Sender: TObject);
Begin
  If sgEvents.Row > 1 Then sgEvents.Row := sgEvents.Row - 1;
  pMoveToRecord;
End;

Procedure TfrmAttendance.ButTopActionExecute(Sender: TObject);
Begin
  If sgEvents.Row > 0 Then sgEvents.Row := 1
  Else
    MessageDlg('There are no Events for this player....', mtInformation, [mbOK], 0);
End;

Procedure TfrmAttendance.ButLastActionExecute(Sender: TObject);
Begin
  If sgEvents.Row > 0 Then sgEvents.Row := (sgEvents.RowCount - 1)
  Else
    MessageDlg('There are no Events for this player....', mtInformation, [mbOK], 0);
End;

Procedure TfrmAttendance.F1KeyExecute(Sender: TObject);
Begin
  //Application.HelpCommand(HELP_CONTENTS, 0);
  pRunExtProg(frmAttendance, cfg.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
End;

Procedure TfrmAttendance.F4KeyExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmAttendance.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmAttendance.ButEditActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  If frmPlayers.sPlayMode = 'T' Then Begin
    { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
      editing so the application will know if a KEY Field has been changed and Indexes need to be
      regenerated }
    PreEditSaveKeys;
    LMDGraphicLabel1.Caption := 'Edit Mode';
    // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
    iDataMode := 2;
    sDataMode := 'E';
    // Save the forms Position and Size coordinates so they can be restored when editing is complete
    frmPlayers.pSaveFormInfo;
    //frmAttendance.BorderStyle := bsNone;
    ActionManager1.State := asSuspended;
    ActionManager2.State := asNormal;
    GBMenu.Enabled := False;
    GBMenu.Hide;
    //AutoSize := True;
    LMDSimplePanel1.Enabled := True;
    OSFEventSessNo.SetFocus;
    // Turn the Done Button on and make it visible
    pButState(LMDButAttendDone, True);
  End
  Else
    MessageDlg('Changes to a player''s attendance record cannot be made here.' + #13 + #10 +
      'Make changes in the game file and DBADD again, or press F6' + #13 + #10 +
      'to report previously unrecorded masterpoints for this player.', mtInformation, [mbOK], 0);
End;

Procedure TfrmAttendance.pMaskEditOnEnter(Sender: TObject);
Begin
  (Sender As tMaskEdit).Color := clHighlight;
  (Sender As tMaskEdit).Font.Color := clHighlightText;
End;

Procedure TfrmAttendance.pMaskEditOnExit(Sender: TObject);
Begin
  (Sender As tMaskEdit).Color := clWindow;
  (Sender As tMaskEdit).Font.Color := clWindowText;
End;

Procedure TfrmAttendance.ButAttendDoneExecute(Sender: TObject);
Begin
  { If one of MaskEdit Fields data has changed then ask the user if they want to save the changes }
  If fCheckForChanges Then pSaveAttend;
  { Disable the Panel that contains all the MaskEdit fields, this by default disables the
    MaskEdit fields }
  LMDSimplePanel1.Enabled := False;
  { Enable and Show the Main Tool Bar }
  GBMenu.Enabled := True;
  GBMenu.Show;
  sgEvents.SetFocus;
  { Turn on the Action Manager for the Main Tool Bar }
  ActionManager1.State := asNormal;
  { Turn off the Action Manager for Edit/Add/Copy functions }
  ActionManager2.State := asSuspended;
  {  Set the form back to its original size }
  frmPlayers.pSetFormBack;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  bEscKeyUsed := False;
  { Turn the Done Button on and make it visible }
  pButState(LMDButAttendDone, False);
  { Set the Graphic Label to blank }
  LMDGraphicLabel1.Caption := '';
  { Fill the StringGrids (sgEvents) with the Tournament Events for this Player }
  pFillEvents;
End;

Procedure TfrmAttendance.pSaveAttend;
Begin
  // Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyAttendRecData;
        Add_Record;
      End;
    2: Begin // Edit Mode
        pCheckAttendEditFields;
        frmPlayers.bKeyIsUnique := PostEditFixKeys;
        If Not frmPlayers.bKeyIsUnique Then Begin
          If (MessageDlg('This record already exists in this database.', mtError, [mbOK], 0) = mrOK)
            Then Begin
            OSFEventSessNo.SetFocus;
            Exit;
          End
          Else pAbortAttendChanges;
        End
        Else PutARec(Attend_no); { If everything is ok, then save the record }
      End;
  End;
  pUpdateAttendStatusBar;
  // =======================================================================
  StatusBar2.Panels[0].Text := IntToStr(sgEvents.RowCount - 1) + ' Events';
  pResetFldAttrib;
End;

Procedure TfrmAttendance.pAbortAttendChanges;
Begin
  { Disable the Panel that contains all the MaskEdit fields, this by default disables the
    MaskEdit fields }
  LMDSimplePanel1.Enabled := False;
  { Enable and Show the Main Tool Bar }
  GBMenu.Enabled := True;
  GBMenu.Show;
  sgEvents.SetFocus;
  { Turn on the Action Manager for the Main Tool Bar }
  ActionManager1.State := asNormal;
  { Turn off the Action Manager for Edit/Add/Copy functions }
  ActionManager2.State := asSuspended;
  {  Set the form back to its original size }
  frmPlayers.pSetFormBack;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  bEscKeyUsed := False;
  { Turn the Done Button on and make it visible }
  pButState(LMDButAttendDone, False);
  { Set the Graphic Label to blank }
  LMDGraphicLabel1.Caption := '';
  { Fill the StringGrids (sgEvents) with the Tournament Events for this Player }
  pFillEvents;
  pResetFldAttrib;
End;

Procedure TfrmAttendance.pUpdateAttendStatusBar;
Begin
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'ATTEND.DAT');
  { Store the number of records in PLAYERN.DAT to iReccount }
  iChildRecs := db33.UsedRecs(DatF[Attend_no]^);
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iChildRecs)) + ' Records.';
End;

Procedure TfrmAttendance.pCheckAttendEditFields;
Begin
  With Attend Do Begin
    SESSION_NO := Pad(OSFEventSessNo.Text,1);
    SECTION := Pad(OSFSection.Text,2);
    MP_COLOR := Pad(OSFColorNo.Text,1);
    MP_TYPE := Pad(OSFTypeNo.Text,1);
    MPS := LeftPad(Trim(OSFMPs.Text),6);
    PERCENT := LeftPad(Trim(OSFPercent.Text),5);
    Stratum := Pad(OSFStratum.Text,1);
    Direction := Pad(OSFDirection.Text,1);
    Pair_no := LeftPad(Trim(OSFPairNo.Text),3);
    rank_low := LeftPad(Trim(OSFRankLow.Text),3);
    rank_high := LeftPad(Trim(OSFRankHigh.Text),3);
    boards := LeftPad(Trim(OSFBoards.Text),3);
  End;
End;

Procedure TfrmAttendance.pCopyAttendRecData;
Begin
  { Need Hash and other fields for Added and Copied records }
  With Attend Do Begin
    SESSION_NO := Pad(OSFEventSessNo.Text,1);
    SECTION := Pad(OSFSection.Text,2);
    MP_COLOR := Pad(OSFColorNo.Text,1);
    MP_TYPE := Pad(OSFTypeNo.Text,1);
    MPS := LeftPad(Trim(OSFMPs.Text),6);
    PERCENT := LeftPad(Trim(OSFPercent.Text),5);
    Stratum := Pad(OSFStratum.Text,1);
    Direction := Pad(OSFDirection.Text,1);
    Pair_no := LeftPad(Trim(OSFPairNo.Text),3);
    rank_low := LeftPad(Trim(OSFRankLow.Text),3);
    rank_high := LeftPad(Trim(OSFRankHigh.Text),3);
    boards := LeftPad(Trim(OSFBoards.Text),3);
  End;
End;

Procedure TfrmAttendance.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  If iDataMode <> 0 Then Begin
    ButAttendDoneExecute(LMDButAttendDone);
    iDataMode := 0;
    sDataMode := Null;
    bEscKeyUsed := False;
  End;
End;

Function TfrmAttendance.fCheckForChanges: Boolean;
Begin
  { Check to see if any of the Mask Edit Field Contents has been changed so we know to ask the
    user if the changes should be saved or not. RESULT DEFAULTS TO FALSE!!!!! }
  With Attend Do Begin
    Result := (trim(OSFEventSessNo.Text) <> Trim(SESSION_NO));
    If Not Result Then Result := (trim(OSFSection.Text) <> Trim(SECTION));
    If Not Result Then Result := (trim(OSFColorNo.Text) <> Trim(MP_COLOR));
    If Not Result Then Result := (trim(OSFTypeNo.Text) <> Trim(MP_TYPE));
    If Not Result Then Result := (trim(OSFMPs.Text) <> Trim(MPS));
    If Not Result Then Result := (trim(OSFPercent.Text) <> Trim(PERCENT));
    If Not Result Then Result := (trim(OSFStratum.Text) <> Trim(Stratum));
    If Not Result Then Result := (trim(OSFDirection.Text) <> Trim(Direction));
    If Not Result Then Result := (trim(OSFPairNo.Text) <> Trim(Pair_no));
    If Not Result Then Result := (trim(OSFRankLow.Text) <> Trim(rank_low));
    If Not Result Then Result := (trim(OSFRankHigh.Text) <> Trim(rank_high));
    If Not Result Then Result := (trim(OSFBoards.Text) <> Trim(boards));
  End;
End;

Procedure TfrmAttendance.ButAddActionExecute(Sender: TObject);
//***************************************************************************************//
{ THESE VARIABLES ARE IS ONLY USED IN DEVELOPMENT MODE!!!!!!!!! }
Var
  ksKey: KeyStr;
  {iFileHandle, iStringLen: Integer;
  sFileName: String;}
//***************************************************************************************//
Begin
  //***************************************************************************************//
  { THIS SECTION OF CODE IS ONLY USED IN DEVELOPMENT MODE!!!!!!!!! }
  //sFileName := CFG.DBpath + SancDigits(Trim(TOURNEV.SANCTION_NO)) + '.REP';
  { Check to make sure the drive letter is used in the variable. If it is not, the program
    will use the drive that the executable is running from! }
  {If Copy(sFileName, 1, 1) = '\' Then sFileName := 'C:' + sFileName;
  If Not fileexists(sFileName) Then Begin
    iFileHandle := SysUtils.FileCreate(sFileName);
    iStringLen := 80;
    SysUtils.FileWrite(iFileHandle, iStringLen, SizeOf(iStringLen));
    SysUtils.FileClose(iFileHandle);
  End;}
  //***************************************************************************************//
  bEscKeyUsed := False;
  If frmPlayers.sPlayMode = 'T' Then Begin
    If fOktoModify('A', frmPlayers.iPlayEvent, Self) Then Begin
      fPickTournEvent;
      If (sPickEvent) <> '' Then Begin
        { Clear entries in all MaskEdit Fields }
        pClearMEFields;
        InitRecord(Filno);
        { Store entries in MaskEdit Fields }
        pFillMEFields;
        { Set the label caption for mode we are in }
        LMDGraphicLabel1.Caption := 'Add Mode';
        { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
        iDataMode := 1;
        sDataMode := 'A';
        { Save the forms Position and Size coordinates so they can be restored when editing is
          complete }
        frmPlayers.pSaveFormInfo;
        { Turn off the Action Manager for the Main Tool Bar }
        ActionManager1.State := asSuspended;
        { Turn on the Action Manager for Edit/Add/Copy functions }
        ActionManager2.State := asNormal;
        { Disable and Hide the Main Tool Bar }
        GBMenu.Enabled := False;
        GBMenu.Hide;
        { Show the text label for the mode the user selected Edit, Add or Copy }
        LMDSimplePanel1.Enabled := True;
        { Set the cursor focus to the first MaskEdit field Session Number }
        OSFEventSessNo.SetFocus;
        { Turn the Done Button on and make it visible }
        pButState(LMDButAttendDone, True);
        { Refresh the form }
        frmAttendance.Refresh;
      End;
    End;
  End
  Else
    MessageDlg('Changes to a player''s attendance record cannot be made here.' + #13 + #10 +
      'Make changes in the game file and DBADD again, or press F6' + #13 + #10 +
      'to report previously unrecorded masterpoints for this player.', mtInformation, [mbOK], 0);
End;

Function TfrmAttendance.fPickTournEvent: String;
Begin
  { Populate a PickList for the user to select which Tournament Event to use for this record
    Use the form frmPickEvent to display it.}
  frmPickEvent := tfrmPickEvent.Create(Nil);
  frmPickEvent.ShowModal;
  frmPickEvent.Free;
End;

Procedure TfrmAttendance.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  If frmPlayers.sPlayMode = 'T' Then Begin
    LMDGraphicLabel1.Caption := 'Copy Mode';
    { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    iDataMode := 3;
    sDataMode := 'C';
    { Save the forms Position and Size coordinates so they can be restored when editing is complete }
    frmPlayers.pSaveFormInfo;
    { Turn off the Action Manager for the Main Tool Bar }
    ActionManager1.State := asSuspended;
    { Turn on the Action Manager for Edit/Add/Copy functions }
    ActionManager2.State := asNormal;
    { Disable and Hide the Main Tool Bar }
    GBMenu.Enabled := False;
    GBMenu.Hide;
    { Show the text label for the mode the user selected Edit, Add or Copy }
    LMDSimplePanel1.Enabled := True;
    Add_Init;
    pFillMEFields;
    { Set the cursor focus to the first MaskEdit field Session Number }
    OSFEventSessNo.SetFocus;
    { Turn the Done Button on and make it visible }
    pButState(LMDButAttendDone, True);
  End
  Else
    MessageDlg('Changes to a player''s attendance record cannot be made here.' + #13 + #10 +
      'Make changes in the game file and DBADD again, or press F6' + #13 + #10 +
      'to report previously unrecorded masterpoints for this player.', mtInformation, [mbOK], 0);
End;

Procedure TfrmAttendance.ButDeleteActionExecute(Sender: TObject);
Begin
  Delete_Record(True, frmAttendance.HelpContext);
  { Fill the StringGrids (sgEvents) with the Tournament Events for this Player }
  pFillEvents;
  // =======================================================================
  StatusBar2.Panels[0].Text := IntToStr(sgEvents.RowCount - 1) + ' Events';
End;

Procedure TfrmAttendance.sgEventsDblClick(Sender: TObject);
Begin
  ButEditActionExecute(ButEdit);
End;

Procedure TfrmAttendance.pClearMEFields;
Begin
  { Set all the editable MaskEdit Fields to blanks }
  OSFEventSessNo.Text := '';
  OSFSection.Text := '';
  OSFColorNo.Text := '';
  OSFTypeNo.Text := '';
  OSFMPs.Text := '';
  OSFPercent.Text := '';
  OSFStratum.Text := '';
  OSFDirection.Text := '';
  OSFPairNo.Text := '';
  OSFRankLow.Text := '';
  OSFRankHigh.Text := '';
  OSFBoards.Text := '';
End;

Procedure TfrmAttendance.pFillMEFields;
Begin
  { Used by Add Mode, fill all editable MaskEdit Fields with the Tournament Selected by the user
    and the default values for the other fields }
  OSFEventSessNo.Text := Attend.SESSION_NO;
  OSFSection.Text := Trim(Attend.SECTION);
  OSFColorNo.Text := '1';
  OSFTypeNo.Text := '1';
  OSFMPs.Text := Attend.MPS; // '0.00'
  OSFPercent.Text := '0';
  OSFStratum.Text := '1';
  OSFDirection.Text := '1';
  OSFPairNo.Text := '0';
  OSFRankLow.Text := '0';
  OSFRankHigh.Text := '0';
  OSFBoards.Text := '0';
  { If we are in Tournament Mode }
  If UpperCase(MEEventLink.Text) = 'T' Then
    MEShowLink.Text := Slash(TOURNEV.EVENT_DATE) + '  ' + TOURNEV.SANCTION_NO + '  '
      + ATTEND.SESSION_NO + '  ' + ATTEND.MPS + fGetMPColor(ATTEND.MP_COLOR)
      + '  ' + ATTEND.PERCENT + '  ' + TOURNEV.EVENT_CODE + '  ' +
      TOURNEV.EVENT_NAME
  Else If UpperCase(MEEventLink.Text) = 'C' Then { Club Mode }
    MEShowLink.Text := Slash(CLUBGAME.EVENT_DATE) + '  ' + CLUBGAME.CLUB_NO + '  '
      + CLUBGAME.CLUB_SESS_NO + '  ' + ATTEND.MPS + ATTEND.SECTION
      + '  ' + ATTEND.PERCENT + '  ' + CLUBGAME.EVENT_NCODE + '  ' +
      CLUBGAME.EVENT_NAME;
End;

Procedure TfrmAttendance.pAttendGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If key = VK_F1 Then Begin
    If HelpContext > 0 Then Begin
      bF1KeyUsed := True;
    End
    Else bF1KeyUsed := False;
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortAttendChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Begin
    Key := 0;
    bF1KeyUsed := False;
  End;
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    If iDataMode > 0 Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    If iDataMode > 0 Then pSaveAttend;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmAttendance.pAttendGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If key = VK_F1 Then Begin
    If HelpContext > 0 Then Begin
      bF1KeyUsed := True;
    End
    Else bF1KeyUsed := False;
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortAttendChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Begin
    Key := 0;
    bF1KeyUsed := False;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    If iDataMode > 0 Then pSaveAttend;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmAttendance.FormKeyDown(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortAttendChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Key := 0;
End;

Procedure TfrmAttendance.FormKeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortAttendChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Key := 0;
End;

Const
  sStartDate: _DateString = '';
  sEndDate: _DateString = '';

Procedure TfrmAttendance.pPrintAttend(Sender: TObject);
Const
  AttendTextC: Array[1..4] Of Pchar =
  ('1 All Attendance records',
    '2 Only masterpoint records',
    '3 All Attendance records - restrict by date',
    '4 Only masterpoint records - restrict by date');
  AttendTextT: Array[1..2] Of Pchar =
  ('1  Include all Attendance records',
    '2  Include only masterpoint records');
  ColorTab: Array[0..5] Of String[8] = ('None', 'Black', 'Silver',
    'Red', 'Gold', 'Platinum');

  Function fGetDate: boolean;
  Begin
    frmGetDateRange := TfrmGetDateRange.Create(Self);
    frmGetDateRange.ShowModal;
    frmGetDateRange.Free;
    Result := (iStartDate > 0);
  End;

Var
  oldrecno: longint;
  tempkey, savekey: keystr;
  HashKey: keystr;
  tsavekey: keystr;
  last_link: char;
  AllAttend: boolean;
  AttendChoice: word;
  isdate: boolean;
  Validrec: boolean;
  TotMps: real;
  Totattend: word;
  ColorMps: Array[0..5] Of real;
  Color: word;
  sFileName: String;

Label
  ExitPoint,
    date_again;
Begin
  oldrecno := recno[Attend_no];
  tsavekey := getkey(Attend_no, 1);
  tempkey := getkey(Playern_no, 3);
  savekey := tempkey;
  SearchKey(IdxKey[Attend_no, 1]^, recno[Attend_no], tempkey);
  ok := ok And CompareKey(tempkey, savekey);

  If Not ok Then With Playern Do Begin
      ErrBox('No attendance records found for ' + Trim(First_Name) + ' ' +
        Trim(Last_Name), MC, 0);
      exit;
    End;
  If CSDef.CFG.Tournament Then
    AttendChoice := PickChoice(2, 1, 'report type (ESC to exit) ', @AttendTextT,
      MC, false, 0)
  Else
    AttendChoice := PickChoice(4, 1, 'report type (ESC to exit) ', @AttendTextC,
      MC, false, 0);

  If AttendChoice < 1 Then Exit;

  If CSDef.CFG.Tournament Then Begin
    AllAttend := AttendChoice In [1];
    isdate := False;
  End
  Else Begin
    AllAttend := AttendChoice In [1, 3];
    isdate := AttendChoice In [3, 4];
  End;
  If isdate Then Begin
    Date_again:
    iStartDate := 0;
    iEndDate := 0;
    // -------------------------------------------------------
    fGetDate;
    // -------------------------------------------------------
    sStartDate := fYearMonthDay(iStartDate);
    sEndDate := fYearMonthDay(iEndDate);
    If sEndDate < sStartDate Then Begin
      ErrBox('Newest Date cannot be earlier than Oldest date', MC, 0);
      Goto date_again;
    End
    Else If ((Length(Trim(sEndDate)) < 8) Or (Length(Trim(sStartDate)) < 8)) Then Exit;
  End;
  TotMps := 0;
  TotAttend := 0;
  FillChar(ColorMPs, SizeOf(ColorMps), 0);
  sFileName := 'A' + Trim(Playern.Player_no) + '.TXT';
  Set_Destination(55, 3, 2, Nil, 'Attendance destination ', sFileName,'', 0, 80);
  If Destination < 1 Then Goto ExitPoint;
  PleaseWait;
  SetLineSpace(6);
  ReportLine(0, ppica, 1, '');
  If AllAttend Then ReportLine(1, pnone, 1, 'ATTENDANCE for ')
  Else ReportLine(1, pnone, 1, 'MASTERPOINTS for ');
  With Playern Do ReportLine(0, pnone, 1, Trim(Player_no) + ' '
      + Trim(First_Name) + ' ' + Trim(Last_Name));
  If isdate Then ReportLine(0, pnone, 1, ' from ' + DateToStr(iStartDate) + ' to '
      + DateToStr(iEndDate))
  Else ReportLine(0, pnone, 1, ' as of ' + Slash(SysDate));

  last_link := ' ';

  While ok Do With Attend Do Begin
      getarec(Attend_no);

      If CSDef.CFG.Tournament Then Begin
        If last_link <> link_file[1] Then Begin
          last_link := link_file[1];
          Case last_link Of
            'T': If AllAttend Then ReportLine(2, pnone, 1,
                  '    DATE     SANCTION  SESS  MPs    %   EVENT CODE & NAME')
              Else ReportLine(2, pnone, 1,
                  '    DATE     SANCTION  SESS  MPs  EVENT CODE & NAME');
            'C': If AllAttend Then ReportLine(2, pnone, 1,
                  '    DATE     CLUB  SESS  MPs    %   SECTION & EVENT')
              Else ReportLine(2, pnone, 1,
                  '    DATE     CLUB  SESS  MPs  EVENT');
          End;
        End;
        If AllAttend Or (Valu(Attend.MPS) > 0) Then Begin
          //ReportLine(1, pnone, 1, '  ' + show_link(false, link_file, hash, 75, AllAttend));
          ReportLine(1, pnone, 1, '  ' + show_link(false, link_file, hash));
          TotMps := TotMps + Valu(MPS);
          Color := Ival(MP_Color);
          If (Color In [0, 1, 2, 3, 4, 5]) Then
            ColorMps[Color] := ColorMps[Color] + Valu(MPS);
          Inc(TotAttend);
        End;
      End
      Else Begin
        ValidRec := AllAttend Or (Valu(Attend.MPS) > 0);
        If ValidRec And isdate Then Begin
          If link_file[1] = 'C' Then Begin
            Hashkey := hash;
            FindKey(IdxKey[ClubGame_no, 3]^, Recno[ClubGame_no], Hashkey);
            ValidRec := OK;
            If ValidRec Then With ClubGame Do Begin
                GetARec(ClubGame_no);
                ValidRec := (fYearMonthDay(DateStringtoDate(Event_Date)) >= sStartDate)
                  And (fYearMonthDay(DateStringtoDate(Event_Date)) <= sEndDate);
              End;
          End
          Else ValidRec := false;
        End;
        If ValidRec Then Begin
          If last_link <> link_file[1] Then Begin
            last_link := link_file[1];
            Case last_link Of
              'T': If AllAttend Then ReportLine(2, pnone, 1,
                    '    DATE     SANCTION  SESS  MPs    %   EVENT CODE & NAME')
                Else ReportLine(2, pnone, 1,
                    '    DATE     SANCTION  SESS  MPs  EVENT CODE & NAME');
              'C': If AllAttend Then ReportLine(2, pnone, 1,
                    '    DATE     CLUB  SESS  MPs    %   SECTION & EVENT')
                Else ReportLine(2, pnone, 1,
                    '    DATE     CLUB  SESS  MPs  EVENT');
            End;
          End;
          // ReportLine(1, pnone, 1, '  ' + show_link(link_file, hash, 75, AllAttend));
          ReportLine(1, pnone, 1, '  ' + show_link(False, link_file, hash));
          Color := Ival(MP_Color);
          If (Color In [0, 1, 2, 3, 4, 5]) Then
            ColorMps[Color] := ColorMps[Color] + Valu(MPS);
          Inc(TotAttend);
          TotMps := TotMps + Valu(MPS);
        End;
      End;
      nextkey(IdxKey[Attend_no, 1]^, recno[Attend_no], tempkey);
      ok := ok And CompareKey(tempkey, savekey);
    End;

  If TotAttend < 1 Then ReportLine(2, pnone, 1, 'No Attendance records reported')
  Else Begin
    NewLine(1);
    For Color := 0 To 5 Do If ColorMps[Color] > 0 Then
        ReportLine(1, pnone, 1, LeftPad(ColorTab[Color], 19)
          + Real2Str(ColorMps[Color], 9, 2));
    ReportLine(1, pnone, 1, LeftPad('Total', 19) + Real2Str(TotMps, 9, 2));
  End;
  If Destination = 3 Then NewLine(2);
  EraseBoxWindow;
  ViewCentered := true;
  If Destination = 1 Then ViewerShow('', 'A' + Trim(Playern.Player_no) + '.TXT');
  ExitPoint:
  ok := true;
  recno[Attend_no] := oldrecno;
  If Status_Ok(Attend_no) And (oldrecno > 0) Then Begin
    findmulti(Attend_no, recno[Attend_no], tsavekey);
    getarec(Attend_no);
  End;
  Close_Destination;
End;

Procedure TfrmAttendance.ButFindActionExecute(Sender: TObject);
Var
  sHashNo01, sHashNo02: String;
  iRowMove, iRowCount: Integer;
Begin
  iRowCount := SgEvents.RowCount;
  If SgEvents.RowCount > 2 Then Begin
    sHashNo01 := Key2Str(ATTEND.HASH, 4);
    // --------------------------------------------------
    frmAttendance.Enabled := False;
    ActionManager1.State := asSuspended;
    // --------------------------------------------------
      { OK must be set to true before calling the Find_Record function }
    Ok := True;
    Find_Record(GetKey(Playern_no, 3), GetKey(Attend_no, 1), '', MC, 1, True);
    // --------------------------------------------------
    frmAttendance.Enabled := True;
    ActionManager1.State := asNormal;
    // --------------------------------------------------
    sHashNo02 := Key2Str(ATTEND.HASH, 4);
    SgEvents.Row := 1;
    If SgEvents.RowCount > 2 Then Begin
      For iRowMove := 1 To (SgEvents.RowCount - 2) Do Begin
        SgEvents.Row := SgEvents.Row + 1;
        If sgEvents.Cells[8, SgEvents.Row] = sHashNo02 Then Exit;
      End;
    End
    Else MessageDlg('No Attendance records to find.', mtInformation, [mbOK], 0);
  End
  Else MessageDlg('No Attendance records to find.', mtInformation, [mbOK], 0);
End;

Procedure TfrmAttendance.Action1Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmAttendance.Action2Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmAttendance.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
  If UpperCase(sType) = 'S' Then
    pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'M' Then
    pMEFieldAvail(bDoedit, (oSender As TMaskEdit))
  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
  // -------------------------------------------------------------------------------------------------------
  If bDisplay_Rec Then Begin
    { Load all information to the appropriate field from the current record }
    pFillMEFields;
  End;
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If UpperCase(sType) = 'M' Then Begin
      If Not (oSender As TMaskEdit).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure TfrmAttendance.pMEFieldAvail(bAvail: Boolean; oOSField: TMaskEdit);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmAttendance.pResetFldAttrib;
Var
  iComponent: integer;
Begin
  With Self Do Begin
    For iComponent := 0 To ComponentCount - 1 Do Begin
      If Components[iComponent].ClassType = TMaskedit Then
        pMEFieldAvail(True, (Components[iComponent] As TMaskEdit))
    End;
  End;
End;

Procedure TfrmAttendance.pButtOn(bTurnOn: Boolean);
Begin
  ButCopyAction.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
End;

Procedure TfrmAttendance.F5KeyExecute(Sender: TObject);
Begin
  Case frmPlayers.iRanking Of
    8: Begin { 8 = Percent_no }
        frmPercentages := TfrmPercentages.Create(Self);
        frmPercentages.ShowModal;
        frmPercentages.Free;
      End;
    10: Begin { 10 = OARank_no }
        frmOARank := TfrmOARank.Create(Self);
        frmOARank.ShowModal;
        frmOARank.Free;
      End;
  End;
  Filno := Attend_no;
  Scrno := 2;
End;

Procedure TfrmAttendance.Button8Click(Sender: TObject);
Begin
  If HelpContext > 0 Then keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmAttendance.pSetColsWidth(sgName: tStringGrid);
Var
  i, j, ColWidth, iCurColWidth, ThisCellWidth: integer;
Begin
  With sgName Do Begin
    For j := 0 To (ColCount - 1) Do Begin
      iCurColWidth := ColWidths[j];
      ColWidth := 0;
      For i := 0 To (RowCount - 1) Do Begin
        ThisCellWidth := sgName.Canvas.TextWidth(Cells[j, i]);
        If ThisCellWidth > ColWidth Then ColWidth := ThisCellWidth;
      End; {for all rows}
      { Only adjust the colwidth if the text is greater than the default colwidth }
      If (ColWidth + 5) > iCurColWidth Then
        sgName.ColWidths[j] := ColWidth + 5; {or + whatever margin you want}
    End; {for all columns}
  End; {with StringGrid1}
End;

Procedure TfrmAttendance.OSFEventSessNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFEventSessNo.Text) >= 1) And (Ival(OSFEventSessNo.Text) <= 9)) Then Begin
        ErrBox('Session number must be between 1 and 9 inclusive', mc, 0);
        OSFEventSessNo.Text := '1';
        OSFEventSessNo.SetFocus;
      End
      Else Begin
        ATTEND.SESSION_NO := Pad(OSFEventSessNo.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 3, ATTEND.SESSION_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFEventSessNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.SESSION_NO := Pad(OSFEventSessNo.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 3, ATTEND.SESSION_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFSectionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.SECTION := Pad(OSFSection.Text,2);
  bDoEdit := CustomPLEdit(Scrno, 4, ATTEND.SECTION, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFSectionExit(Sender: TObject);
Var
  sString: ShortString;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      sString := OSFSection.Text;
      If Not (valid_section(sString)) Then Begin
        ErrBox('Invalid Section', mc, 0);
        OSFSection.Text := '';
        OSFSection.SetFocus;
      End
      Else Begin
        ATTEND.SECTION := Pad(OSFSection.Text,2);
        bDoEdit := CustomPLEdit(Scrno, 4, ATTEND.SECTION, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFColorNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.MP_COLOR := Pad(OSFColorNo.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 6, ATTEND.MP_COLOR, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFColorNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((OSFColorNo.Text >= '0') And (OSFColorNo.Text <= '6')) Then Begin
        ErrBox('Invalid MP color selection', mc, 0);
        OSFColorNo.Text := '0';
        OSFColorNo.SetFocus;
      End
      Else Begin
        ATTEND.MP_COLOR := Pad(OSFColorNo.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 6, ATTEND.MP_COLOR, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFColorNoChange(Sender: TObject);
Begin
  LabelColorText.Caption := LibData.fGetFullMPColor(OSFColorNo.Text);
End;

Procedure TfrmAttendance.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmAttendance.OSFTypeNoChange(Sender: TObject);
Begin
  LabelTypeText.Caption := LibData.fGetType(OSFTypeNo.Text);
End;

Procedure TfrmAttendance.OSFTypeNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.MP_TYPE := Pad(OSFTypeNo.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 8, ATTEND.MP_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFTypeNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((OSFTypeNo.Text >= '0') And (OSFTypeNo.Text <= '2')) Then Begin
        ErrBox('Invalid MP type selection', mc, 0);
        OSFTypeNo.Text := '0';
        OSFTypeNo.SetFocus;
      End
      Else Begin
        ATTEND.MP_TYPE := Pad(OSFTypeNo.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 8, ATTEND.MP_TYPE, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFMPsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.MPS := LeftPad(Trim(OSFMPs.Text),6);
  bDoEdit := CustomPLEdit(Scrno, 10, ATTEND.MPS, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFMPsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((VALU(OSFMPs.Text) >= 0)) Then Begin
        ErrBox('', mc, 0);
        OSFMPs.Text := '0';
        OSFMPs.SetFocus;
      End
      Else Begin
        ATTEND.MPS := LeftPad(Trim(OSFMPs.Text),6);
        bDoEdit := CustomPLEdit(Scrno, 10, ATTEND.MPS, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFPercentEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.PERCENT := LeftPad(Trim(OSFPercent.Text),5);
  bDoEdit := CustomPLEdit(Scrno, 11, ATTEND.PERCENT, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFPercentExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((VALU(OSFPercent.Text) >= 0) And (VALU(OSFPercent.Text) < 100)) Then Begin
        ErrBox('Percentage must be between 0 and 100', mc, 0);
        OSFPercent.Text := '0';
        OSFPercent.SetFocus;
      End
      Else Begin
        ATTEND.PERCENT := LeftPad(Trim(OSFPercent.Text),5);
        bDoEdit := CustomPLEdit(Scrno, 11, ATTEND.PERCENT, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFStratumEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.Stratum := Pad(OSFStratum.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 12, ATTEND.Stratum, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFStratumExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((OSFStratum.Text[1] In ['1', '2', '3'])) Then Begin
        ErrBox('Strat must be 1 (top), 2, or 3 (bottom)', mc, 0);
        OSFStratum.Text := '1';
        OSFStratum.SetFocus;
      End
      Else Begin
        ATTEND.Stratum := Pad(OSFStratum.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 12, ATTEND.Stratum, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFDirectionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.Direction := Pad(OSFDirection.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 13, ATTEND.Direction, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFDirectionExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFDirection.Text) In [1, 2, 3, 4])) Then Begin
        ErrBox('Direction must be 1, 2, 3, or 4', mc, 0);
        OSFDirection.Text := '1';
        OSFDirection.SetFocus;
      End
      Else Begin
        ATTEND.Direction := Pad(OSFDirection.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 13, ATTEND.Direction, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFPairNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.Pair_no := LeftPad(Trim(OSFPairNo.Text),3);
  bDoEdit := CustomPLEdit(Scrno, 15, ATTEND.Pair_no, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFPairNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFPairNo.Text) >= 0)) Then Begin
        ErrBox('Must not be negative', mc, 0);
        OSFPairNo.Text := '0';
        OSFPairNo.SetFocus;
      End
      Else Begin
        ATTEND.Pair_no := LeftPad(Trim(OSFPairNo.Text),3);
        bDoEdit := CustomPLEdit(Scrno, 15, ATTEND.Pair_no, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFRankLowEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.rank_low := LeftPad(Trim(OSFRankLow.Text),3);
  bDoEdit := CustomPLEdit(Scrno, 16, ATTEND.rank_low, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFRankLowExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFRankLow.Text) >= 0)) Then Begin
        ErrBox('Must not be negative', mc, 0);
        OSFRankLow.Text := '0';
        OSFRankLow.SetFocus;
      End
      Else Begin
        ATTEND.rank_low := LeftPad(Trim(OSFRankLow.Text),3);
        bDoEdit := CustomPLEdit(Scrno, 16, ATTEND.rank_low, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFRankHighEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.rank_high := LeftPad(Trim(OSFRankHigh.Text),3);
  bDoEdit := CustomPLEdit(Scrno, 17, ATTEND.rank_high, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFRankHighExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFRankHigh.Text) = 0) Or (Ival(OSFRankHigh.Text) >= Ival(OSFRankLow.Text))) Then Begin
        ErrBox('High rank must be zero or >= low rank', mc, 0);
        OSFRankHigh.Text := '0';
        OSFRankHigh.SetFocus;
      End
      Else Begin
        ATTEND.rank_high := LeftPad(Trim(OSFRankHigh.Text),3);
        bDoEdit := CustomPLEdit(Scrno, 17, ATTEND.rank_high, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmAttendance.OSFBoardsEnter(Sender: TObject);
Begin
  bDoEdit := True;
  ATTEND.boards := LeftPad(Trim(OSFBoards.Text),3);
  bDoEdit := CustomPLEdit(Scrno, 18, ATTEND.boards, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmAttendance.OSFBoardsExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFBoards.Text) >= 0)) Then Begin
        ErrBox('Must not be negative', mc, 0);
        OSFBoards.Text := '0';
        OSFBoards.SetFocus;
      End
      Else Begin
        ATTEND.boards := LeftPad(Trim(OSFBoards.Text),3);
        bDoEdit := CustomPLEdit(Scrno, 18, ATTEND.boards, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Function TfrmAttendance.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

End.

