Unit formPercentages;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ResizeKit, ExtCtrls, ActnList,
  XPStyleActnCtrls, ActnMan, Mask, LMDCustomComponent, LMDCustomHint,
  LMDHint, LMDCustomButton, LMDButton, LMDControl, LMDBaseControl,
  LMDBaseGraphicControl, LMDGraphicControl, LMDCustomGraphicLabel,
  LMDGraphicLabel, ovceditf, ovcedpop, ovcedcal, Grids, ovcbase, ovcef,
  ovcsf, ovccmbx, ovcpb, ovcpf, dBase, DB33, vListBox, PVio;

Type
  TfrmPercentages = Class(TForm)
    ResizeKit1: TResizeKit;
    StatusBar1: TStatusBar;
    StaticText1: TStaticText;
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF5Key: TButton;
    ButtonF7Key: TButton;
    StaticTextF5Key: TStaticText;
    StaticTextF7Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F5Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    gbPercent: TGroupBox;
    Label1: TLabel;
    MELastName: TMaskEdit;
    Label2: TLabel;
    MEFirstName: TMaskEdit;
    Label3: TLabel;
    MEPlayerNo: TMaskEdit;
    MERecordNumber: TMaskEdit;
    Label5: TLabel;
    Shape1: TShape;
    Shape2: TShape;
    Shape4: TShape;
    LMDGraphicLabel1: TLMDGraphicLabel;
    LMDButPercentDone: TLMDButton;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButPercentDone: TAction;
    Label4: TLabel;
    MEGameLink: TMaskEdit;
    Shape3: TShape;
    Label6: TLabel;
    ODEPercentDate: TOvcDateEdit;
    sgPercent: TStringGrid;
    OSFClubNo: TOvcSimpleField;
    Label7: TLabel;
    MEClubName: TMaskEdit;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    OSFClubWkSessNo: TOvcSimpleField;
    OSFSection: TOvcSimpleField;
    OPFPercent: TOvcPictureField;
    Procedure F5KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure pSetFunctionKeys;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pFillPercentFields;
    Procedure pGetPercentRecord;
    Function fGetGameDesc: String;
    Procedure ODEDateExit(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pAbortPercentChanges;
    Procedure pSavePercentChanges;
    Procedure pGetClubDefRecord;
    Procedure OSFClubNoEnter(Sender: TObject);
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure pNextPerRec;
    Procedure ButPrevClick(Sender: TObject);
    Procedure pPrevPerRec;
    Procedure pFillPercentGrid;
    Procedure pClearPerRecords;
    Procedure pColPerHeadings;
    Procedure pGetNextPerRec;
    Procedure sgPercentClick(Sender: TObject);
    Procedure pMoveToPerRecord;
    Procedure fGetClubGame;
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure LMDButPercentDoneClick(Sender: TObject);
    Procedure sgPercentDblClick(Sender: TObject);
    Procedure pPrintPercent;
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure pClearPerFields;
    Procedure pCopyPercentRecData;
    Procedure pCheckPercentEditFields;
    Procedure pPercentDone;
    Procedure sgPercentKeyPress(Sender: TObject; Var Key: Char);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure OSFClubNoExit(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bPerRecFound: Boolean;
    bClubDefRecFound: Boolean;
    bLinked: Boolean;
  End;

Var
  frmPercentages: TfrmPercentages;
  iRow: integer; // String Grid Row Number
  iChildRecs: Integer;
  ksERecNoKey, ksETempKey: keystr;
  ksARecNoKey, ksATempKey: keystr;
  ksPN, ksPNTemp: keystr;
  aOK: Boolean;

Implementation

Uses formPlayers, Util1, Util2, CSDef, LibData, YesNoBoxu;

{$R *.dfm}

Procedure TfrmPercentages.F5KeyExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPercentages.F7KeyExecute(Sender: TObject);
Begin
  pPrintPercent;
End;

Procedure TfrmPercentages.pPrintPercent;
Var
  oldrecno: longint;
  tempkey,
    savekey: keystr;
  tsavekey: keystr;
Begin
  oldrecno := recno[frmPlayers.iRanking];
  tsavekey := getkey(frmPlayers.iRanking, 1);
  tempkey := getkey(Playern_no, 3);
  savekey := tempkey;
  SearchKey(IdxKey[frmPlayers.iRanking, 1]^, recno[frmPlayers.iRanking], tempkey);
  ok := ok And CompareKey(tempkey, savekey);
  If Not ok Then With Playern Do Begin
      ErrBox('No handicap percent records found for ' + Trim(First_Name) + ' '
        + Trim(Last_Name), MC , 0);
      exit;
    End;
  PleaseWait;
  OpenDestination(2, 55, 80);
  SetLineSpace(6);
  ReportLine(0, ppica, 1, '');
  With Playern Do ReportLine(1, pnone, 1, 'HANDICAP PERCENTAGES for '
      + Trim(Player_no) + ' ' + Trim(First_Name) + ' ' + Trim(Last_Name) + ' as of '
      + Slash(SysDate));
  ReportLine(2, pnone, 1, 'CLUB #    DATE     SESS PERCENT    EVENT');
  {$Ifdef UOARank}
  While ok Do With OARank Do Begin
  {$Else}
  While ok Do With percent Do Begin
  {$Endif}
      getarec(frmPlayers.iRanking);
      ReportLine(1, pnone, 1, club_no + ' ' + slash(percent_date) + '   ' + club_sess_no
        + '   ' + percent + ' ' + show_link('C', hash, 35, true));
      nextkey(IdxKey[frmPlayers.iRanking, 1]^, recno[frmPlayers.iRanking], tempkey);
      ok := ok And CompareKey(tempkey, savekey);
    End;
  ok := true;
  recno[frmPlayers.iRanking] := oldrecno;
  If Status_Ok(frmPlayers.iRanking) And (oldrecno > 0) Then Begin
    findmulti(frmPlayers.iRanking, recno[frmPlayers.iRanking], tsavekey);
    getarec(frmPlayers.iRanking);
  End;
  Close_Destination;
  PVIO.EraseBoxWindow;
End;

Procedure TfrmPercentages.pSetFunctionKeys;
Var
  iComponentLoop: Integer;
  tcTemp: TComponent;
  iButAdd, iButQuit, iButLMD, iPercentNo, iPlayerNo: Integer;
  bTurnOff: Boolean;
  sButName: String;
Begin
  { F1 Key }
  ButtonF1Key.Hint := frmPlayers.sStandFuncKeyHint + StaticTextF1Key.Caption;
  { F5 Key }
  ButtonF5Key.Hint := frmPlayers.sStandFuncKeyHint + StaticTextF5Key.Caption;
  { F7 Key }
  ButtonF7Key.Hint := frmPlayers.sStandFuncKeyHint + StaticTextF7Key.Caption;
  { If there is not a record for this player in the Percent Table, only allow for Adding a record
    or Quiting }
  {$Ifdef UOARank}
  iPercentNo := Key2Num(OARank.RECORD_NO);
  {$Else}
  iPercentNo := Key2Num(PERCENT.RECORD_NO);
  {$Endif}
  iPlayerNo := Key2Num(PLAYERN.RECORD_NO);
  { bPerRecFound = was a Percent Record found for this Player? }
  bLinked := (iPercentNo <> iPlayerNo);
  { If there is no Percent Record for this Player }
  If bLinked Then Begin
    { Loop thru all components looking for TButtons, certain buttons should not be available,
      if there is no existing Percent Record }
    For iComponentLoop := (ComponentCount - 1) Downto 0 Do Begin
      tcTemp := Components[iComponentLoop];
      If (tcTemp Is TButton) And (Pos('KEY', UpperCase((tcTemp As TButton).Name)) = 0) Then Begin
        { Don't turn off the Add, Quit Buttons or the Function Keys }
        sButName := UpperCase((tcTemp As TButton).Name);
        // ==================================================================
        { Check to see if the Current Button is not the Add or Quit buttons }
        iButAdd := Pos('BUTADD', sButName);
        iButQuit := Pos('BUTQUIT', sButName);
        { Check to see if the Current Button is not an LMD button }
        iButLMD := Pos('LMD', sButName);
        { If adding the three values together is greater than zero, then it is one of the buttons
          we do not want to turn off }
        bTurnOff := ((iButAdd + iButQuit + iButLMD) = 0);
        If bTurnOff Then Begin
          (tcTemp As TButton).Enabled := False;
          (tcTemp As TButton).Action.OnExecute := Nil;
          (tcTemp As TButton).OnClick := Nil;
        End; { bTurnOff is True }
      End; { Is TButton and name does not contain the word "KEY" }
    End; { For Loop thru all form components looking for TButtons }
  End; { If there is no Percent Record for this Player }
End;

Procedure TfrmPercentages.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPercentages.FormActivate(Sender: TObject);
Begin
  { frmPlayers.iRanking possible values:
    10 = using OARank.Dat
     8 = using Percent.Dat }
  if frmPlayers.iRanking = 10 then {$define UOARank};
  pSetFunctionKeys;
  LMDGraphicLabel1.Caption := '';
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else Begin
    StatusBar1.Panels[0].Text := 'Club';
  End;
  { Check to make sure the drive letter is used in the Prepend variable. If it is not, the program
    will use the drive that the executable is running from! }
  //If Copy(dbase.Prepend, 1, 1) = '\' Then dbase.Prepend := 'C:' + dbase.Prepend;
  { Show the path to the table in use and the table name in StatusBar1. }
  {$Ifdef UOARank}
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'OARANK.DAT');
  {$Else}
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'PERCENT.DAT');
  {$Endif}
  { Store the number of records in iChildRecs }
  iChildRecs := db33.UsedRecs(DatF[frmPlayers.iRanking]^);
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iChildRecs)) + ' Records.';
  // --------------------------------------------------------------------------
  ksPN := GetKey(Playern_no, 3);
  ksPNTemp := ksPN;
  ClearKey(IdxKey[frmPlayers.iRanking, 1]^);
  { Search for records in the Percent table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], ksPNTemp);
  If (Ok And CompareKey(ksPNTemp, ksPN)) Then Begin
    pFillPercentFields;
    pFillPercentGrid;
  End;
End;

Procedure TfrmPercentages.F1KeyExecute(Sender: TObject);
Begin
  Application.HelpCommand(HELP_CONTENTS, 0);
End;

Procedure TfrmPercentages.pFillPercentFields;
Begin
  { Load Club Player information to the appropriate MaskEdit field from the current record }
  MELastName.Text := Playern.LAST_NAME;
  MEFirstName.Text := Playern.FIRST_NAME;
  MEPlayerNo.Text := Playern.PLAYER_NO;
  MERecordNumber.Text := IntToStr(Key2Num(dbase.PLAYERN.RECORD_NO));
  // ------------------------------------------------------------------
  pGetPercentRecord; // Get the percentage record for this player
  // ------------------------------------------------------------------
  If bPerRecFound Then Begin
    MEGameLink.Text := fGetGameDesc;
    // ------------------------------------------------------------------
  {$Ifdef UOARank}
    ODEPercentDate.Text := LibData.fFormatDateString(OARANK.);
    OSFClubNo.Text := '';
    OSFClubWkSessNo.Text := OARANK.SESSION_NO;
    OSFSection.Text := OARANK.SECTION;
    OPFPercent.Text := FloatToStrF(StrToFloat(OARANK.SCORE), ffFixed, 6, 2);
  {$Else}
    ODEPercentDate.Text := LibData.fFormatDateString(PERCENT.PERCENT_DATE);
    OSFClubNo.Text := PERCENT.CLUB_NO;
    OSFClubWkSessNo.Text := PERCENT.CLUB_SESS_NO;
    OSFSection.Text := PERCENT.SECTION;
    OPFPercent.Text := FloatToStrF(StrToFloat(PERCENT.PERCENT), ffFixed, 6, 2);
  {$Endif}
    // ------------------------------------------------------------------
    pGetClubDefRecord; // Get the Club Def that corresponds to the Percentage record
  End
  Else Begin
    MEGameLink.Text := '';
    // ------------------------------------------------------------------
    ODEPercentDate.Text := '';
    OSFClubNo.Text := '';
    OSFClubWkSessNo.Text := '';
    OSFSection.Text := '';
    OPFPercent.Text := '';
    // ------------------------------------------------------------------
    MEClubName.Text := '';
  End;
End;

Procedure TfrmPercentages.pGetPercentRecord;
Var
  ksXRecNoKey, ksXTempKey: keystr;
  bCompare: Boolean;
Begin
  ksXRecNoKey := GetKey(Playern_no, 3);
  ksXTempKey := ksXRecNoKey;
  ClearKey(IdxKey[frmPlayers.iRanking, 1]^);
  { Search for records in the Percent table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], ksXTempKey);
  bCompare := (CompareKey(ksXTempKey, ksXRecNoKey));
  bPerRecFound := ((ok) And bCompare);
  If bPerRecFound Then Begin
    dBase.GetARec(frmPlayers.iRanking); // Store the record
  End;
End;

Function TfrmPercentages.fGetGameDesc: String;
Var
  ksRNKey, ksTKey: keystr;
  bOK: Boolean;
Begin
  ksRNKey := GetKey(frmPlayers.iRanking, 3);
  ksTKey := ksRNKey;
  ClearKey(IdxKey[CLUBGAME_no, 1]^);
  db33.SearchKey(IdxKey[CLUBGAME_no, 3]^, Recno[CLUBGAME_no], ksTKey);
  bOK := (ok And CompareKey(ksTKey, ksRNKey));
  If bOK Then Begin
    DBUseLinks := False;
    { Must call GetARec to display the record if it is found }
    dBase.GetARec(CLUBGAME_no);
    DBUseLinks := True;
    { Return the Event Information }
    result := Percent.PERCENT_DATE + '  ' + Percent.CLUB_NO + '  ' + Percent.CLUB_SESS_NO + '  ' +
      FloatToStrF(dBase.GetPlayerPoints(PlayerN, ppTotal) * 0.010, ffFixed, 15, 2) + '  ' +
      Percent.PERCENT + '  ' + Percent.SECTION + '  ' + CLUBGAME.EVENT_NAME;
  End
  Else result := 'NOT LINKED';
End;

Procedure TfrmPercentages.ODEDateExit(Sender: TObject);
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
End;

Procedure TfrmPercentages.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then pAbortPercentChanges;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then pSavePercentChanges;
End;


Procedure TfrmPercentages.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then pAbortPercentChanges;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then pSavePercentChanges;
End;

Procedure TfrmPercentages.pAbortPercentChanges;
Begin
  pPercentDone;
  MEGameLink.Text := fGetGameDesc;
  // ------------------------------------------------------------------
  ODEPercentDate.Text := LibData.fFormatDateString(Dbase.PERCENT.PERCENT_DATE);
  OSFClubNo.Text := dbase.PERCENT.CLUB_NO;
  OSFClubWkSessNo.Text := dbase.PERCENT.CLUB_SESS_NO;
  OSFSection.Text := dbase.PERCENT.SECTION;
  OPFPercent.Text := FloatToStrF(StrToFloat(Dbase.PERCENT.PERCENT), ffFixed, 6, 2);
  // ------------------------------------------------------------------
  pGetClubDefRecord; // Get the Club Def that corresponds to the Percentage record
End;

Procedure TfrmPercentages.pSavePercentChanges;
Begin
  // Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  dbase.Filno := frmPlayers.iRanking;
  Case frmPlayers.iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyPercentRecData;
        dbase.Add_Record;
      End;
    2: Begin // Edit Mode
        pCheckPercentEditFields;
        dbase.PutARec(frmPlayers.iRanking);
      End;
  End;
  pPercentDone;
  pFillPercentGrid;
End;

Procedure TfrmPercentages.pCopyPercentRecData;
Begin
  Dbase.PERCENT.PERCENT_DATE := LibData.fPadRight(fStripDateField(ODEPercentDate.Text), 40);
  dbase.PERCENT.CLUB_NO := LibData.fPadRight(OSFClubNo.Text, 40);
  dbase.PERCENT.CLUB_SESS_NO := LibData.fPadRight(OSFClubWkSessNo.Text, 40);
  dbase.PERCENT.SECTION := LibData.fPadRight(OSFSection.Text, 40);
  Dbase.PERCENT.PERCENT := LibData.fPadRight(OPFPercent.Text, 40);
End;

Procedure TfrmPercentages.pCheckPercentEditFields;
Begin
  If Trim(Dbase.PERCENT.PERCENT_DATE) <> Trim(fStripDateField(ODEPercentDate.Text)) Then
    Dbase.PERCENT.PERCENT_DATE := LibData.fPadRight(fStripDateField(ODEPercentDate.Text), 40);
  If Trim(dbase.PERCENT.CLUB_NO) <> Trim(OSFClubNo.Text) Then
    dbase.PERCENT.CLUB_NO := LibData.fPadRight(OSFClubNo.Text, 40);
  If Trim(dbase.PERCENT.CLUB_SESS_NO) <> Trim(OSFClubWkSessNo.Text) Then
    dbase.PERCENT.CLUB_SESS_NO := LibData.fPadRight(OSFClubWkSessNo.Text, 40);
  If trim(dbase.PERCENT.SECTION) <> Trim(OSFSection.Text) Then
    dbase.PERCENT.SECTION := LibData.fPadRight(OSFSection.Text, 40);
  If trim(Dbase.PERCENT.PERCENT) <> trim(OPFPercent.Text) Then
    Dbase.PERCENT.PERCENT := LibData.fPadRight(OPFPercent.Text, 40);
End;

Procedure TfrmPercentages.pGetClubDefRecord;
Var
  ksXRecNoKey, ksXTempKey: keystr;
Begin
  ksXRecNoKey := GetKey(frmPlayers.iRanking, 2);
  ksXTempKey := ksXRecNoKey;
  ClearKey(IdxKey[ClubDef_no, 1]^);
  { Search for records in the Percent table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[ClubDef_no, 1]^, Recno[ClubDef_no], ksXTempKey);
  bClubDefRecFound := ((ok) And (CompareKey(ksXTempKey, ksXRecNoKey)));
  If bClubDefRecFound Then Begin
    DBUseLinks := False;
    dBase.GetARec(ClubDef_no);
    DBUseLinks := True;
    MEClubName.Text := Dbase.CLUBDEF.CLUB_NAME;
  End
  Else MEClubName.Text := '';
End;

Procedure TfrmPercentages.OSFClubNoEnter(Sender: TObject);
Var
  lbClubs: TListBox;
Begin
  { Create a listbox of choices for the available clubs }
  If Trim(OSFClubNo.Text) = '' Then Begin
    lbClubs := tListBox.Create(Self);
    lbClubs.Left := OSFClubNo.Left;
    lbClubs.top := (OSFClubNo.Top - 30);
    lbClubs.Width := 200;
    lbClubs.Height := 200;
    lbClubs.Free;
  End;
End;

Procedure TfrmPercentages.ButNextActionExecute(Sender: TObject);
Begin
  If sgPercent.Row <> (sgPercent.RowCount - 1) Then sgPercent.Row := sgPercent.Row + 1;
  pMoveToPerRecord;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPercentages.pNextPerRec;
Var
  FndKey: KeyStr;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  LibData.SkipNext(frmPlayers.iRanking, FndKey);
  { Read data base file Playern_no.  Record number in Recno[Playern_no]}
  GetARec(frmPlayers.iRanking);
  { Load information to the appropriate field from the current record }
  pFillPercentFields;
End;

Procedure TfrmPercentages.ButPrevClick(Sender: TObject);
Begin
  pPrevPerRec; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus;
End;

Procedure TfrmPercentages.pPrevPerRec;
Var
  FndKey: KeyStr;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  PrevKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], FndKey);
  GetARec(frmPlayers.iRanking);
  { Must run Align_Rec to align children records for this record }
  dBase.Align_Rec(RecNo[frmPlayers.iRanking]);
  { Load information to the appropriate field from the current record }
  pFillPercentFields;
End;

Procedure TfrmPercentages.pFillPercentGrid;
Begin
  { Fill the StringGrids with the Percent Records for this Player }
  { pClearGroups: Clears the entries in the StringGrid }
  pClearPerRecords;
  // ---------------------------------------------------------------------------
  { Store the current records data to the string grid }
  iRow := 0;
  //==============================================================================================
  ksARecNoKey := GetKey(Playern_no, 3);
  ksATempKey := ksARecNoKey;
  { Search for records in the Percent table that match the Player Record Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], ksATempKey);
  aOK := OK;
  //==============================================================================================
  pColPerHeadings;
  If ok And CompareKey(ksATempKey, ksARecNoKey) Then Begin
    // Increment the Row Count for the next available Row in the string grid
    iRow := iRow + 1;
    If sgPercent.RowCount < iRow + 1 Then sgPercent.RowCount := sgPercent.RowCount + 1;
    dBase.GetARec(frmPlayers.iRanking); // Store the record
    fGetClubGame;
  End;

  While ok And CompareKey(ksATempKey, ksARecNoKey) Do Begin
    sgPercent.Cells[0, iRow] := dbase.PERCENT.CLUB_NO;
    sgPercent.Cells[1, iRow] := Slash(dbase.PERCENT.PERCENT_DATE);
    sgPercent.Cells[2, iRow] := dbase.PERCENT.CLUB_SESS_NO;
    sgPercent.Cells[3, iRow] := dbase.PERCENT.PERCENT;
    sgPercent.Cells[4, iRow] := dbase.PERCENT.SECTION;
    sgPercent.Cells[5, iRow] := dbase.CLUBGAME.EVENT_NAME;
    // Go to the next record in the table
    pGetNextPerRec;
    If (aOK) And (CompareKey(ksATempKey, ksARecNoKey)) Then Begin
      dBase.GetARec(frmPlayers.iRanking); // Store the record
      // Increment the Row Count for the next available Row in the string grid
      iRow := iRow + 1;
      If sgPercent.RowCount < iRow + 1 Then sgPercent.RowCount := sgPercent.RowCount + 1;
      fGetClubGame;
    End
    Else Begin
      //==========================================================================================
      { Need to place record pointer back on the first Percent record. Move String Grid back to
        row 1 also. }
      { Reset ksATempKey back to the intial value to go to the first linked record for the
        child record }
      ksATempKey := ksARecNoKey;
      db33.SearchKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], ksATempKey);
      dBase.GetARec(frmPlayers.iRanking); // Store the record
      Break;
    End;
  End;
  If sgPercent.RowCount > 0 Then sgPercent.Row := 1;
End;

Procedure TfrmPercentages.pClearPerRecords;
Var
  iForLoop: integer;
Begin
  { Clear out all previous entries in the String Grid for Groups in preparation
    for any entries from the current player percent record }
  For iForLoop := 0 To (sgPercent.RowCount - 1) Do Begin
    sgPercent.Rows[iForLoop].Clear;
  End;
  sgPercent.RowCount := 1;
End;

Procedure TfrmPercentages.pColPerHeadings;
Begin
  sgPercent.Cells[0, iRow] := 'CLUB #';
  sgPercent.Cells[1, iRow] := 'DATE';
  sgPercent.Cells[2, iRow] := 'SESSION #';
  sgPercent.Cells[3, iRow] := 'PERCENT';
  sgPercent.Cells[4, iRow] := ''; // SECTION
  sgPercent.Cells[5, iRow] := ''; // EVENT NAME
End;

Procedure TfrmPercentages.pGetNextPerRec;
Begin
  NextKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], ksATempKey);
  aOK := OK;
  OK := True;
End;

Procedure TfrmPercentages.sgPercentClick(Sender: TObject);
Begin
  pMoveToPerRecord;
End;

Procedure TfrmPercentages.pMoveToPerRecord;
Var
  iMove, iCurrRow: Integer;
Begin
  ClearKey(IdxKey[frmPlayers.iRanking, 1]^);
  //==============================================================================================
  iCurrRow := sgPercent.Row;
  ksARecNoKey := GetKey(Playern_no, 3);
  ksATempKey := ksARecNoKey;
  { Search for records in the Percent table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[frmPlayers.iRanking, 1]^, Recno[frmPlayers.iRanking], ksATempKey);
  iMove := 1;
  If iCurrRow > 0 Then Begin
    While iMove < sgPercent.Row Do Begin
      pGetNextPerRec;
      Inc(iMove);
    End;
    If (aOK) And (CompareKey(ksATempKey, ksARecNoKey)) Then Begin
      dBase.GetARec(frmPlayers.iRanking); // Store the record
      MEGameLink.Text := fGetGameDesc;
      // ------------------------------------------------------------------
      ODEPercentDate.Text := LibData.fFormatDateString(Dbase.PERCENT.PERCENT_DATE);
      OSFClubNo.Text := dbase.PERCENT.CLUB_NO;
      OSFClubWkSessNo.Text := dbase.PERCENT.CLUB_SESS_NO;
      OSFSection.Text := dbase.PERCENT.SECTION;
      OPFPercent.Text := FloatToStrF(StrToFloat(Dbase.PERCENT.PERCENT), ffFixed, 6, 2);
      // ------------------------------------------------------------------
      pGetClubDefRecord; // Get the Club Def that corresponds to the Percentage record
    End;
  End;
End;

Procedure TfrmPercentages.fGetClubGame;
Var
  ksRNKey, ksTKey: keystr;
  bOK: Boolean;
Begin
  ksRNKey := GetKey(frmPlayers.iRanking, 3);
  ksTKey := ksRNKey;
  ClearKey(IdxKey[CLUBGAME_no, 3]^);
  db33.SearchKey(IdxKey[CLUBGAME_no, 3]^, Recno[CLUBGAME_no], ksTKey);
  bOK := (ok And CompareKey(ksTKey, ksRNKey));
  If bOK Then Begin
    DBUseLinks := False;
    { Must call GetARec to display the record if it is found }
    dBase.GetARec(CLUBGAME_no);
    DBUseLinks := True;
  End;
End;

Procedure TfrmPercentages.ButEditActionExecute(Sender: TObject);
Begin
  LMDGraphicLabel1.Caption := 'Edit Mode';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  frmPlayers.iDataMode := 2;
  // Save the forms Position and Size coordinates so they can be restored when editing is complete
  frmPlayers.pSaveFormInfo;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asNormal;
  GBMenu.Enabled := False;
  GBMenu.Hide;
  gbPercent.Enabled := True;
  // Turn the Done Button on and make it visible
  LMDButPercentDone.Enabled := True;
  LMDButPercentDone.Visible := True;
  ODEPercentDate.SetFocus;
End;

Procedure TfrmPercentages.LMDButPercentDoneClick(Sender: TObject);
Begin
  pSavePercentChanges;
  pPercentDone;
End;

Procedure TfrmPercentages.pPercentDone;
Begin
  // Turn the Done Button on and make it invisible
  LMDButPercentDone.Enabled := False;
  LMDButPercentDone.Visible := False;
  LMDGraphicLabel1.Caption := '';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  frmPlayers.iDataMode := 0;
  ActionManager1.State := asNormal;
  ActionManager2.State := asSuspended;
  GBMenu.Enabled := True;
  GBMenu.Show;
  gbPercent.Enabled := False;
  frmPlayers.pSetFormBack;
End;

Procedure TfrmPercentages.sgPercentDblClick(Sender: TObject);
Begin
  ButEditActionExecute(ButEdit);
End;

Procedure TfrmPercentages.ButAddActionExecute(Sender: TObject);
Begin
  LMDGraphicLabel1.Caption := 'Add Mode';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  frmPlayers.iDataMode := 1;
  // Save the forms Position and Size coordinates so they can be restored when editing is complete
  frmPlayers.pSaveFormInfo;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asNormal;
  GBMenu.Enabled := False;
  GBMenu.Hide;
  gbPercent.Enabled := True;
  // Turn the Done Button on and make it visible
  LMDButPercentDone.Enabled := True;
  LMDButPercentDone.Visible := True;
  pClearPerFields;
  ODEPercentDate.SetFocus;
End;

Procedure TfrmPercentages.pClearPerFields;
Begin
  MEGameLink.Text := '';
  // ------------------------------------------------------------------
  ODEPercentDate.Text := '';
  OSFClubNo.Text := '';
  OSFClubWkSessNo.Text := '';
  OSFSection.Text := '';
  OPFPercent.Text := '';
  // ------------------------------------------------------------------
  MEClubName.Text := '';
End;

Procedure TfrmPercentages.ButCopyActionExecute(Sender: TObject);
Begin
  LMDGraphicLabel1.Caption := 'Copy Mode';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  frmPlayers.iDataMode := 3;
  // Save the forms Position and Size coordinates so they can be restored when editing is complete
  frmPlayers.pSaveFormInfo;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asNormal;
  GBMenu.Enabled := False;
  GBMenu.Hide;
  gbPercent.Enabled := True;
  // Turn the Done Button on and make it visible
  LMDButPercentDone.Enabled := True;
  LMDButPercentDone.Visible := True;
  ODEPercentDate.SetFocus;
End;

Procedure TfrmPercentages.sgPercentKeyPress(Sender: TObject; Var Key: Char);
Begin
  pMoveToPerRecord;
End;

Procedure TfrmPercentages.ButPrevActionExecute(Sender: TObject);
Begin
  If sgPercent.Row > 1 Then sgPercent.Row := sgPercent.Row - 1;
  pMoveToPerRecord;
End;

Procedure TfrmPercentages.ButTopActionExecute(Sender: TObject);
Begin
  If sgPercent.Row > 0 Then sgPercent.Row := 1
  Else
    MessageDlg('There are no Event Rank/Quals for this player....', mtInformation, [mbOK], 0);
End;

Procedure TfrmPercentages.ButLastActionExecute(Sender: TObject);
Begin
  If sgPercent.Row > 0 Then sgPercent.Row := (sgPercent.RowCount - 1)
  Else
    MessageDlg('There are no Event Rank/Quals for this player....', mtInformation, [mbOK], 0);
End;

Procedure TfrmPercentages.ButDeleteActionExecute(Sender: TObject);
Begin
  If (MessageDlg('Delete the current record?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) Then Begin
    dbase.Filno := frmPlayers.iRanking;
    dbase.DelARec(frmPlayers.iRanking);
    pFillPercentGrid;
  End;
End;

Procedure TfrmPercentages.OSFClubNoExit(Sender: TObject);
Var
  bValidClub: Boolean;
Begin
  { Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  bValidClub := Valid_club(OSFClubNo.Text, false);
  { if it is not a valid club number }
  If Not bValidClub Then Begin
    MessageDlg('Invalid club number or club number already exists', mtError, [mbOK], 0);
    OSFClubNo.SetFocus;
  End
  Else MEClubName.Text := CLUBDEF.CLUB_NAME;
End;

Procedure TfrmPercentages.ButFindActionExecute(Sender: TObject);
Begin
  FilNo := frmPlayers.iRanking;
  pGenericFind;
End;

End.

