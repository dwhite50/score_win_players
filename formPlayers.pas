{$DEFINE FormResize}
Unit formPlayers;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ovcbase, ovcdlg, ovcclkdg, StdCtrls, Mask, Buttons,
  ToolWin, ExtCtrls, ActnMan, ActnCtrls, Menus, ActnList, ovcef, ovcsf,
  ovcrvcbx, ovcpb, ovcpf, ovcnf, ovceditf, ovcedpop, ovcedcal, ovcbordr,
  ovcbcalc, ovcbcldr, XPStyleActnCtrls, Grids, ImgList, {ResizeKit, }LMDCustomComponent,
  LMDWndProcComponent, LMDFormShape, LMDShapeHint,
  LMDHint, LMDCustomScrollBox, LMDListBox, LMDCustomControl, LMDCustomPanel,
  LMDCustomBevelPanel, LMDBaseEdit, LMDCustomEdit, LMDCustomMaskEdit, LMDCustomExtCombo,
  LMDCustomListComboBox, LMDListComboBox, ovccmbx, LMDHeaderListComboBox,
  LMDCustomComboBox, LMDComboBox, STSTRS, LMDIniCtrl, LMDFormA,
  LMDStorBase, LMDStorXMLVault, LMDStorBinVault, LMDCustomHint,
  LMDCustomShapeHint, LMDMessageHint, LibData, LMDCalendarEdit,
  LMDCustomButton, LMDButton, LibUtil, mtables, LMDSimplePanel, LMDControl,
  LMDBaseControl, LMDBaseGraphicButton, LMDCustomShapeButton,
  LMDShapeButton, groups,hh,hh_funcs;

Type
  { Required for Util2.Player_Check Function }
  OpenString = String[10];
  _DateString = String[8];

Type
  TfrmPlayers = Class(TForm)
    StatusBar1: TStatusBar;
    GBPlayerInfo: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Shape1: TShape;
    Label3: TLabel;
    Shape2: TShape;
    StaticText1: TStaticText;
    GBMenu: TGroupBox;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Shape3: TShape;
    Label11: TLabel;
    ActionManager1: TActionManager; // Allows for older version keystoke compatibility
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Shape4: TShape;
    Shape5: TShape;
    Label15: TLabel;
    Shape6: TShape;
    Bevel1: TBevel;
    Label17: TLabel;
    Label18: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Shape7: TShape;
    Shape8: TShape;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Shape9: TShape;
    Shape10: TShape;
    Shape11: TShape;
    Shape12: TShape;
    Label24: TLabel;
    Shape13: TShape;
    Label25: TLabel;
    Shape14: TShape;
    Label26: TLabel;
    ButQuit: TButton;
    ButQuitAction: TAction;
    ButtonF4Key: TButton;
    ButtonF3Key: TButton;
    ButtonF5Key: TButton;
    Shape23: TShape;
    ButtonF2Key: TButton;
    ButtonF7Key: TButton;
    StaticTextF3Key: TStaticText;
    StaticTextF4Key: TStaticText;
    StaticTextF5Key: TStaticText;
    StaticTextF2Key: TStaticText;
    StaticTextF7Key: TStaticText;
    ButtonF8Key: TButton;
    StaticTextF8Key: TStaticText;
    ButtonF6Key: TButton;
    StaticTextF6Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    F4Key: TAction;
    F3Key: TAction;
    F5Key: TAction;
    F2Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    F8Key: TAction;
    F6Key: TAction;
    ButDonePlayerInfo: TButton;
    ActionManager2: TActionManager; // Allows for older version keystoke compatibility
    ButPlayerInfoDone: TAction;
    ActionManager3: TActionManager;
    ImageList1: TImageList;
    LMDHLCBState: TLMDHeaderListComboBox;
    LMDHLCBCountry: TLMDHeaderListComboBox;
    SpeedButton1: TSpeedButton;
    LMDHLCBRank: TLMDHeaderListComboBox;
    LMDHint1: TLMDHint;
    LMDHLCBMail: TLMDHeaderListComboBox;
    LMDHLCBGender: TLMDHeaderListComboBox;
    LMDHLCBFee: TLMDHeaderListComboBox;
    gbGroups: TGroupBox;
    Label38: TLabel;
    Label39: TLabel;
    SGGroups: TStringGrid;
    Label40: TLabel;
    Shape25: TShape;
    OSFLastName: TOvcSimpleField;
    OSFFirstName: TOvcSimpleField;
    OSFPlayerNo: TOvcSimpleField;
    OSFStreet1: TOvcSimpleField;
    OSFStreet2: TOvcSimpleField;
    OSFCity: TOvcSimpleField;
    OSFEmail: TOvcSimpleField;
    OPFPaidThru: TOvcPictureField;
    OPFZip: TOvcPictureField;
    OSFUnitNo: TOvcSimpleField;
    OSFDistrictNo: TOvcSimpleField;
    OvcController1: TOvcController;
    ODEStartDate: TOvcDateEdit;
    LMDButPlayerInfoDone: TLMDButton;
    OPFCatA: TOvcPictureField;
    OPFCatB: TOvcPictureField;
    OPFGroupCode: TOvcPictureField;
    OPFGroupNo: TOvcPictureField;
    ActionManager4: TActionManager;
    Action1: TAction;
    GBMasterPoints: TGroupBox;
    Label28: TLabel;
    Shape17: TShape;
    Label29: TLabel;
    Shape18: TShape;
    Label30: TLabel;
    Shape19: TShape;
    Label31: TLabel;
    Shape20: TShape;
    Label32: TLabel;
    OPFPTotal: TOvcPictureField;
    OPFPYTD: TOvcPictureField;
    OPFPMTD: TOvcPictureField;
    OPFPRecent: TOvcPictureField;
    OPFPEligibility: TOvcPictureField;
    Action2: TAction;
    Action3: TAction;
    Action4: TAction;
    OPFPhone: TOvcPictureField;
    Action5: TAction;
//    ResizeKit1: TResizeKit;
    Label37: TLabel;
    Shape15: TShape;
    GroupsMenu: TLMDSimplePanel;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    Button6: TButton;
    Button7: TButton;
    Button8: TButton;
    ActionManager5: TActionManager;
    GNext: TAction;
    GPrev: TAction;
    GTop: TAction;
    GLast: TAction;
    GEdit: TAction;
    GAdd: TAction;
    GCopy: TAction;
    GDelete: TAction;
    GF1: TAction;
    GPrint: TAction;
    Button10: TButton;
    Button11: TButton;
    F3: TAction;
    OPFLocalActiveDate: TOvcPictureField;
    OPFAcblActiveDate: TOvcPictureField;
    OPFLastACBLUpdate: TOvcPictureField;
    Button9: TButton;
    Procedure FormActivate(Sender: TObject);
    Procedure FormClose(Sender: TObject; Var Action: TCloseAction);
    { pLoadTPFields: Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    Procedure pLoadTPFields;
    Procedure pNextRecord;
    Procedure pPrevRecord;
    { pGBPlayerInfoReadOnly: Sets the enabled property of GBPlayerInfo to the boolean variable passed }
    Procedure pGBPlayerInfoReadOnly(bLogical: Boolean);
    { pGBMasterPointsReadOnly: Sets the enabled property of GBMasterPoints to the boolean variable passed }
    Procedure pGBMasterPointsReadOnly(bLogical: Boolean);
    Procedure FormCreate(Sender: TObject);
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    { pSetButHints: Set the edit menu buttons hints to their default entries }
    Procedure pSetButHints;
    { pFillVars: Fill the StringList slButtonNames with the button names for the menu.
                 Names must be added in the order of:   ButNext, ButPrev, ButFind, ButTop,
                 ButLast, ButEdit, ButAdd, ButCopy, ButDelete, ButQuit }
    Procedure pFillVars;
    { pFillGroups: Fill The StringGrid SGGroups with any groups for this Player, clear the grid if there are none }
    Procedure pFillGroups;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure ButQuitClick(Sender: TObject);
    Procedure F3KeyExecute(Sender: TObject);
    Procedure F5KeyExecute(Sender: TObject);
    Procedure F2KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    { DEVELOPMENT ONLY!!!!!!!
      pKeyExecuteMsg: Basic procedure for testing to tell which key the user pressed,
                      to make sure they are working as expected }
    Procedure pKeyExecuteMsg(oSender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure F8KeyExecute(Sender: TObject);
    Procedure F6KeyExecute(Sender: TObject);
    { ButAddActionExecute: Add Button OnClick Action }
    Procedure ButAddActionExecute(Sender: TObject);
    { pClearGroups: Clears the entries in the StringGrid SGGroups }
    Procedure pClearGroups;
    { pPlayerInfo: Allow or Deny access to the Player Information fields.
                      If BENABLE is True allow access,
                      If BENABLE is False Deny access  }
    Procedure pPlayerInfo(bEnable: Boolean);
    { ButPlayerInfoDoneExecute: Edit Button OnClick Action
      Done Button is only visible when access is allowed to the Player Information Fields.
      This button is used to trigger saving of any additions or changes to the record or
      reverting the record back to it's premodification state }
    Procedure ButPlayerInfoDoneExecute(Sender: TObject);
    { ButEditActionExecute: Edit Button OnClick Action }
    Procedure ButEditActionExecute(Sender: TObject);
    { ButDeleteActionExecute: Delete Button OnClick Action }
    Procedure ButDeleteActionExecute(Sender: TObject);
    { ButCopyActionExecute: Copy Button OnClick Action }
    Procedure ButCopyActionExecute(Sender: TObject);
    { pAbortChanges: Abort the edits to the current record }
    Procedure pAbortChanges;
    { pSaveFormInfo: Save the forms Height, Width, Left Side Position and Top Position }
    Procedure pSaveFormInfo;
    { pSetFormBack: Set the forms Height and Width back to what it was }
    Procedure pSetFormBack;
//    Function Status_OK(Const Fno: Integer): Boolean;
    { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
      in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
      with the changed data. The PLAYERN dataset is used by all record saving, adding and copying
      routines in DBASE.PAS, to update the actual PLAYERN.DAT table.}
    Procedure pCheckEditFields;
    { FFINDCBITEMINDEX: Used to see if there is a matching entry in the current
      TLMDHeaderListComboBox object. Returns the index position if there is or 0 if not. }
    Function fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
    { COMBOBOXCHANGE: TLMDHeaderListComboBox OnChange Routine }
    Procedure ComboBoxChange(Sender: TObject);
    { COMBOBOXENTER: TLMDHeaderListComboBox OnEnter Routine }
    Procedure ComboBoxEnter(Sender: TObject);
    { PCOPYRECDATA: Saves all data entry fields to their correct position in the PLAYERN dataset.
      The PLAYERN dataset is used by all record saving, adding and copying routines in DBASE.PAS,
      to update the actual PLAYERN.DAT table. }
    Procedure pCopyRecData;
    Procedure LMDHLCBEnter(Sender: TObject);
    Procedure pUpdateStatusBar;
    { If currently in Tournament Mode store the open file boolean settings in the array
      TOURNFILES to DBTOOPEN, if we are Club Mode store the ones in the array CLUBFILES.
      Then use the array DBToOpen to know which tables should be used. }
    //Procedure pSetDBToUse;
    Procedure SpeedButton1Click(Sender: TObject);
    Procedure F4KeyExecute(Sender: TObject);
    Procedure LMDHLCBRankExit(Sender: TObject);
    Procedure LMDHLCBMailExit(Sender: TObject);
    Procedure LMDHLCBGenderExit(Sender: TObject);
    Procedure LMDHLCBFeeExit(Sender: TObject);
    Procedure pGBGroupsReadOnly(bLogical: Boolean);
    Procedure pGBGroups(bEnable: Boolean);
    Procedure SGGroupsDblClick(Sender: TObject);
    Procedure OPFGroupCodeExit(Sender: TObject);
    Procedure pLoadGroupRec;
    Procedure pAddGroup;
    Procedure pAbortGroupAdd;
    { OSFLastNameExit: Validation of Last Name field when exiting it }
    Procedure OSFLastNameExit(Sender: TObject);
    Procedure OSFPlayerNoExit(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Function fGetLength(iFileNo: Integer; sFieldName: String): Integer;
    Procedure pDateSetFormBack;
    Procedure pDateSaveFormInfo;
    Function fCheckForChanges: Boolean;
    Procedure pSetMaxLengthOfFields;
    Procedure FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure FormKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure FormResize(Sender: TObject);
    Procedure MailingAddressCheck;
    Procedure CATaCheck;
    Procedure OSFUnitNoExit(Sender: TObject);
    Procedure pSetEvents(bSwitchEventsOn: Boolean);
    Procedure OSFDistrictNoExit(Sender: TObject);
    Procedure pDateExit(Sender: TObject);
    Procedure OPFGroupNoExit(Sender: TObject);
    Procedure pPrintTPlayers; // Print Tournament Players Record
    Procedure pPrintCPlayers;
    Procedure Action1Execute(Sender: TObject); // Print Club Players Record
    { Set Function Key Labels, Visibility, Hints and Actions for Tournament and Club Modes }
    Procedure pSetFunctionKeys;
    Procedure pGBMasterPoints(bEnable: Boolean);
    Procedure MasterPointsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pAbortMPChanges;
    Procedure pSaveMPChanges;
    Procedure pMasterPointsDone;
    Procedure OPFPEligibilityExit(Sender: TObject);
    Procedure OPFPaidThruExit(Sender: TObject);
    Procedure pDateFieldKeyPress(Sender: TObject; Var Key: Char);
    Procedure OPFPTotalExit(Sender: TObject);
    Procedure OPFPYTDExit(Sender: TObject);
    Procedure OPFPMTDExit(Sender: TObject);
    Procedure OPFPRecentExit(Sender: TObject);
    Procedure pFixPoints;
    Function fShowPoints(Const Ptype: byte): String;
    Procedure SetPoints(Const j: byte);
    Procedure FixPoints(Const j: byte);
    Procedure pStoreMPoints;
    Procedure pRefreshMPFields;
    Procedure Action2Execute(Sender: TObject);
    Procedure Action3Execute(Sender: TObject);
    Procedure Action4Execute(Sender: TObject);
    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    Procedure pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
    Procedure pResetFldAttrib;
    Procedure OSFLastNameEnter(Sender: TObject);
    Procedure OSFFirstNameEnter(Sender: TObject);
    Procedure OSFFirstNameExit(Sender: TObject);
    Procedure OSFPlayerNoEnter(Sender: TObject);
    Procedure OSFStreet1Enter(Sender: TObject);
    Procedure OSFStreet1Exit(Sender: TObject);
    Procedure OSFStreet2Enter(Sender: TObject);
    Procedure OSFStreet2Exit(Sender: TObject);
    Procedure OSFCityEnter(Sender: TObject);
    Procedure OSFCityExit(Sender: TObject);
    Procedure LMDHLCBStateEnter(Sender: TObject);
    Procedure LMDHLCBStateExit(Sender: TObject);
    Procedure OPFZipEnter(Sender: TObject);
    Procedure OPFZipExit(Sender: TObject);
    Procedure LMDHLCBCountryEnter(Sender: TObject);
    Procedure LMDHLCBCountryExit(Sender: TObject);
    Procedure OSFEmailEnter(Sender: TObject);
    Procedure OSFEmailExit(Sender: TObject);
    Procedure LMDHLCBRankEnter(Sender: TObject);
    Procedure OPFCatAEnter(Sender: TObject);
    Procedure OPFCatAExit(Sender: TObject);
    Procedure OPFCatBEnter(Sender: TObject);
    Procedure OPFCatBExit(Sender: TObject);
    Procedure LMDHLCBMailEnter(Sender: TObject);
    Procedure OSFUnitNoEnter(Sender: TObject);
    Procedure OSFDistrictNoEnter(Sender: TObject);
    Procedure LMDHLCBGenderEnter(Sender: TObject);
    Procedure LMDHLCBFeeEnter(Sender: TObject);
    Procedure ODEStartDateEnter(Sender: TObject);
    Procedure ODEStartDateExit(Sender: TObject);
    Procedure OPFPaidThruEnter(Sender: TObject);
    Procedure pButtOn(bTurnOn: Boolean);
    Procedure Action5Execute(Sender: TObject);
    Procedure pSGGroupsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pSGGroupsKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure SGGroupsClick(Sender: TObject);
    Procedure pSGGroupsKeyPress(Sender: TObject; Var Key: Char);
    Procedure GDoneExecute(Sender: TObject);
    Procedure GAddExecute(Sender: TObject);
    Procedure GNextExecute(Sender: TObject);
    Procedure GPrevExecute(Sender: TObject);
    Procedure GTopExecute(Sender: TObject);
    Procedure GEditExecute(Sender: TObject);
    Procedure SGGroupsEnter(Sender: TObject);
    Procedure Button1Enter(Sender: TObject);
    Procedure GLastExecute(Sender: TObject);
    Procedure GCopyExecute(Sender: TObject);
    Procedure OPFGroupNoEnter(Sender: TObject);
    Procedure OPFGroupCodeEnter(Sender: TObject);
    Procedure GDeleteExecute(Sender: TObject);
    Procedure pFindGroup(sGroupCode, sGroupNo: String);
    Procedure GF1Execute(Sender: TObject);
    Procedure pAbortGroupEdit;
    Procedure ButtonF1KeyClick(Sender: TObject);
    Procedure ODEStartDatePopupOpen(Sender: TObject);
    Procedure sgButtAll(AllButtAvail: Boolean);
    Function FormHelp(Command: Word; Data: Integer;
      Var CallHelp: Boolean): Boolean;
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    { Integer variable used dynamically to store the tables record count }
    iReccount: Integer;
    iPlayEvent, iSanctions, iWhereAt, iRanking: Integer;
    { Integer variables to store the forms original: Height, Width, Left Position and Top Position.
      Used to return form back to it's original size and position. }
    iTPFormHeight, iTPFormWidth, iTPFormLeft, iTPFormTop: Integer;
    iTPDFormHeight, iTPDFormWidth, iTPDFormLeft, iTPDFormTop: Integer;
    { iDataMode Integer Variable
      Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    //iDataMode: Integer;
    bOkToModify: Boolean;
    { String grid groups current column number }
    iSGGroupsCol: Integer;
    sPlayMode, sStandFuncKeyHint: String;
    bKeyIsUnique: Boolean;
  End;

  {$I dbnames.dcl}
  {$I dbfiles.dcl}

Var
  frmPlayers: TfrmPlayers;
  { StringList containing the Group Box menu's button names }
  mHHelp: THookHelpSystem;
  slButtonNames: TStringList;
  sOldElgPts, sCompStr: String;
  aPpoints: Array[1..5] Of real;
  rOldepts: real;
  aOldpoints: Array[1..5] Of String[4];
  wState: TWindowState;
  iOrgHeight, iOrgWidth: Integer;
  iNewHeight, iNewWidth: Integer;
  sOldGroup: String;
  bGroupDirty: boolean;

Const
  asFormFields: Array[1..28] Of String =
  ('OPFACBLACTIVEDATE', 'LMDHLCBRANK', 'OPFCATA', 'OPFCATB', 'OSFCITY', 'OSFFIRSTNAME',
    'LMDHLCBGENDER', 'OSFLASTNAME', 'OPFLOCALACTIVEDATE', 'LMDHLCBMAIL', 'OPFPMTD', 'OPFPHONE',
    'OSFPLAYERNO', 'ODESTARTDATE', 'LMDHLCBSTATE', 'OSFSTREET2', 'OPFPTOTAL',
    'OSFUNITNO', 'OPFPYTD', 'OPFZIP', 'OPFPAIDTHRU', 'OPFLASTACBLUPDATE', 'OSFDISTRICTNO',
    'LMDHLCBCOUNTRY', 'OSFSTREET1', 'OSFEMAIL', 'LMDHLCBFEE', 'OPFPELIGIBILITY');

  asRecordFields: Array[1..28] Of String =
  ('ACBL_DATE', 'ACBL_RANK', 'CAT_A', 'CAT_B', 'CITY', 'FIRST_NAME', 'GENDER',
    'LAST_NAME', 'LOCAL_DATE', 'MAIL_CODE', 'M_T_D', 'PHONE', 'PLAYER_NO',
    'START_DATE', 'STATE', 'STREET2', 'TOT_POINTS', 'UNIT_NO', 'Y_T_D', 'ZIP', 'PAID_THRU',
    'ACBL_UPDATE_DATE', 'DISTRICT_NO', 'COUNTRY', 'STREET1', 'EMAIL', 'FEE_TYPE',
    'ELIG_PTS');

Const
  {screen location equates}
  TR = 1; {top right}
  TL = 2; {top left}
  TC = 3; {top center}
  MR = 4; {middle right}
  ML = 5; {middle left}
  MC = 6; {middle center}
  BR = 7; {bottom right}
  BL = 8; {bottom left}
  BC = 9; {bottom center}
  { Variable to store the length of the table array }
  iLenArray: Integer = MaxFilno;
  bFirstEntryPl: Boolean = True;
  bEscKeyUsed: Boolean = False;
  bF1KeyUsed: Boolean = False;
  Scrno: Integer = 1;
  { iDataMode Integer Variable
    Set the current data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode: Integer = 0;
  { sDataMode is the Char representation of iDataMode }
  sDataMode: Char = Null;
  bDoEdit: Boolean = True;
  bDisplay_Rec: Boolean = False;

Function duplicate_player(Const number: String; Fno, Kno: integer; Func: String): boolean;

Implementation

Uses DB33, Dbase, Util1, Util2, YesNoBoxU, CSDef, formAttendance, formTblsCurrOpen, dbSizes,
  ClubFees, PVIO, VListBox, formPercentages, formOARank;

{$DEFINE indatabase}
{$DEFINE isnoattend}
{$I STSTRS.dcl}
{$I Showpts.inc}
{$I addform.inc}
{$I tdrank.inc}
{$I fixdb.inc}
{$I i_update.inc}

Procedure PleaseWait;
Begin
  PlsWait;
End;

{$R *.dfm}

Procedure TfrmPlayers.FormActivate(Sender: TObject);
Var
  // Boolean variable to tell whether a table opened successfully.
  bTablesOpened: Boolean;
  sTableName: String;
Begin
  { Save the forms Height, Width, Left Side Position and Top Position }
  pSaveFormInfo;
  If bFirstEntryPl Then Begin
    { TopFilNo: What is the topmost database we are using in this application. }
    TopFilNo := Playern_no;
    Scrno := 1;
    Edit_add := True;
    sStandFuncKeyHint := 'Use this Function Key to ';
    { Procedure to check and see if the Tables were closed correctly }
    TestBadDbase;
    { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
    UseDBLinkages := True;
    { DBUseLinks is used by the GetARec Function to tell it whether or not it should align child
      tables to the corresponded linked record also }
    DBUseLinks := True;
    //==========================================================
    { These variables are declared in CEDef.pas }
    IniName := GetCurrentDir + '\' + DefIniName;
    AppName := 'Players';
    { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is made to a "Record",
      dbase re-writes the table header and in Playern.dat, updates dbLastImpDate entry. Set them to
      False when you Delete a record, then immediately set them back to True }
    { This must be done as a standard whenever a table is opened for modifications }
    Flush_DF := True;
    Flush_IF := True;
    //==========================================================
    LibData.fCheckLoadPath;
    pSetMaxLengthOfFields;
    LibData.pCheckCFGPaths;
    // ----------------------------------------------------------------------------------------------------------
    // String variable to hold the name of the table that did not open successfully.
    sTableName := '';
    // ----------------------------------------------------------------------------------------------------------
    // Create the button name stringlist for working with button settings
    slButtonNames := TStringList.Create;
    // ----------------------------------------------------------------------------------------------------------
    // Fill all this forms static variables
    pFillVars;
    // ----------------------------------------------------------------------------------------------------------
    { Store the current play mode for Tournament or Club mode to the variable sPlayMode. }
    If CSDef.CFG.Tournament Then Begin
      sPlayMode := 'T';
      iPlayEvent := TournEv_no;
      iSanctions := Tourn_no;
      iWhereAt := Tourn_no;
      iRanking := OArank_no;
    End
    Else Begin
      sPlayMode := 'C';
      iPlayEvent := ClubGame_no;
      iSanctions := ClubSanc_no;
      iWhereAt := ClubDef_no;
      iRanking := Percent_no;
    End;
    { Since we are telling the program that we are using a path other that what is stored in the CFG.INI file, we must
      pass the path for the tables in the system wide variable: PREPEND
      FIXPATH checks to make sure there is a forward slash "\" at the end of the path passed.  }
    If CFG.Tournament Then dbase.PrePend := CFG.TournDBpath
    Else dbase.Prepend := CFG.ClubDBpath;
    { Check to make sure the drive letter is used in the Prepend variable. If it is not, the program
      will use the drive that the executable is running from! }
    // ----------------------------------------------------------------------------------------------------------
    { Remember that the application reads from the CURRENT DRIVE you are running the executable from.
      Show the current mode Club or Tournament. }
    CSDef.ReadCFG;
    If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
    Else StatusBar1.Panels[0].Text := 'Club';
    { If currently in Tournament Mode store the open file boolean settings in the array
      TOURNFILES to DBTOOPEN, if we are Club Mode store the ones in the array CLUBFILES.
      Then use the array DBToOpen to know which tables should be used. }
    pSetDBToUse;
    // ----------------------------------------------------------------------------------------------------------
    { Show the path to the table in use and the table name in StatusBar1. }
    StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'PLAYERN.DAT');
    // ----------------------------------------------------------------------------------------------------------
    { Begin opening the tables. }
    { Set the Global Variable Filno to the main table PlayerNo }
    FilNo := Playern_no;
    { Try to open the main Player table (PLAYERN.DAT) }
    bTablesOpened := OpenDBFile(Playern_no);
    If bTablesOpened Then Begin // If the main Player table opened (PLAYERN.DAT)
      { Store the number of records in PLAYERN.DAT to iReccount }
      iReccount := UsedRecs(DatF[Playern_no]^);
      { Add the record count to StatusBar1's display }
      StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
        Trim(IntToStr(iReccount)) + ' Records.';
      If Not fOpenReqTables Then Begin
        MessageDlg('Unable to open all required files!', mtError, [mbOK], 0);
        Close; // Close the form
      End;
      { Set the Global Variable Filno to the main table PlayerNo }
      FilNo := Playern_no;
      If iReccount > 0 Then Begin
        { Find the first available record and load it's values to the fields on the screen }
        Top_Record;
        pLoadTPFields;
        pFillGroups;
        { Make the GroupBox GBPlayerInfo read only, all Maskedits are in this GB, so they by
          default, are read only also }
        pGBPlayerInfoReadOnly(True);
        { Make the GroupBox GBMasterPoints read only, all Maskedits are in this GB, so they by
          default, are read only also }
        pGBMasterPointsReadOnly(True);
        { Make the GroupBox GBGroups read only, all Group Maskedits are in this GB, so they by
          default, are read only also }
        pGBGroupsReadOnly(True);
      End;
    End
    Else Begin
      { If the table did not open }
      MessageDlg('Unable to open file!', mtError, [mbOK], 0);
      Close; // Close the form
    End;
    { Make sure Button Hints are set and available }
    pSetButHints;
    { Set Function Key Labels, Visibility, Hints and Actions for Tournament and Club Modes }
    pSetFunctionKeys;
    { If the Next Button is Enabled, set the initial system focus to it. }
    If iReccount > 0 Then Begin
      If ButNext.Enabled Then ButNext.SetFocus;
      pButtOn(True);
    End
    Else Begin
      { Since the record count in PLAYERN.DAT is zero, go directly to add a record mode }
      pButtOn(False);
    End;
    bFirstEntryPl := False;
  End;
End;

Procedure TfrmPlayers.FormClose(Sender: TObject; Var Action: TCloseAction);
Begin
  { Form Cleanup actions }
  { Free the StringList slButtonNames from memory }
  slButtonNames.Free;
  { Close any open tables }
  dBase.CloseFiles;
End;

Procedure TfrmPlayers.pLoadTPFields;
Var
  sEmail: String;
  rPoints: Extended;
Begin
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  OSFLastName.Text := Trim(Playern.LAST_NAME);
  OSFFirstName.Text := Trim(Playern.FIRST_NAME);
  OSFPlayerNo.Text := Trim(Playern.PLAYER_NO);
  OSFStreet1.Text := Trim(Playern.STREET1);
  OSFStreet2.Text := Trim(Playern.STREET2);
  OSFCity.Text := Trim(Playern.CITY);
  // STATE   ----------------------------------------------------------------------
  LMDHLCBState.Text := Trim(Playern.STATE);
  // ------------------------------------------------------------------------------
  OPFZip.Text := Trim(Playern.ZIP);
  // COUNTRY ----------------------------------------------------------------------
  LMDHLCBCountry.Text := Trim(Playern.COUNTRY);
  // ------------------------------------------------------------------------------
  sEmail := Trim(Playern.EMAIL);
  OSFEmail.Text := Trim(Playern.EMAIL);
  // ------------------------------------------------------------------------------
//  OPFPhone.Text := Trim(Playern.PHONE);
  OPFPhone.Text := Playern.PHONE;
  // ------------------------------------------------------------------------------
  OPFLocalActiveDate.Text := Playern.LOCAL_DATE;
  OPFAcblActiveDate.Text := Playern.ACBL_DATE;
  // ------------------------------------------------------------------------------
  LMDHLCBRank.Text := Playern.ACBL_RANK;
  // ------------------------------------------------------------------------------
  OPFCatA.Text := Playern.CAT_A;
  OPFCatB.Text := Playern.CAT_B;
  // ------------------------------------------------------------------------------
  LMDHLCBMail.Text := Playern.MAIL_CODE;
  // ------------------------------------------------------------------------------
  OSFUnitNo.Text := Trim(Playern.UNIT_NO);
  OSFDistrictNo.Text := Trim(Playern.DISTRICT_NO);
  // ------------------------------------------------------------------------------
  LMDHLCBGender.Text := Playern.GENDER;
  LMDHLCBFee.Text := Playern.FEE_TYPE;
  // ------------------------------------------------------------------------------
  ODEStartDate.Text := LibData.fFormatDateString(Playern.START_DATE);
  // ------------------------------------------------------------------------------
  OPFPaidThru.Text := Playern.PAID_THRU;
  // ------------------------------------------------------------------------------
  OPFLastACBLUpdate.Text := Playern.ACBL_UPDATE_DATE;
  // ==============================================================================================
  pStoreMPoints;
  sOldElgPts := FloatToStr(Key2Num(PLAYERN.Elig_pts) * 0.010);
  { Calculate the Players Total Points }
  sCompStr := fShowPoints(4);
  OPFPTotal.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points YTD }
  sCompStr := fShowPoints(3);
  OPFPYTD.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points MTD }
  sCompStr := fShowPoints(2);
  OPFPMTD.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Recent Points }
  sCompStr := fShowPoints(1);
  OPFPRecent.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Eligibility Points }
  sCompStr := fShowPoints(0);
  OPFPEligibility.Text := sCompStr;
  // ==============================================================================================
  { Fill The StringGrid SGGroups with any groups for this Player, clear the grid if there are none }
  pFillGroups;
  { Clear the MaskEdit fields for Group Code and Number }
  OPFGroupCode.Text := '';
  OPFGroupNo.Text := '';
End;

Procedure TfrmPlayers.pGBPlayerInfoReadOnly(bLogical: Boolean);
Begin
  { pGBPlayerInfoReadOnly: Sets the enabled property of GBPlayerInfo to the boolean variable passed }
  GBPlayerInfo.Enabled := Not bLogical;
End;

Procedure TfrmPlayers.pGBMasterPointsReadOnly(bLogical: Boolean);
Begin
  { pGBMasterPointsReadOnly: Sets the enabled property of GBMasterPoints to the boolean variable passed }
  GBMasterPoints.Enabled := Not bLogical;
End;

Procedure TfrmPlayers.pGBGroupsReadOnly(bLogical: Boolean);
Begin
  { pGBGroupsReadOnly: Sets the enabled property of GBGroups to the boolean variable passed }
  GBGroups.Enabled := Not bLogical;
End;

Procedure TfrmPlayers.FormCreate(Sender: TObject);
Begin
  mHHelp := THookHelpSystem.Create(DefChmFile,'',htHHAPI);
  ReadCfg;
End;

Procedure TfrmPlayers.ButNextActionExecute(Sender: TObject);
Begin
  pNextRecord; // Move the record pointer in the table to the next available record and load the data in the screen fields
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPlayers.pNextRecord;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  Next_Record;
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
  pFillGroups;
End;

Procedure TfrmPlayers.ButPrevActionExecute(Sender: TObject);
Begin
  pPrevRecord; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus;
End;

Procedure TfrmPlayers.pPrevRecord;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  Prev_Record;
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
  pFillGroups;
End;

Procedure TfrmPlayers.ButFindActionExecute(Sender: TObject);
Var
  bFoundMatch: Boolean;
Begin
  { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
  Filno := Playern_no;
  { OK must be set to true before calling the Find_Record function }
  Ok := True;
  // --------------------------------------------------
  frmPlayers.Enabled := False;
  ActionManager1.State := asSuspended;
  // --------------------------------------------------
  { Display the find dialog box }
  bFoundMatch := Find_Record('', '', '', MC, 0, True);
  // --------------------------------------------------
  frmPlayers.Enabled := True;
  ActionManager1.State := asNormal;
  // --------------------------------------------------
  If bFoundMatch Then Begin
    { Display the selected record }
    GetARec(Playern_no);
    { Must run Align_Rec to align children records for this record }
    Align_Rec(RecNo[Filno]);
    { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    pLoadTPFields;
    pFillGroups;
  End
  Else MessageDlg('Name not found!', mtInformation, [mbOK], 0); ;
End;

Procedure TfrmPlayers.ButTopActionExecute(Sender: TObject);
Begin
  Top_Record;
  pLoadTPFields;
  pFillGroups;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPlayers.ButLastActionExecute(Sender: TObject);
Begin
  Last_Record;
  pLoadTPFields;
  pFillGroups;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPlayers.pSetButHints;
Var
  iForLoop, IControls, iSLIndex: Integer;
  sHint, sButtonName: String;
Begin
  { pSetButHints: Set the edit menu buttons hints to their default entries }
  IControls := GBMenu.ControlCount;
  For iForLoop := 0 To (IControls - 1) Do Begin
    If GBMenu.Controls[iForLoop].ClassType = TButton Then Begin
      (GBMenu.Controls[iForLoop] As TButton).ShowHint := True;
      { Get the button's name }
      sButtonName := GBMenu.Controls[iForLoop].Name;
      { Find the position of the button name in the stringlist }
      iSLIndex := slButtonNames.IndexOf(sButtonName);
      Case iSLIndex Of
        0: sHint := 'Display the next record...'; // ButNext
        1: sHint := 'Display the previous record...'; // ButPrev
        2: sHint := 'Find a record and display it...'; // ButFind
        3: sHint := 'Display the first record...'; // ButTop
        4: sHint := 'Display the last record...'; // ButLast
        5: sHint := 'Edit the current record...'; // ButEdit
        6: sHint := 'Add a new record...'; // ButAdd
        7: sHint := 'Copy the current record to a new record...'; // ButCopy
        8: sHint := 'Delete the current record...'; // ButDelete
        9: sHint := 'Close this screen...'; // ButQuit
      Else
        sHint := '';
      End;
      // Set the button's hint to the new string
      GBMenu.Controls[iForLoop].Hint := sHint;
    End;
  End;
End;

Procedure TfrmPlayers.pFillVars;
Begin
  { Fill the StringList slButtonNames with the button names for the menu.
    Names must be added in the order of:   ButNext, ButPrev, ButFind, ButTop,
    ButLast, ButEdit, ButAdd, ButCopy, ButDelete, ButQuit }
  slButtonNames.Add('ButNext');
  slButtonNames.Add('ButPrev');
  slButtonNames.Add('ButFind');
  slButtonNames.Add('ButTop');
  slButtonNames.Add('ButLast');
  slButtonNames.Add('ButEdit');
  slButtonNames.Add('ButAdd');
  slButtonNames.Add('ButCopy');
  slButtonNames.Add('ButDelete');
  slButtonNames.Add('ButQuit');
End;

Procedure TfrmPlayers.ButQuitActionExecute(Sender: TObject);
Begin
  Close; // Close the form
End;

Procedure TfrmPlayers.ButQuitClick(Sender: TObject);
Begin
  Close; // Close the form
End;

Procedure TfrmPlayers.F5KeyExecute(Sender: TObject);
Begin
  wState := WindowState;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asSuspended;
  ActionManager3.State := asSuspended;
  // ================================================================
  { Hide the Players Form }
  frmPlayers.Hide;
  // ================================================================
  Case iRanking Of
    8: Begin { 8 = Percent_no }
        frmPercentages := TfrmPercentages.Create(Self);
        frmPercentages.ShowModal;
        frmPercentages.Free;
      End;
    10: Begin { 10 = OARank_no }
        frmOARank := TfrmOARank.Create(Self);
        frmOARank.ShowModal;
        frmOARank.Free;
      End;
  End;
  // ================================================================
  { Show the Players Form }
  frmPlayers.Show;
  { Turn on the Action Managers for frmPlayers }
  ActionManager1.State := asNormal;
  ActionManager2.State := asSuspended;
  ActionManager3.State := asSuspended;
  // ================================================================
  { Set the Global Variable Filno to the main table PlayerNo }
  FilNo := Playern_no;
  Scrno := 1;
  HelpContext := 4;
  WindowState := wState;
  Position := poScreenCenter;
End;

Procedure TfrmPlayers.F2KeyExecute(Sender: TObject);
Begin
  { Enable the Group Box for Master Points for Editing }
  pGBMasterPoints(True);
End;

Procedure TfrmPlayers.pGBMasterPoints(bEnable: Boolean);
Begin
  { Turn the Player Info Data Area ON }
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  //GBMenu.Align := alNone;
  // ----------------------------------------------------------------------
  GBMasterPoints.Enabled := bEnable;
  // ----------------------------------------------------------------------
  If bEnable Then HelpContext := 205 Else HelpContext := 4;
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 2;
  sDataMode := 'E';
  // ==============================================================================================
  //pStoreMPoints; { Added 07/11/06 }
  sOldElgPts := FloatToStr(Key2Num(PLAYERN.Elig_pts) * 0.010);
  OPFPTotal.SetFocus;
End;

Procedure TfrmPlayers.F7KeyExecute(Sender: TObject);
Begin
  Filno := PlayerN_No;
  If sPlayMode = 'T' Then pPrintTPlayers Else pPrintCPlayers;
End;

Procedure TfrmPlayers.pPrintCPlayers;
Var
  GroupKey: KeyStr;
  TmpKey: KeyStr;
  SaveKey: KeyStr;
  full: boolean;
  esc: boolean;
Begin
  { Print the current players record for the Club Mode }
  esc := false;
  full := Player_Check(Playern.Player_no, pnACBL)
    And YesNoBox(esc, false,
    'Include change of address / membership renewal form', 0, MC, Nil);
  If esc Then exit;
  PleaseWait;
  OpenDestination(2, 0, 80);
  SetLineSpace(6);
  ReportLine(0, ppica, 1, '');
  With Playern Do Begin
    ReportLine(1, pnone, 1, Player_no + ' ' + Trim(First_Name) + ' ' + Trim(Last_Name));
    ReportLine(2, pnone, 1, Trim(Street1) + ', ' + Trim(Street2) + ', ' + Trim(City)
      + ', ' + State + ' ' + Trim(Zip) + ' ' + Country);
    ReportLine(2, pnone, 1, 'Phone: ' + GetPhone(Copy(Phone, 1, 10)) + ', '
      + GetPhone(Copy(Phone, 11, 10)) + ';  Gender: ' + Gender);
    ReportLine(2, pnone, 1, 'Start date: ' + Slash(Start_Date)
      + '; Last active date:  Local: ' + Slash(Local_Date)
      + '; ACBL: ' + Slash(ACBL_Date));
    ReportLine(2, pnone, 1, 'ACBL rank: ' + ACBL_Rank + '; Cat A: ' + Cat_a + '; Cat B: '
      + Cat_b + '; Mail: ' + Mail_Code + '; Unit: ' + Unit_no + '; District: '
      + District_no);
    ReportLine(2, pnone, 1, 'Paid Thru: ' + Copy(Paid_Thru, 1, 2) + '/'
      + Copy(Paid_Thru, 3, 4) + '; Last ACBL update: ' + Slash(ACBL_update_date));
    ReportLine(2, pnone, 1, 'Masterpoints: Total: ' + Trim(ShowPoints(4))
      + '; Y-T-D: ' + Trim(ShowPoints(3)) + '; M-T-D: ' + Trim(ShowPoints(2))
      + '; Recent: ' + Trim(ShowPoints(1)));
    ReportLine(2, pnone, 1, 'Email: ' + Trim(email));
  End;
  If full Then PrintAddressForm(false, false)
  Else Begin
    GroupKey := GetKey(Playern_no, 3);
    TmpKey := GroupKey;
    SearchKey(IdxKey[PlayerGr_no, 1]^, recno[PlayerGr_no], tmpkey);
    If ok And CompareKey(TmpKey, GroupKey) Then With PlayerGr Do Begin
        ReportLine(2, pnone, 1, 'GROUP CODES:');
        NewLine(1);
        SaveKey := GetKey(PlayerGr_no, 1);
        While ok And CompareKey(TmpKey, GroupKey) Do Begin
          GetARec(PlayerGr_no);
          ReportLine(1, pnone, 1, Group + ' ' + Group_no);
          NextKey(IdxKey[PlayerGr_no, 1]^, recno[PlayerGr_no], tmpkey);
        End;
        FindKey(IdxKey[PlayerGr_no, 1]^, recno[PlayerGr_no], SaveKey);
        GetARec(PlayerGr_no);
      End;
  End;
  ok := true;
  Close_Destination;
  EraseBoxWindow;
End;

Procedure TfrmPlayers.pPrintTPlayers;
Var
  GroupKey: KeyStr;
  TmpKey: KeyStr;
  SaveKey: KeyStr;
  full: boolean;
Begin
  { Print the current players record for the Tournament Mode }
  full := Player_Check(Playern.Player_no, pnACBL);
  PleaseWait;
  OpenDestination(2, 0, 80);
  SetLineSpace(6);
  ReportLine(0, ppica, 1, '');
  With Playern Do Begin
    ReportLine(1, pnone, 1, Player_no + ' ' + Trim(First_Name) + ' ' + Trim(Last_Name));
    ReportLine(2, pnone, 1, Trim(Street1) + ', ' + Trim(Street2) + ', ' + Trim(City)
      + ', ' + State + ' ' + Trim(Zip) + ' ' + Country);
    ReportLine(2, pnone, 1, 'Phone: ' + GetPhone(Copy(Phone, 1, 10)) + ', '
      + GetPhone(Copy(Phone, 11, 10)) + ';  Gender: ' + Gender);
    ReportLine(2, pnone, 1, 'Start date: ' + Slash(Start_Date)
      + '; Last active date:  Local: ' + Slash(Local_Date)
      + '; ACBL: ' + Slash(ACBL_Date));
    ReportLine(2, pnone, 1, 'ACBL rank: ' + ACBL_Rank + '; Cat A: ' + Cat_a + '; Cat B: '
      + Cat_b + '; Mail: ' + Mail_Code + '; Unit: ' + Unit_no + '; District: '
      + District_no);
    ReportLine(2, pnone, 1, 'Paid Thru: ' + Copy(Paid_Thru, 1, 2) + '/'
      + Copy(Paid_Thru, 3, 4) + '; Last ACBL update: ' + Slash(ACBL_update_date));
    ReportLine(2, pnone, 1, 'Masterpoints: Total: ' + Trim(ShowPoints(4))
      + '; Y-T-D: ' + Trim(ShowPoints(3)) + '; M-T-D: ' + Trim(ShowPoints(2))
      + '; Recent: ' + Trim(ShowPoints(1)));
    ReportLine(2, pnone, 1, 'Email: ' + Trim(email));
  End;
  GroupKey := GetKey(Playern_no, 3);
  TmpKey := GroupKey;
  SearchKey(IdxKey[PlayerGr_no, 1]^, recno[PlayerGr_no], tmpkey);
  If ok And CompareKey(TmpKey, GroupKey) Then With PlayerGr Do Begin
      ReportLine(2, pnone, 1, 'GROUP CODES:');
      NewLine(1);
      SaveKey := GetKey(PlayerGr_no, 1);
      While ok And CompareKey(TmpKey, GroupKey) Do Begin
        GetARec(PlayerGr_no);
        ReportLine(1, pnone, 1, Group + ' ' + Group_no);
        NextKey(IdxKey[PlayerGr_no, 1]^, recno[PlayerGr_no], tmpkey);
      End;
      FindKey(IdxKey[PlayerGr_no, 1]^, recno[PlayerGr_no], SaveKey);
      GetARec(PlayerGr_no);
    End;
  ok := true;
  If full Then PrintAddressForm(false, false);
  Close_Destination;
  EraseBoxWindow;
End;

Procedure TfrmPlayers.pKeyExecuteMsg(oSender: TObject);
Begin
  { DEVELOPEMENT: Basic procedure for testing to tell which key the user pressed,
                 to make sure they are working as expected }
  MessageDlg('The ' + (oSender As TAction).Caption + ' key was pressed.',
    mtInformation, [mbOK], 0);
End;

Procedure TfrmPlayers.F1KeyExecute(Sender: TObject);
Begin
  wState := WindowState;
  pRunExtProg(frmPlayers, cfg.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  { Set the Global Variable Filno to the main table PlayerNo }
  FilNo := Playern_no;
  Scrno := 1;
  WindowState := wState;
  Position := poScreenCenter;
End;

Procedure TfrmPlayers.F8KeyExecute(Sender: TObject);
Begin
  wState := WindowState;
  // ------------------------------------------------------------
  LibData.MergeAttend;
  // ------------------------------------------------------------
  { Set the Global Variable Filno to the main table PlayerNo }
  FilNo := Playern_no;
  Scrno := 1;
  WindowState := wState;
  Position := poScreenCenter;
End;

Procedure TfrmPlayers.F6KeyExecute(Sender: TObject);
Var
  OldNum, TRecno: longint;
  DefPick, Key: keystr;
Begin
  If RecAvail[Filno] Then OldNum := Recno[Filno]
  Else OldNum := 0;
  With Playern Do Repeat
      NextKey(IdxKey[Filno, 1]^, Recno[Filno], Key);
      If ok Then GetARec(Filno);
    Until Not ok Or Not Player_Check(Player_no, pnACBL);
  Key := '';
  If ok Then Begin
    TRecno := Recno[Filno];
  End
  Else Begin
    Align_Rec(OldNum);
    ErrBox('No more non-members found', MC, 0);
  End;
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
  pFillGroups;
End;

Procedure TfrmPlayers.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  iDataMode := 1;
  sDataMode := 'A';
  HelpContext := 203;
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);
  InitRecord(Filno);
  pLoadTPFields;
  { pClearGroups: Clears the entries in the StringGrid SGGroups }
  pClearGroups;
  frmplayers.OPFGroupCode.Text := '';
  frmplayers.OPFGroupNo.Text := '';
  { Put the cursor in the first field }
  frmplayers.OSFLastName.SetFocus;
End;

Procedure TfrmPlayers.pClearGroups;
Begin
  { Clear out all previous entries in the String Grid for Groups in preparation
    for any entries from the current player record }
  SGGroups.Rows[0].Clear;
  SGGroups.Rows[1].Clear;
End;

Procedure TfrmPlayers.pPlayerInfo(bEnable: Boolean);
Begin
  { Set Events for Edit Fields }
  pSetEvents(bEnable);
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  { Set the state of ActionManager2 for the form players GBMenu }
  If bEnable Then ActionManager2.State := asNormal Else
    ActionManager2.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  // ----------------------------------------------------------------------
  GBPlayerInfo.Enabled := bEnable;
  // ----------------------------------------------------------------------
  LMDButPlayerInfoDone.Enabled := bEnable;
  LMDButPlayerInfoDone.Visible := bEnable;
  // ----------------------------------------------------------------------
  If Not bEnable Then HelpContext := 4;
End;

Procedure TfrmPlayers.ButPlayerInfoDoneExecute(Sender: TObject);
Begin
  Refresh;
  Filno := Playern_no;
  Case iDataMode Of
    1, 3: Begin
        If Not duplicate_player(OSFPlayerNo.Text, Playern_no, 2, 'C') Then Begin
          CATaCheck;
          MailingAddressCheck;
          pCopyRecData;
          Add_Record;
        End
        Else Begin
          If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFPlayerNo.SetFocus;
            Exit;
          End
          Else pAbortChanges; // Adding or Copying a record
        End;
      End;
    2: Begin
        If fCheckForChanges Then Begin
          { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
            in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
            with the changed data. }
          CATaCheck;
          MailingAddressCheck;
          pCheckEditFields;
          bKeyIsUnique := PostEditFixKeys;
          If Not bKeyIsUnique Then Begin
            If (MessageDlg('This ACBL Player Number already exists in this database.', mtError,
              [mbOK], 0) = mrOK) Then Begin
              OSFPlayerNo.SetFocus;
              Exit;
            End
            Else pAbortChanges;
          End
          Else Begin
            PutARec(Playern_no); { If everything is ok, then save the record }
            pButtOn(True);
          End;
        End
        Else pAbortChanges; // Editing a record
      End;
  End;
  { Turn the Player Info Data Area OFF }
  pPlayerInfo(False);
  If UsedRecs(DatF[Playern_no]^) > 0 Then pButtOn(True) Else pButtOn(False);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  pUpdateStatusBar;
  bEscKeyUsed := False;
  pResetFldAttrib;
End;

Procedure TfrmPlayers.pUpdateStatusBar;
Begin
  iReccount := UsedRecs(DatF[Playern_no]^);
  StatusBar1.Panels[1].Text := UpperCase(Prepend + 'PLAYERN.DAT') + ' - Contains ' +
    Trim(IntToStr(iReccount)) + ' Records.';
End;


Procedure TfrmPlayers.ButEditActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  PreEditSaveKeys;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 2;
  sDataMode := 'E';
  HelpContext := 202;
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);
  { Put the cursor in the first field }
  frmplayers.OSFLastName.SetFocus;
End;

Procedure TfrmPlayers.ButDeleteActionExecute(Sender: TObject);
Begin
  Filno := Playern_no;
  Delete_Record(True, frmPlayers.HelpContext);
  pUpdateStatusBar;
  { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
  pLoadTPFields;
  pFillGroups;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmPlayers.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  iDataMode := 3;
  sDataMode := 'C';
  HelpContext := 204;
  { Turn the Player Info Data Area ON }
  pPlayerInfo(True);
  { Put the cursor in the first field }
  frmplayers.OSFLastName.SetFocus;
End;

Procedure TfrmPlayers.pSetFormBack;
Begin
  { Set the forms Height and Width back to what it was }
  frmplayers.Height := iTPFormHeight;
  frmplayers.Width := iTPFormWidth;
  frmplayers.Left := iTPFormLeft;
  frmplayers.Top := iTPFormTop;
End;

Procedure TfrmPlayers.pSaveFormInfo;
Begin
  { Save the forms Height, Width, Left Side Position and Top Position }
  iTPFormHeight := frmplayers.Height;
  iTPFormWidth := frmplayers.Width;
  iTPFormLeft := frmplayers.Left;
  iTPFormTop := frmplayers.Top;
End;

Procedure TfrmPlayers.pAbortChanges;
Begin
  { Abort the edits to the current record }
  { Set the forms Height and Width back to what it was }
  //pSetFormBack;
  { Turn the Player Info Data Area OFF }
  pPlayerInfo(False);
  //-------------------------------------------------------------------------------------
  If iDataMode = 1 Then Begin // If adding a record
    //ClearKey(IdxKey[Playern_no, 1]^);
    { Find the first available record and load it's values to the fields on the screen }
    pNextRecord;
  End
  Else Begin
    { Reload the data from the table into the record }
    GetARec(Playern_no);
    { Load all Tournment Player information to the appropriate MaskEdit field from the current record }
    pLoadTPFields;
    pFillGroups;
  End;
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  bEscKeyUsed := False;
  pResetFldAttrib;
End;

{Function TfrmPlayers.Status_OK(Const Fno: Integer): Boolean;
Var
  TOK: Boolean;
Begin
  TOK := True;
  TOK := (UsedRecs(DatF[Fno]^) > 0);
  Status_OK := TOK
End;}

Function TfrmPlayers.fGetLength(iFileNo: Integer; sFieldName: String): Integer;
Begin
  Result := dbSizes.Playern_no
End;

Procedure TfrmPlayers.pCheckEditFields;
Begin
  { PCHECKEDITFIELDS: Checks each field to see if has changed from the original entry
    in PLAYERN dataset and, if it has, updates the correct field position in PLAYERN dataset
    with the changed data. The PLAYERN dataset is used by all record saving, adding and copying
    routines in PAS, to update the actual PLAYERN.DAT table.}
  { Pad fields with 40 blank spaces, since 40 characters is the widest field length and when
  a record field is stored, extra spaces will be stripped off. }
  With PLAYERN Do Begin
    LAST_NAME := Pad(OSFLastName.Text,16);
    FIRST_NAME := Pad(OSFFirstName.Text,16);
    PLAYER_NO := Pad(OSFPlayerNo.Text,7);
    STREET1 := Pad(OSFStreet1.Text,30);
    STREET2 := Pad(OSFStreet2.Text,26);
    CITY := Pad(OSFCity.Text,16);
    STATE := Pad(LMDHLCBState.Text,2);
    ZIP := Pad(OPFZip.Text,10);
    COUNTRY := Pad(LMDHLCBCountry.Text,2);
    EMAIL := Pad(OSFEmail.Text,40);
    PHONE := Pad(OPFPhone.GetStrippedEditString,20);
    ACBL_RANK := Pad(LMDHLCBRank.Text,1);
    CAT_A := Pad(OPFCatA.Text,1);
    CAT_B := Pad(OPFCatB.Text,1);
    MAIL_CODE := Pad(LMDHLCBMail.Text,1);
    UNIT_NO := FixNumField(OSFUnitNo.Text,3);
    DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
    GENDER := Pad(LMDHLCBGender.Text,1);
    FEE_TYPE := Pad(LMDHLCBFee.Text,1);
    START_DATE := Pad(fStripDateField(ODEStartDate.Text),8);
    PAID_THRU := Pad(OPFPaidThru.GetStrippedEditString,6);
  End;
End;

Function TfrmPlayers.fCheckForChanges: Boolean;
Var
  osPlayer_no: OpenString;
Begin
  With PLAYERN Do Begin
    Result := false;
    { Check to see if the player number has changed, then check to make sure it is not a duplicate
      player number. }
    If trim(frmplayers.OSFPlayerNo.Text) <> trim(PLAYER_NO) Then Begin
      Result := True;
      osPlayer_no := OSFPlayerNo.Text;
      If Not (player_check(osPlayer_no, pnAny)) Then Begin
        MessageDlg('Invalid ACBL Player Number!', mtError, [mbOK], 0);
        Result := True;
      End;
    End;

    If Trim(OSFLastName.Text) <> Trim(LAST_NAME) Then Result := True;
    If Trim(OSFFirstName.Text) <> Trim(FIRST_NAME) Then Result := True;
    If Trim(OSFStreet1.Text) <> Trim(STREET1) Then Result := True;
    If Trim(OSFStreet2.Text) <> Trim(STREET2) Then Result := True;
    If Trim(OSFCity.Text) <> Trim(CITY) Then Result := True;
    If Trim(LMDHLCBState.Text) <> Trim(STATE) Then Result := True;
    If Trim(OPFZip.Text) <> Trim(ZIP) Then Result := True;
    If Trim(LMDHLCBCountry.Text) <> Trim(COUNTRY) Then Result := True;
    If Trim(OSFEmail.Text) <> Trim(EMAIL) Then Result := True;
    If Trim(OPFPhone.GetStrippedEditString) <> Trim(PHONE) Then Result := True;
    If LMDHLCBRank.Text <> ACBL_RANK Then Result := True;
    If OPFCatA.Text <> CAT_A Then Result := True;
    If OPFCatB.Text <> CAT_B Then Result := True;
    If LMDHLCBMail.Text <> MAIL_CODE Then Result := True;
    If Trim(OSFUnitNo.Text) <> Trim(UNIT_NO) Then Result := True;
    If Trim(OSFDistrictNo.Text) <> Trim(DISTRICT_NO) Then Result := True;
    If LMDHLCBGender.Text <> GENDER Then Result := True;
    If LMDHLCBFee.Text <> FEE_TYPE Then Result := True;
    If Trim(OPFPaidThru.GetStrippedEditString) <> Trim(PAID_THRU) Then Result := True;
    { Date fields must be stripped of any non-numeric characters }
    If Trim(fStripDateField(ODEStartDate.Text)) <> Trim(START_DATE) Then Result := True;
  End;
End;

Procedure TfrmPlayers.pCopyRecData;
Begin
  { PCOPYRECDATA: Saves all data entry fields to their correct position in the PLAYERN dataset.
    The PLAYERN dataset is used by all record saving, adding and copying routines in PAS,
    to update the actual PLAYERN.DAT table. }
  With PLAYERN Do Begin
    LAST_NAME := Pad(OSFLastName.Text,16);
    FIRST_NAME := Pad(OSFFirstName.Text,16);
    PLAYER_NO := Pad(OSFPlayerNo.Text,7);
    STREET1 := Pad(OSFStreet1.Text,30);
    STREET2 := Pad(OSFStreet2.Text,26);
    CITY := Pad(OSFCity.Text,16);
    STATE := Pad(LMDHLCBState.Text,2);
    ZIP := Pad(OPFZip.Text,10);
    COUNTRY := Pad(LMDHLCBCountry.Text,2);
    EMAIL := Pad(OSFEmail.Text,40);
    PHONE := Pad(OPFPhone.GetStrippedEditString,20);
    ACBL_RANK := Pad(LMDHLCBRank.Text,1);
    CAT_A := Pad(OPFCatA.Text,1);
    CAT_B := Pad(OPFCatB.Text,1);
    MAIL_CODE := Pad(LMDHLCBMail.Text,1);
    UNIT_NO := FixNumField(OSFUnitNo.Text,3);
    DISTRICT_NO := FixNumField(OSFDistrictNo.Text,2);
    GENDER := Pad(LMDHLCBGender.Text,1);
    FEE_TYPE := Pad(LMDHLCBFee.Text,1);
    START_DATE := Pad(fStripDateField(ODEStartDate.Text),8);
    PAID_THRU := Pad(OPFPaidThru.GetStrippedEditString,6);
    LOCAL_DATE := Pad(fStripDateField(OPFLocalActiveDate.Text),8);
    ACBL_DATE := Pad(fStripDateField(OPFAcblActiveDate.Text),8);
    ACBL_UPDATE_DATE := Pad(fStripDateField(OPFLastACBLUpdate.Text),8);
  End;
End;

Procedure TfrmPlayers.ComboBoxChange(Sender: TObject);
Var
  iForLoop, iPos, iTxtLen: Integer;
  sTempStr: String;
Begin
  { COMBOBOXCHANGE: TLMDHeaderListComboBox OnChange Routine }
  If (Sender As TLMDHeaderListComboBox).Focused Then Begin
    With (Sender As TLMDHeaderListComboBox) Do Begin
      sTempStr := Text;
      For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
        iPos := Pos(sTempStr, ListBox.Items[iForLoop]);
        iTxtLen := Length(sTempStr);
        If (iPos = 1) Or (iPos = 2) Then Begin
          ListBox.ItemIndex := iForLoop;
          Break;
        End;
      End;
    End;
  End;
End;

Procedure TfrmPlayers.ComboBoxEnter(Sender: TObject);
Begin
  { COMBOBOXENTER: TLMDHeaderListComboBox OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    DroppedDown := True;
    ListBox.AlternateColors := True;
    Refresh;
    If ListBox.ItemIndex <> -1 Then ListBox.ItemIndex := fFindCBItemIndex(Sender, Text)
    Else ListBox.ItemIndex := 1;
  End;
End;

Function TfrmPlayers.fFindCBItemIndex(cbName: TObject; sText2Find: String): Integer;
Var
  iForLoop: Integer;
  sTempStr: String;
Begin
  { FFINDCBITEMINDEX: Used to see if there is a matching entry in the current
    TLMDHeaderListComboBox object. Returns the index position if there is or 0 if not. }
  Result := 1;
  With (cbName As TLMDHeaderListComboBox) Do Begin
    sTempStr := Text;
    For iForLoop := 0 To (ListBox.Items.Count - 1) Do Begin
      If Pos(sTempStr, ListBox.Items[iForLoop]) = 1 Then Begin
        Result := iForLoop;
        Break;
      End;
    End;
  End;
End;

Function duplicate_player(Const number: String; Fno, Kno: integer; Func: String): boolean;
Var
  tempkey: keystr;
  trecno: longint;
  Pn: String[7];
Begin
  duplicate_player := false;
  pn := number;
  If Not player_check(pn, pnPoundACBL) Then exit;
  tempkey := Pads(player_key(number), 7);
  findkey(IdxKey[Fno, Kno]^, trecno, tempkey);
  If ok
    And ((trecno <> recno[Playern_no]) Or ((func = 'A') Or (func = 'C'))) Then
    duplicate_player := true;
  ok := true;
End;

Procedure TfrmPlayers.LMDHLCBEnter(Sender: TObject);
Begin
  { TLMDHLCBCountry OnEnter Routine }
  With (Sender As tLMDHeaderListComboBox) Do Begin
    ListBox.AlternateColors := True;
  End;
End;

Procedure TfrmPlayers.SpeedButton1Click(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmPlayers.F4KeyExecute(Sender: TObject);
Begin
  wState := WindowState;
  // -----------------------------------------------------
  { Turn off the Action Managers for frmPlayers }
  ActionManager1.State := asSuspended;
  ActionManager2.State := asSuspended;
  ActionManager3.State := asSuspended;
  // -----------------------------------------------------
  { Set the Global Variable Filno to the table Percentages }
  FilNo := Attend_no;
  { Hide the Players Screen }
  frmPlayers.Hide;
  // -----------------------------------------------------
  frmAttendance := tfrmAttendance.Create(Self);
  frmAttendance.ShowModal;
  frmAttendance.Free;
  // -----------------------------------------------------
  { Show the Players Screen }
  frmPlayers.Show;
  HelpContext := 4;
  // -----------------------------------------------------
  { Set the Global Variable Filno to the main table PlayerNo }
  FilNo := Playern_no;
  Scrno := 1;
  // -----------------------------------------------------
  { Turn on the Action Managers for frmPlayers }
  ActionManager1.State := asNormal;
  ActionManager2.State := asSuspended;
  ActionManager3.State := asSuspended;
  // -----------------------------------------------------
  WindowState := wState;
  Position := poScreenCenter;
End;

Procedure TfrmPlayers.pGBGroups(bEnable: Boolean);
Begin
  { Turn the Player Info Data Area ON }
  { Allow or Deny access to the Player Information fields }
  { Set the state of ActionManager1 for the form players GBMenu }
  If bEnable Then ActionManager1.State := asSuspended Else
    ActionManager1.State := asNormal;
  If bEnable Then ActionManager5.State := asNormal Else
    ActionManager5.State := asSuspended;
  // ----------------------------------------------------------------------
  If bEnable Then GBMenu.Hide Else GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := Not bEnable;
  GroupsMenu.Visible := bEnable;
  // ----------------------------------------------------------------------
  GBGroups.Enabled := bEnable;
  // ----------------------------------------------------------------------
  Label37.Caption := '';
  Label37.Visible := bEnable;
  Label38.Visible := bEnable;
  Label39.Visible := bEnable;
  iSGGroupsCol := 0;
  { OPFGroupCode.TEXT := SGGROUPS.CELLS[COL, ROW] }
  // --------------------------------------------------
  OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
  OPFGroupCode.Visible := bEnable;
  OPFGroupCode.Enabled := bEnable;
  // --------------------------------------------------
  OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  OPFGroupNo.Visible := bEnable;
  OPFGroupNo.Enabled := bEnable;
  If bEnable Then Filno := PlayerGr_no Else Filno := PlayerN_no;
  If Not bEnable Then HelpContext := 4;
End;

Procedure TfrmPlayers.SGGroupsDblClick(Sender: TObject);
Begin
  { If the string grid for grids is double-clicked, cheack to make sure the cell selected
    is not empty. If it is not, then store the string grid column number and store the values in
    the cells to the mask edit fields so they can be editied. }
  If Trim(sgGroups.Cells[SGGroups.Col, SGGroups.Row]) <> '' Then Begin
    iSGGroupsCol := SGGroups.Col;
    OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
    OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
    pFindGroup(OPFGroupCode.Text, OPFGroupNo.Text);
  End;
End;

Procedure TfrmPlayers.OPFGroupCodeExit(Sender: TObject);
Begin
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  If (iDataMode <> 0) Then Begin
    If Not bEscKeyUsed Then Begin
      PlayerGr.GROUP := OPFGroupCode.Text;
      If {Not} empty(OPFGroupCode.Text) or fDuplicateKey(Filno, 1, iDAtaMode,Pad(OPFGroupCode.Text,3)) Then Begin
        ErrBox('Group code is blank or already exists.', mc, 0);
        OPFGroupCode.Text := '';
        OPFGroupCode.SetFocus;
      End
      Else If trim(OPFGroupCode.Text) = '' Then Begin
        Case iDataMode Of
          1, 3: pAbortGroupAdd; // Adding a record
          2: pAbortGroupEdit; // Editing a record
        End;
      End;
    End
    Else Begin
      Case iDataMode Of
        1, 3: pAbortGroupAdd; // Adding a record
        2: pAbortGroupEdit; // Editing a record
      End;
    End;
    sgButtAll(SGGroups.Cells[0, 0] <> '');
  End;
End;

Procedure TfrmPlayers.pAbortGroupAdd;
Begin
  sgGroups.Row := 0;
  sgGroups.Col := 0;
  iSGGroupsCol := SGGroups.Col;
  OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
  OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  sgGroups.SetFocus;
End;

Procedure TfrmPlayers.pAbortGroupEdit;
Begin
  OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
  OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  sgGroups.SetFocus;
End;

Procedure TfrmPlayers.pLoadGroupRec;
Var
  ksRecNoKey, ksTempKey: keystr;
  iCol: integer;
Begin
  ksRecNoKey := GetKey(Playern_no, 3); // Key 3 = RECORD_NO
  ksTempKey := ksRecNoKey;
  { Search for records in the player group table that match the Player Number key of the current Player Number record }
  SearchKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
  { While the Player Number Key matches, store this players groups to the string grid for viewing }
  While ok And CompareKey(ksTempKey, ksRecNoKey) Do Begin
    { Set the pas variable Filno to Table Number we are working with, Filno is used Globally }
    GetARec(Playergr_no); // Store the record
    If (Playergr.GROUP = sgGroups.Cells[iSGGroupsCol, 0]) And
      (Playergr.GROUP_NO = sgGroups.Cells[iSGGroupsCol, 1]) Then Begin
      { Save the changes }
      sgGroups.Cells[iSGGroupsCol, 0] := OPFGroupCode.Text;
      sgGroups.Cells[iSGGroupsCol, 1] := OPFGroupNo.Text;
      Playergr.GROUP := OPFGroupCode.Text;
      Playergr.GROUP_NO := OPFGroupNo.Text;
      PutARec(PlayerGr_no);
      Break;
    End;
    // Go to the next record in the table
    NextKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
  End;
End;

Procedure TfrmPlayers.pAddGroup;
Var
  iForLoop: Integer;
Begin
  Label37.Caption := 'Add Group';
  { Store blanks to edit fields }
  OPFGroupCode.Text := '';
  OPFGroupNo.Text := '';
  { Set the cursor focus to the Group Code field }
  OPFGroupCode.SetFocus;
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 1;
  sDataMode := 'A';
End;

Procedure TfrmPlayers.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If key = VK_F1 Then Begin
    If HelpContext > 0 Then Begin
      bF1KeyUsed := True;
    End
    Else bF1KeyUsed := False;
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Begin
    Key := 0;
    bF1KeyUsed := False;
  End;
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    If iDataMode > 0 Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Key := 0;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    { At least the Last Name Field must be filled in... }
//    If iDataMode > 0 Then x
    If Trim(OSFLastName.Text) <> '' Then LMDButPlayerInfoDone.Click Else pAbortChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmPlayers.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If key = VK_F1 Then Begin
    If HelpContext > 0 Then Begin
      bF1KeyUsed := True;
    End
    Else bF1KeyUsed := False;
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Begin
    Key := 0;
    bF1KeyUsed := False;
  End;

  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    { At least the Last Name Field must be filled in... }
    If iDataMode > 0 Then If Trim(OSFLastName.Text) <> '' Then LMDButPlayerInfoDone.Click Else pAbortChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmPlayers.pDateSetFormBack;
Begin
  // Set the forms Height and Width back to what it was
  frmplayers.Height := iTPDFormHeight;
  frmplayers.Width := iTPDFormWidth;
  frmplayers.Left := iTPDFormLeft;
  frmplayers.Top := iTPDFormTop;
End;

Procedure TfrmPlayers.pDateSaveFormInfo;
Begin
  // Save the forms Height, Width, Left Side Position and Top Position
  iTPDFormHeight := frmplayers.Height;
  iTPDFormWidth := frmplayers.Width;
  iTPDFormLeft := frmplayers.Left;
  iTPDFormTop := frmplayers.Top;
End;

Procedure TfrmPlayers.pSetMaxLengthOfFields;
Begin
  frmplayers.OSFLastName.MaxLength := (SizeOf(Playern.LAST_NAME) - 1);
  frmplayers.OSFFirstName.MaxLength := (SizeOf(Playern.FIRST_NAME) - 1);
  frmplayers.OSFPlayerNo.MaxLength := (SizeOf(Playern.PLAYER_NO) - 1);
  frmplayers.OSFStreet1.MaxLength := (SizeOf(Playern.STREET1) - 1);
  frmplayers.OSFStreet2.MaxLength := (SizeOf(Playern.STREET2) - 1);
  frmplayers.OSFCity.MaxLength := (SizeOf(Playern.CITY) - 1);
  frmplayers.LMDHLCBState.MaxLength := (SizeOf(Playern.STATE) - 1);
  frmplayers.OPFZip.MaxLength := (SizeOf(Playern.ZIP) - 1);
  frmplayers.LMDHLCBCountry.MaxLength := (SizeOf(Playern.COUNTRY) - 1);
  frmplayers.OSFEmail.MaxLength := (SizeOf(Playern.EMAIL) - 1);
  frmplayers.OPFPhone.MaxLength := 20;
  // ------------------------------------------------------------------------------
  frmplayers.LMDHLCBRank.MaxLength := (SizeOf(Playern.ACBL_RANK) - 1);
  frmplayers.OPFCatA.MaxLength := (SizeOf(Playern.CAT_A) - 1);
  frmplayers.OPFCatB.MaxLength := (SizeOf(Playern.CAT_B) - 1);
  frmplayers.LMDHLCBMail.MaxLength := (SizeOf(Playern.MAIL_CODE) - 1);
  frmplayers.OSFUnitNo.MaxLength := (SizeOf(Playern.UNIT_NO) - 1);
  frmplayers.OSFDistrictNo.MaxLength := (SizeOf(Playern.District_no) - 1);
  frmplayers.LMDHLCBGender.MaxLength := (SizeOf(Playern.GENDER) - 1);
  frmplayers.LMDHLCBFee.MaxLength := (SizeOf(Playern.Fee_type) - 1);
  frmplayers.OPFPaidThru.MaxLength := (SizeOf(Playern.Paid_Thru) - 1);
End;

Procedure TfrmPlayers.FormKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Key := 0;
End;

Procedure TfrmPlayers.FormKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Key := 0;
End;

Procedure TfrmPlayers.FormResize(Sender: TObject);
Begin
  If frmPlayers.AutoSize Then frmPlayers.AutoSize := False;
  If frmPlayers.AutoScroll Then frmPlayers.AutoScroll := False;
  If frmPlayers.Scaled Then frmPlayers.Scaled := False;
End;

Procedure TfrmPlayers.MailingAddressCheck;
Var
  bGoodMailingAddress: Boolean;
Begin
  bGoodMailingAddress := True;
  If Trim(frmPlayers.OSFStreet1.Text) = '' Then bGoodMailingAddress := False;
  If Trim(frmPlayers.OSFCity.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(frmPlayers.LMDHLCBState.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(frmPlayers.LMDHLCBState.Text) <> '' Then bGoodMailingAddress := False;
  If Trim(frmPlayers.OPFZip.Text) <> '' Then bGoodMailingAddress := False;
  If bGoodMailingAddress Then Begin
    If Trim(frmPlayers.LMDHLCBMail.Text) = '' Then frmPlayers.LMDHLCBMail.Text := 'M';
  End;
End;

Procedure TfrmPlayers.CATaCheck;
Begin
  If Trim(OSFPlayerNo.Text) <> '' Then Begin
    If (OSFPlayerNo.Text[1] In ['A'..'Z']) And (OPFCatA.Text = '') Then OPFCatA.Text := 'L';
  End;
End;

Procedure TfrmPlayers.pSetEvents(bSwitchEventsOn: Boolean);
Begin
  If bSwitchEventsOn Then Begin
    frmPlayers.OSFLastName.OnExit := OSFLastNameExit;
    frmPlayers.OSFPlayerNo.OnExit := OSFPlayerNoExit;
    frmPlayers.LMDHLCBState.OnExit := LMDHLCBStateExit;
    frmPlayers.LMDHLCBRank.OnExit := LMDHLCBRankExit;
    frmPlayers.LMDHLCBMail.OnExit := LMDHLCBMailExit;
    frmPlayers.LMDHLCBGender.OnExit := LMDHLCBGenderExit;
    frmPlayers.LMDHLCBFee.OnExit := LMDHLCBFeeExit;
    frmPlayers.OSFDistrictNo.OnExit := OSFDistrictNoExit;
  End
  Else Begin
    frmPlayers.OSFLastName.OnExit := Nil;
    frmPlayers.OSFPlayerNo.OnExit := Nil;
    frmPlayers.LMDHLCBState.OnExit := Nil;
    frmPlayers.LMDHLCBRank.OnExit := Nil;
    frmPlayers.LMDHLCBMail.OnExit := Nil;
    frmPlayers.LMDHLCBGender.OnExit := Nil;
    frmPlayers.LMDHLCBFee.OnExit := Nil;
    frmPlayers.OSFDistrictNo.OnExit := Nil;
  End;
End;

Procedure TfrmPlayers.pDateExit(Sender: TObject);
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
End;

Procedure TfrmPlayers.OPFGroupNoExit(Sender: TObject);
Begin
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  If (iDataMode <> 0) Then Begin
    Case iDataMode Of
      1: Begin // Add Mode
          If trim(OPFGroupCode.Text) <> '' Then Begin
            With playergr Do If (sOldGroup <> group) And Not empty(group)
              And (GroupFind(group) = 0) Then Begin
                GroupAdd(Playergr.group);
                bGroupDirty := true;
              End;
            Playergr.GROUP := OPFGroupCode.Text;
            Playergr.GROUP_NO := OPFGroupNo.Text;
            Add_Record;
          End
          Else pAbortGroupAdd; // Adding a record
        End;
      2: Begin // Edit Mode
          If trim(OPFGroupCode.Text) <> '' Then Begin
            sgGroups.Cells[iSGGroupsCol, 0] := OPFGroupCode.Text;
            sgGroups.Cells[iSGGroupsCol, 1] := OPFGroupNo.Text;
            With playergr Do If (sOldGroup <> group) And Not empty(group)
              And (GroupFind(group) = 0) Then Begin
                GroupAdd(Playergr.group);
                bGroupDirty := true;
              End;
            Playergr.GROUP := OPFGroupCode.Text;
            Playergr.GROUP_NO := OPFGroupNo.Text;
            PutARec(Filno);
          End
          Else pAbortGroupEdit; // Editing a record
        End;
      3: Begin // Copy Mode
          If trim(OPFGroupCode.Text) <> '' Then Begin
            With playergr Do If (sOldGroup <> group) And Not empty(group)
              And (GroupFind(group) = 0) Then Begin
                GroupAdd(Playergr.group);
                bGroupDirty := true;
              End;
            Playergr.GROUP := OPFGroupCode.Text;
            Playergr.GROUP_NO := OPFGroupNo.Text;
            Add_Record;
          End
          Else pAbortGroupEdit; // Copying a record
        End;
    End;
    If bGroupDirty Then Begin
      GroupWrite;
      bGroupDirty := false;
    End;
    Top_Record;
    pFillGroups;
    SGGroups.SetFocus;
    sgButtAll(SGGroups.Cells[0, 0] <> '');
  End;
End;

Procedure TfrmPlayers.Action1Execute(Sender: TObject);
Begin
  frmTblsCurrOpen := TfrmTblsCurrOpen.Create(Self);
  frmTblsCurrOpen.ShowModal;
  frmTblsCurrOpen.Free;
End;

Procedure TfrmPlayers.pSetFunctionKeys;
Begin
  { Set Function Key Labels, Visibility, Hints and Actions for Tournament and Club Modes }
  If sPlayMode = 'T' Then Begin
    { TOURNAMENT MODE }
    { F2 Key }
    StaticTextF2Key.Caption := 'Edit MPs (Total, YTD, MTD, Recent) ';
    ButtonF2Key.Hint := sStandFuncKeyHint + 'Edit MPs (Total, YTD, MTD, Recent) ';
    { F5 Key }
    StaticTextF5Key.Caption := 'Edit/View Event Ranks/Qual. for this player ';
    ButtonF5Key.Hint := sStandFuncKeyHint + 'Edit/View Event Ranks/Qual. for this player ';
  End
  Else Begin
    { CLUB MODE }
    { F2 Key }
    StaticTextF2Key.Caption := 'Edit MPs for this player (Club use only) ';
    ButtonF2Key.Hint := sStandFuncKeyHint + 'Edit MPs for this player (Club use only) ';
    { F5 Key }
    StaticTextF5Key.Caption := 'Edit/View Handicap Percentages for this Player ';
    ButtonF5Key.Hint := sStandFuncKeyHint + 'Edit/View Handicap Percentages for this Player ';
    // ======================================================================================
    { The F6 Key is not used in Club Mode, so turn off the button, it's label and events}
    StaticTextF6Key.Visible := False;
    ButtonF6Key.Visible := False;
    ButtonF6Key.Action.OnExecute := Nil;
    ButtonF6Key.OnClick := Nil;
    // ======================================================================================
  End;
  { F1 Key }
  ButtonF1Key.Hint := sStandFuncKeyHint + StaticTextF1Key.Caption;
  { F3 Key }
  ButtonF3Key.Hint := sStandFuncKeyHint + StaticTextF3Key.Caption;
  { F4 Key }
  ButtonF4Key.Hint := sStandFuncKeyHint + StaticTextF4Key.Caption;
  { F6 Key }
  ButtonF6Key.Hint := sStandFuncKeyHint + StaticTextF6Key.Caption;
  { F7 Key }
  ButtonF7Key.Hint := sStandFuncKeyHint + StaticTextF7Key.Caption;
  { F8 Key }
  ButtonF8Key.Hint := sStandFuncKeyHint + StaticTextF8Key.Caption;
End;

Procedure TfrmPlayers.MasterPointsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    pAbortMPChanges;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    pSaveMPChanges;
  End;
End;

Procedure TfrmPlayers.pAbortMPChanges;
Begin
  { Read data base file Playern_no.  Record number in Recno[Playern_no]
    Reload the record before any changes where made. }
  GetARec(Playern_no);
  { Calculate the Players Total Points }
  sCompStr := fShowPoints(4);
  OPFPTotal.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points YTD }
  sCompStr := fShowPoints(3);
  OPFPYTD.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Points MTD }
  sCompStr := fShowPoints(2);
  OPFPMTD.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Recent Points }
  sCompStr := fShowPoints(1);
  OPFPRecent.Text := sCompStr;
  // ------------------------------------------------------------------------------------
  { Calculate the Players Eligibility Points }
  sCompStr := fShowPoints(0);
  pStoreMPoints;
  OPFPEligibility.Text := sCompStr;
  pMasterPointsDone;
End;

Procedure TfrmPlayers.pSaveMPChanges;
Var
  J, k: byte;
Begin
  aPpoints[1] := OPFPEligibility.AsExtended;
  aPpoints[2] := OPFPRecent.AsExtended;
  aPpoints[3] := OPFPMTD.AsExtended;
  aPpoints[4] := OPFPYTD.AsExtended;
  aPpoints[5] := OPFPTotal.AsExtended;
  For J := 1 To 5 Do Begin
    For k := 1 To 5 Do SetPoints(k);
  End;

  { Save the Changes back to the Database }
  PutARec(Playern_no);
  { Set the form back to normal view mode }
  pMasterPointsDone;
  pLoadTPFields;
End;

Procedure TfrmPlayers.pMasterPointsDone;
Begin
  { Set the forms Height and Width back to what it was }
  //pSetFormBack;
  { Make the GroupBox GBMasterPoints read only, all Maskedits are in this GB, so they by
    default, are read only also }
  pGBMasterPointsReadOnly(True);
  { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  iDataMode := 0;
  sDataMode := Null;
  bEscKeyUsed := False;
  ActionManager1.State := asNormal;
  // ----------------------------------------------------------------------
  GBMenu.Show;
  // ----------------------------------------------------------------------
  GBMenu.Enabled := True;
  //GBMenu.Align := alBottom;
  // ----------------------------------------------------------------------
  {$IFDEF FormResize}
  { Must turn off ResizeKit before turning Autosize on, form will do some strange things if not }
  //If ResizeKit1.Enabled Then ResizeKit1.Enabled := False;
  {$ENDIF}
  //  frmplayers.AutoSize := True;
  //  frmplayers.AutoSize := False;
  {$IFDEF FormResize}
  { Turn auto resizing on or off for the form }
  //ResizeKit1.Enabled := True;
  {$ENDIF}
  pResetFldAttrib;
  ButNext.SetFocus;
End;

Procedure TfrmPlayers.pDateFieldKeyPress(Sender: TObject; Var Key: Char);
Begin
  pEditFieldKeyPress(Sender, Key);
End;

Procedure TfrmPlayers.OPFPTotalExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[5] := OPFPTotal.AsExtended;
  J := 5;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayers.OPFPYTDExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[4] := OPFPYTD.AsExtended;
  J := 4;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayers.OPFPMTDExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[3] := OPFPMTD.AsExtended;
  J := 3;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayers.OPFPRecentExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[2] := OPFPRecent.AsExtended;
  J := 2;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
End;

Procedure TfrmPlayers.OPFPEligibilityExit(Sender: TObject);
Var
  J, k: byte;
Begin
  aPpoints[1] := OPFPEligibility.AsExtended;
  J := 1;
  FixPoints(j);
  For k := 1 To 5 Do SetPoints(k);
  pRefreshMPFields;
  { Save any changes to the Master Points }
  pSaveMPChanges;
End;

Procedure TfrmPlayers.FixPoints(Const j: byte);
Var
  pp: real;
  k: byte;
Begin
  If j = 1 Then aPpoints[5] := aPpoints[5] + aPpoints[1] - rOldepts;
  If j < 3 Then exit;
  If aPpoints[j - 1] > aPpoints[j] Then aPpoints[j - 1] := aPpoints[j];
  If (j = 5) And (aPpoints[1] > aPpoints[5]) Then aPpoints[1] := aPpoints[5];
  FixPoints(j - 1);
End;

Procedure TfrmPlayers.SetPoints(Const j: byte);
Var
  num: longint;
Begin
  With playern Do Begin
    If j > 2 Then Begin
      If appoints[j] < aPpoints[j - 1] Then aPpoints[j] := aPpoints[j - 1];
      num := Round((aPpoints[j] - aPpoints[j - 1]) * 100.0);
      If (j = 5) And (aPpoints[j] < aPpoints[1]) Then aPpoints[j] := aPpoints[1];
    End
    Else num := Round(aPpoints[j] * 100.0);
    Case j Of
      1: Elig_pts := Num2Key(num, 4);
      2: Tourn_pts := Num2Key(num, 4);
      3: M_T_D := Num2Key(num, 4);
      4: Y_T_D := Num2Key(num, 4);
      5: Tot_points := Num2Key(num, 4);
    End;
  End;
End;

Procedure TfrmPlayers.pStoreMPoints;
Var
  j: byte;
Begin
  With playern Do Begin
    aOldpoints[1] := Elig_pts;
    aOldpoints[2] := Tourn_pts;
    aOldpoints[3] := M_T_D;
    aOldpoints[4] := Y_T_D;
    aOldpoints[5] := Tot_points;
    For j := 1 To 5 Do Begin
      aPpoints[j] := Key2Num(aOldpoints[j]) / 100.0;
      If j = 1 Then roldepts := aPpoints[1];
      If j > 2 Then aPpoints[j] := aPpoints[j] + aPpoints[j - 1];
    End;
  End;
End;

Procedure TfrmPlayers.pRefreshMPFields;
Begin
  OPFPEligibility.Text := Real2Str((aPpoints[1]), 9, 2);
  OPFPRecent.Text := Real2Str((aPpoints[2]), 9, 2);
  OPFPMTD.Text := Real2Str((aPpoints[3]), 9, 2);
  OPFPYTD.Text := Real2Str((aPpoints[4]), 9, 2);
  OPFPTotal.Text := Real2Str((aPpoints[5]), 9, 2);
End;

Procedure TfrmPlayers.pFixPoints;
Begin
  If Valu(OPFPMTD.Text) < Valu(OPFPRecent.Text) Then
    OPFPMTD.Text := OPFPRecent.Text;
  If Valu(OPFPYTD.Text) < Valu(OPFPMTD.Text) Then
    OPFPYTD.Text := OPFPMTD.Text;
  If Valu(OPFPTotal.Text) < Valu(OPFPYTD.Text) Then
    OPFPTotal.Text := OPFPYTD.Text;
  If Valu(OPFPTotal.Text) < Valu(OPFPEligibility.Text) Then
    OPFPTotal.Text := OPFPEligibility.Text;
End;

Function TfrmPlayers.fShowPoints(Const Ptype: byte): String;
Var
  num: longint;
Begin
  If UsedRecs(DatF[Playern_no]^) < 1 Then num := 0
  Else With playern Do Begin
      If Ptype < 1 Then num := Key2Num(Elig_pts)
      Else Begin
        num := Key2Num(Tourn_pts);
        If Ptype > 1 Then Inc(num, Key2Num(M_T_D));
        If Ptype > 2 Then Inc(num, Key2Num(Y_T_D));
        If Ptype > 3 Then Inc(num, Key2Num(Tot_points));
      End;
    End;
  fShowPoints := Real2Str(num / 100.0, 8, 2);
End;

Procedure TfrmPlayers.Action2Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPlayers.Action3Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPlayers.Action4Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmPlayers.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
  If UpperCase(sType) = 'S' Then
    pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'P' Then
    pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
  Else If UpperCase(sType) = 'C' Then
    pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
  Else If UpperCase(sType) = 'D' Then
    pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
  Else If UpperCase(sType) = 'H' Then
    pLMDHCBFieldAvail(bDoedit, (oSender As TLMDHeaderListComboBox))
  Else MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0);
  // -------------------------------------------------------------------------------------------------------
  If bDisplay_Rec Then Begin
    { Load all information to the appropriate field from the current record }
    pLoadTPFields;
  End;
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'P') Then Begin
      If Not (oSender As TOvcPictureField).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'D') Then Begin
      If Not (oSender As TOvcDateEdit).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'H') Then Begin
      If Not (oSender As TLMDHeaderListComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'C') Then Begin
      If Not (oSender As TOvcComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure TfrmPlayers.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayers.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayers.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayers.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayers.pLMDHCBFieldAvail(bAvail: Boolean; oOSField: TLMDHeaderListComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmPlayers.pResetFldAttrib;
Var
  iComponent: integer;
Begin
  With Self Do Begin
    For iComponent := 0 To ComponentCount - 1 Do Begin
      If Components[iComponent].ClassType = TOvcSimpleField Then
        pOSFieldAvail(True, (Components[iComponent] As TOvcSimpleField))
      Else If Components[iComponent].ClassType = TOvcPictureField Then
        pOPFieldAvail(True, (Components[iComponent] As TOvcPictureField))
      Else If Components[iComponent].ClassType = TOvcDateEdit Then
        pODEFieldAvail(True, (Components[iComponent] As TOvcDateEdit))
      Else If Components[iComponent].ClassType = TOvcComboBox Then
        pOCBFieldAvail(True, (Components[iComponent] As TOvcComboBox));
    End;
  End;
End;

Procedure TfrmPlayers.OSFLastNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.LAST_NAME := Pad(OSFLastName.Text,16);
  bDoEdit := CustomPLEdit(Scrno, 1, Playern.LAST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFLastNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      { Validation of Last Name field when exiting it }
      If trim(OSFLastName.Text) = '' Then Begin
        If MessageDlg('Last Name must NOT be blank! ' + #13 + #10 + '' + #13 + #10 +
          '        Correct this error?', mtError, [mbYes, mbNo], 0) = MRNO Then Begin
          pAbortChanges;
        End
        Else OSFLastName.SetFocus
      End
      Else Begin
        Playern.LAST_NAME := Pad(OSFLastName.Text,16);
        bDoEdit := CustomPLEdit(Scrno, 1, Playern.LAST_NAME, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.OSFFirstNameEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.FIRST_NAME := Pad(OSFFirstName.Text,16);
  bDoEdit := CustomPLEdit(Scrno, 2, Playern.FIRST_NAME, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFFirstNameExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.FIRST_NAME := Pad(OSFFirstName.Text,16);
      bDoEdit := CustomPLEdit(Scrno, 2, Playern.FIRST_NAME, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayers.OSFPlayerNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
  bDoEdit := CustomPLEdit(Scrno, 3, Playern.PLAYER_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFPlayerNoExit(Sender: TObject);
Var
  osPlayer_no: OpenString;
  DataMode: Char;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      osPlayer_no := OSFPlayerNo.Text;
      DataMode := sDataMode;
      { Check for a duplicate Player Number }
      If Not (fcheck_player(osPlayer_no, DataMode)) Then Begin
        If PlayerError = 2 Then
          ErrBox('This ACBL Player Number already exists in this database!', mc, 0)
        Else If PlayerError = 1 Then ErrBox('Invalid ACBL Player Number!', mc, 0);
        OSFPlayerNo.SetFocus;
      End
      Else Begin
        Playern.PLAYER_NO := Pad(OSFPlayerNo.Text,7);
        bDoEdit := CustomPLEdit(Scrno, 3, Playern.PLAYER_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.OSFStreet1Enter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.Street1 := Pad(OSFStreet1.Text,30);
  bDoEdit := CustomPLEdit(Scrno, 4, Playern.Street1, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFStreet1Exit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.Street1 := Pad(OSFStreet1.Text,30);
      bDoEdit := CustomPLEdit(Scrno, 4, Playern.Street1, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayers.OSFStreet2Enter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.Street2 := Pad(OSFStreet2.Text,26);
  bDoEdit := CustomPLEdit(Scrno, 5, Playern.Street2, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFStreet2Exit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.Street2 := Pad(OSFStreet2.Text,26);
      bDoEdit := CustomPLEdit(Scrno, 5, Playern.Street2, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayers.OSFCityEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.CITY := Pad(OSFCity.Text,16);
  bDoEdit := CustomPLEdit(Scrno, 6, Playern.CITY, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFCityExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.CITY := Pad(OSFCity.Text,16);
      bDoEdit := CustomPLEdit(Scrno, 6, Playern.CITY, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'S');
    End;
  End;
End;

Procedure TfrmPlayers.LMDHLCBStateEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.STATE := Pad(LMDHLCBState.Text,2);
  bDoEdit := CustomPLEdit(Scrno, 7, Playern.STATE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayers.LMDHLCBStateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidStateCode(Playern.Country, LMDHLCBState.Text)) Then Begin
        ErrBox('Invalid State/Province code.', mc, 0);
        LMDHLCBState.Text := '';
        LMDHLCBState.SetFocus;
      End
      Else Begin
        Playern.STATE := Pad(LMDHLCBState.Text,2);
        bDoEdit := CustomPLEdit(Scrno, 7, Playern.STATE, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'H');
        LMDHLCBCountry.Text := Playern.Country;
      End;
    End;
  End;
End;

Procedure TfrmPlayers.OPFZipEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.ZIP := Pad(OPFZip.Text,10);
  bDoEdit := CustomPLEdit(Scrno, 8, Playern.ZIP, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayers.OPFZipExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.ZIP := Pad(OPFZip.Text,10);
      bDoEdit := CustomPLEdit(Scrno, 8, Playern.ZIP, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmPlayers.LMDHLCBCountryEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.Country := Pad(LMDHLCBCountry.Text,2);
  bDoEdit := CustomPLEdit(Scrno, 9, Playern.Country, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayers.LMDHLCBCountryExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (ValidCountryCode(LMDHLCBCountry.Text, true)) Then Begin
        ErrBox('Invalid Country Code', mc, 0);
        LMDHLCBCountry.Text := '';
        LMDHLCBCountry.SetFocus;
      End
      Else Begin
        Playern.Country := Pad(LMDHLCBCountry.Text,2);
        bDoEdit := CustomPLEdit(Scrno, 9, Playern.Country, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'H');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.OSFEmailEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.email := Pad(OSFEmail.Text,40);
  bDoEdit := CustomPLEdit(Scrno, 10, Playern.email, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFEmailExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (Empty(OSFEmail.Text) Or (ValidEmailAddress(OSFEmail.Text, 40) > 0)) Then Begin
        ErrBox('Email address is not valid', mc, 0);
        OSFEmail.SetFocus;
      End
      Else Begin
        Playern.email := Pad(OSFEmail.Text,40);
        bDoEdit := CustomPLEdit(Scrno, 10, Playern.email, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.LMDHLCBRankEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 12, Playern.ACBL_RANK, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayers.LMDHLCBRankExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Ranking Codes:

         Rookie (blank)     0 - 5
      A  Junior Master      5 - 20
      B  Club Master       20 - 50
      C  Sectional Master  50 - 100
      D  Regional Master  100 - 200
      E  NAC Master       200 - 300
      F  Life Master      300 - 500
      G  Bronze LM        500 - 1000
      H  Silver LM       1000 - 2500
      I  Gold LM         2500 - 5000
      J  Diamond LM      5000 - 7500
      K  Emerald LM      7500 - 10000
      L  Platinum LM     over   10000
      M  Grand LM NABC   over   10000
      X  Rank not known
      S  Suspended
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.ACBL_RANK := Pad(LMDHLCBRank.Text,1);
      bDoEdit := CustomPLEdit(Scrno, 12, Playern.ACBL_RANK, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBRank.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sRankCode) Do Begin
          If LMDHLCBRank.Text = libdata.sRankCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then LMDHLCBRank.Text := '';
    End;
  End;
End;

Procedure TfrmPlayers.OPFCatAEnter(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  bDoEdit := True;
  Playern.CAT_A := Pad(OPFCatA.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 13, Playern.CAT_A, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayers.OPFCatAExit(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidCatA(OPFCatA.Text)) Then Begin
        ErrBox('Invalid Cat A code...', MC, 0);
        OPFCatA.Text := '';
      End
      Else Begin
        Playern.CAT_A := Pad(OPFCata.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 13, Playern.CAT_a, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.OPFCatBEnter(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  bDoEdit := True;
  Playern.CAT_B := Pad(OPFCatB.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 13, Playern.CAT_B, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayers.OPFCatBExit(Sender: TObject);
Begin
  { Used 13 for the field code since it was not assigned in I_Update.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.CAT_B := Pad(OPFCatB.Text,1);
      bDoEdit := CustomPLEdit(Scrno, 13, Playern.CAT_B, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'P');
    End;
  End;
End;

Procedure TfrmPlayers.LMDHLCBMailEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 15, Playern.MAIL_CODE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayers.LMDHLCBMailExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Mail Codes:

        (blank) Mailing label will NOT be printed
      M Mailing label will be printed
      U Address unknown
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.MAIL_CODE := Pad(LMDHLCBMail.Text,1);
      bDoEdit := CustomPLEdit(Scrno, 15, Playern.MAIL_CODE, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBMail.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sMailCode) Do Begin
          If LMDHLCBMail.Text = libdata.sMailCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then LMDHLCBMail.Text := '';
    End;
  End;
End;

Procedure TfrmPlayers.OSFUnitNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
  bDoEdit := CustomPLEdit(Scrno, 16, Playern.UNIT_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFUnitNoExit(Sender: TObject);
Var
  osUnitNo, osDistNo: OpenString;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      osUnitNo := OSFUnitNo.Text;
      osDistNo := OSFDistrictNo.Text;
      If Not (Groups.ValidUnit(osUnitNo, osDistNo)) Then Begin
        ErrBox('Invalid Unit number', mc, 0);
        OSFUnitNo.Text := '';
        OSFDistrictNo.Text := '';
        OSFUnitNo.SetFocus;
      End
      Else Begin
        Playern.UNIT_NO := FixNumField(OSFUnitNo.Text,3);
        OSFDistrictNo.Text := osDistNo;
        Playern.District_no := FixNumfield(OSFDistrictNo.Text,2);
        bDoEdit := CustomPLEdit(Scrno, 16, Playern.UNIT_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.OSFDistrictNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.District_no := FixNumField(OSFDistrictNo.Text,2);
  bDoEdit := CustomPLEdit(Scrno, 17, Playern.District_no, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmPlayers.OSFDistrictNoExit(Sender: TObject);
Var
  osDistrictNo: OpenString;
Begin
  osDistrictNo := OSFDistrictNo.Text;
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (fValidDistrict(osDistrictNo)) Then Begin
        ErrBox('Invalid District number', mc, 0);
        OSFDistrictNo.Text := '';
        OSFDistrictNo.SetFocus;
      End
      Else Begin
        Playern.District_no := FixNumField(OSFDistrictNo.Text,2);
        bDoEdit := CustomPLEdit(Scrno, 17, Playern.District_no, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.LMDHLCBGenderEnter(Sender: TObject);
Begin
  { Used 18 for the field code since it was not assigned in I_Update.Inc }
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.GENDER := Pad(LMDHLCBGender.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 18, Playern.GENDER, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayers.LMDHLCBGenderExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Gender Codes:
        (blank) Unknown
      F Female
      M Male
  }
  { Used 18 for the field code since it was not assigned in I_Update.Inc }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.GENDER := Pad(LMDHLCBGender.Text,1);
      bDoEdit := CustomPLEdit(Scrno, 18, Playern.GENDER, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If LMDHLCBGender.Text <> ' ' Then Begin
        bValid := False;
        For iForLoop := 1 To length(libdata.sGenderCode) Do Begin
          If LMDHLCBGender.Text = libdata.sGenderCode[iForLoop] Then Begin
            bValid := True;
            break
          End;
        End;
      End
      Else bValid := True;
      If Not bValid Then Begin
        ErrBox('Gender must be ( )Blank, (M)ale or (F)emale', mc, 0);
        LMDHLCBGender.SetFocus;
      End;
    End;
  End;
End;

Procedure TfrmPlayers.LMDHLCBFeeEnter(Sender: TObject);
Begin
  LMDHLCBEnter(Sender);
  bDoEdit := True;
  Playern.Fee_type := Pad(LMDHLCBFee.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 19, Playern.Fee_type, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'H');
End;

Procedure TfrmPlayers.LMDHLCBFeeExit(Sender: TObject);
Var
  iForLoop: Integer;
  bValid: Boolean;
Begin
  { Valid Player Fee Codes:
        (blank) None of the above
      H Household
      J Student
      L Life master reduced
      M LM paying regular fees
      N Non life master regular
      P Patron
      X Patron Household
  }
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      Playern.Fee_type := Pad(LMDHLCBFee.Text,1);
      bDoEdit := CustomPLEdit(Scrno, 19, Playern.Fee_type, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'H');
      If (LMDHLCBFee.Focused) Then Begin
        If LMDHLCBFee.Text <> ' ' Then Begin
          bValid := False;
          For iForLoop := 1 To length(libdata.sFeeCode) Do Begin
            If LMDHLCBFee.Text = libdata.sFeeCode[iForLoop] Then Begin
              bValid := True;
              break
            End;
          End;
        End
        Else bValid := True;
        If Not bValid Then Begin
          ErrBox('Invalid Fee Code', mc, 0);
          LMDHLCBFee.SetFocus;
        End;
      End;
    End;
  End;
End;

Procedure TfrmPlayers.ODEStartDateEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.START_DATE := ODEStartDate.Text;
  bDoEdit := CustomPLEdit(Scrno, 20, Playern.START_DATE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'D');
End;

Procedure TfrmPlayers.ODEStartDateExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      pDateExit(Sender);
      Playern.START_DATE := ODEStartDate.Text;
      bDoEdit := CustomPLEdit(Scrno, 20, Playern.START_DATE, False, bDisplay_Rec, sDataMode, Sender);
      pOnFieldLastStep(False, Sender, 'D');
    End;
  End;
End;

Procedure TfrmPlayers.OPFPaidThruEnter(Sender: TObject);
Begin
  bDoEdit := True;
  Playern.Paid_Thru := OPFPaidThru.Text;
  bDoEdit := CustomPLEdit(Scrno, 21, Playern.Paid_Thru, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'P');
End;

Procedure TfrmPlayers.OPFPaidThruExit(Sender: TObject);
Var
  sPaidThru: String;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      sPaidThru := Copy(OPFPaidThru.Text, 1, 2) + Copy(OPFPaidThru.Text, 4, 4);
      If Not (Empty(sPaidThru) Or chkdate(Copy(sPaidThru, 1, 2) + '01' + Copy(sPaidThru, 3, 4))) Then Begin
        ErrBox('Invalid month/year', mc, 0);
        OPFPaidThru.SetFocus;
      End
      Else Begin
        Playern.Paid_Thru := OPFPaidThru.Text;
        bDoEdit := CustomPLEdit(Scrno, 21, Playern.Paid_Thru, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'P');
      End;
    End;
  End;
End;

Procedure TfrmPlayers.pButtOn(bTurnOn: Boolean);
Begin
  ButCopyAction.Enabled := bTurnOn;
  ButDeleteAction.Enabled := bTurnOn;
  ButEditAction.Enabled := bTurnOn;
  ButFindAction.Enabled := bTurnOn;
  ButTopAction.Enabled := bTurnOn;
  ButLastAction.Enabled := bTurnOn;
  ButNextAction.Enabled := bTurnOn;
  ButPrevAction.Enabled := bTurnOn;
  F2Key.Enabled := bTurnOn;
  F3Key.Enabled := bTurnOn;
  F4Key.Enabled := bTurnOn;
  F5Key.Enabled := bTurnOn;
  F6Key.Enabled := bTurnOn;
  F7Key.Enabled := bTurnOn;
  F8Key.Enabled := bTurnOn;
End;

Procedure TfrmPlayers.Action5Execute(Sender: TObject);
Begin
  WindowState := wsNormal;
  Position := poScreenCenter;
End;

Procedure TfrmPlayers.pSGGroupsKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = vk_Right Then Begin
    If Trim(sgGroups.Cells[sgGroups.Col + 1, 0]) = '' Then Begin
      sgGroups.SetFocus;
      SGGroups.Col := iSGGroupsCol;
      OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
      OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
    End
    Else Next_Record;
  End;
  If key = VK_RETURN Then Begin
    key := 0;
    keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If Key = VK_ESCAPE Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode = 0 Then Begin
      pGBGroups(False);
    End
    Else Begin
      key := 0;
      { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
      iDataMode := 0;
      sDataMode := Null;
      Label37.Caption := '';
      ActionManager5.State := asNormal;
      SGGroups.SetFocus;
    End;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    If iDataMode = 0 Then
      pGBGroups(False)
    Else Begin
      OPFGroupCodeExit(Sender);
      { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
      iDataMode := 0;
      sDataMode := Null;
      Label37.Caption := '';
      ActionManager5.State := asNormal;
      SGGroups.SetFocus;
    End;
  End;
End;

Procedure TfrmPlayers.pSGGroupsKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    If iDataMode = 0 Then
      pGBGroups(False)
    Else Begin
      OPFGroupCodeExit(Sender);
      { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
      iDataMode := 0;
      sDataMode := Null;
      Label37.Caption := '';
      ActionManager5.State := asNormal;
      SGGroups.SetFocus;
    End;
  End;
End;

Procedure TfrmPlayers.SGGroupsClick(Sender: TObject);
Begin
  If sgGroups.Cells[sgGroups.Col, 0] <> '' Then Begin
    If sgGroups.Col <> iSGGroupsCol Then Begin
      iSGGroupsCol := SGGroups.Col;
      OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
      OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
      pFindGroup(OPFGroupCode.Text, OPFGroupNo.Text);
    End;
  End
  Else Begin
    SGGroups.Col := iSGGroupsCol;
    OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
    OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
    pFindGroup(OPFGroupCode.Text, OPFGroupNo.Text);
  End;
End;

Procedure TfrmPlayers.pSGGroupsKeyPress(Sender: TObject; Var Key: Char);
Begin
  If sgGroups.Col <> iSGGroupsCol Then Begin
    iSGGroupsCol := SGGroups.Col;
    OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
    OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
    pFindGroup(OPFGroupCode.Text, OPFGroupNo.Text);
  End;
End;

Procedure TfrmPlayers.GDoneExecute(Sender: TObject);
Begin
  pGBGroups(False);
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 0;
  sDataMode := Null;
  bEscKeyUsed := False;
  sgGroups.Row := 0;
  sgGroups.Col := 0;
  FilNo := Playern_no;
End;

Procedure TfrmPlayers.GAddExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  Label37.Caption := 'Add Group';
  { Store blanks to edit fields }
  OPFGroupCode.Text := '';
  OPFGroupNo.Text := '';
  PLAYERGR.GROUP := '';
  PLAYERGR.GROUP_NO := '';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 1;
  sDataMode := 'A';
  ActionManager5.State := asSuspended;
  { Set the cursor focus to the Group Code field }
  sOldGroup := '';
  OPFGroupCode.SetFocus;
End;

Procedure TfrmPlayers.GNextExecute(Sender: TObject);
Begin
  If Trim(sgGroups.Cells[sgGroups.Col + 1, 0]) <> '' Then Begin
    Next_Record;
    sgGroups.SetFocus;
    { Set the current column number }
    Inc(iSGGroupsCol);
    SGGroups.Col := iSGGroupsCol;
    OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
    OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  End;
End;

Procedure TfrmPlayers.GPrevExecute(Sender: TObject);
Begin
  If sgGroups.Col > 0 Then Begin
    Prev_Record;
    sgGroups.SetFocus;
    { Set the current column number }
    Dec(iSGGroupsCol);
    SGGroups.Col := iSGGroupsCol;
    OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
    OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  End;
End;

Procedure TfrmPlayers.GTopExecute(Sender: TObject);
Begin
  Top_Record;
  If sgGroups.Col > 0 Then Begin
    sgGroups.SetFocus;
    SGGroups.Col := 0;
    iSGGroupsCol := SGGroups.Col;
    OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
    OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  End;
End;

Procedure TfrmPlayers.GEditExecute(Sender: TObject);
Begin
  iSGGroupsCol := SGGroups.Col;
  pFindGroup(OPFGroupCode.Text, OPFGroupNo.Text);
  Label37.Caption := 'Edit Group';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 2;
  sDataMode := 'E';
  sOldGroup := Playergr.GROUP;
  ActionManager5.State := asSuspended;
  OPFGroupCode.SetFocus;
End;

Procedure TfrmPlayers.SGGroupsEnter(Sender: TObject);
Begin
  If iDataMode > 0 Then Begin
    { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    iDataMode := 0;
    sDataMode := Null;
    Label37.Caption := '';
  End;
  ActionManager5.State := asNormal;
End;

Procedure TfrmPlayers.Button1Enter(Sender: TObject);
Begin
  If iDataMode > 0 Then Begin
    { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
    iDataMode := 0;
    sDataMode := Null;
    Label37.Caption := '';
  End;
  ActionManager5.State := asNormal;
End;

Procedure TfrmPlayers.GLastExecute(Sender: TObject);
Var
  iForLoop: Integer;
Begin
  { Loop through the columns in the grid to find the last group code }
  For iForLoop := 0 To 200 Do Begin
    If Trim(sgGroups.Cells[iForLoop, 0]) <> '' Then Begin
      sgGroups.Col := iForLoop;
      sgGroups.Row := 0;
      { Store the current column number }
      iSGGroupsCol := SGGroups.Col;
    End
    Else Begin
      OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
      OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
      { Break out of the for loop }
      Break;
    End;
  End;
  Last_Record;
End;

Procedure TfrmPlayers.GCopyExecute(Sender: TObject);
Begin
  Label37.Caption := 'Copy Group';
  { Loop through the columns in the grid to find the next available blank column }
  OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
  OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 3;
  sDataMode := 'C';
  ActionManager5.State := asSuspended;
  sOldGroup := Playergr.GROUP;
  OPFGroupCode.SetFocus;
End;

Procedure TfrmPlayers.OPFGroupNoEnter(Sender: TObject);
Begin
  //  If (SGGroups.Cells[0, 0] <> '') And (iDataMode <> 0) Then Begin
  //    ActionManager5.State := asSuspended;
  //    { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  //    // iDataMode := 2;
  //    // sDataMode := 'E';
  //    // Label37.Caption := 'Edit Group';
  //  End;
End;

Procedure TfrmPlayers.OPFGroupCodeEnter(Sender: TObject);
Begin
  //  If (SGGroups.Cells[0, 0] <> '') And (iDataMode <> 0) Then Begin
  //    ActionManager5.State := asSuspended;
  //    { Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy }
  //    //iDataMode := 2;
  //    //sDataMode := 'E';
  //    //Label37.Caption := 'Edit Group';
  //  End;
End;

Procedure TfrmPlayers.GDeleteExecute(Sender: TObject);
Begin
  Filno := Playergr_no;
  Delete_Record(True, frmPlayers.HelpContext);
  Top_Record;
  pFillGroups;
  If iSGGroupsCol <> 0 Then Begin
    Dec(iSGGroupsCol);
    SGGroups.Col := iSGGroupsCol;
  End
  Else SGGroups.Col := 0;
  OPFGroupCode.Text := sgGroups.Cells[iSGGroupsCol, 0];
  OPFGroupNo.Text := sgGroups.Cells[iSGGroupsCol, 1];
  pFindGroup(OPFGroupCode.Text, OPFGroupNo.Text);
  sgButtAll(SGGroups.Cells[0, 0] <> '');
End;

Procedure TfrmPlayers.pFillGroups;
Var
  ksRecNoKey, ksTempKey: keystr;
  iCol: integer;
Begin
  { Fill The StringGrid SGGroups with any groups for this Player, clear the grid if there are none }
  { pClearGroups: Clears the entries in the StringGrid SGGroups }
  pClearGroups;
  // ---------------------------------------------------------------------------
  { Store the current records data to the string grid }
  iCol := 0;
  ksRecNoKey := GetKey(Playern_no, 3); // Key 3 = RECORD_NO
  ksTempKey := ksRecNoKey;
  { Search for records in the player group table that match the Player Number key of the current Player Number record }
  SearchKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
  { While the Player Number Key matches, store this players groups to the string grid for viewing }
  While ok And CompareKey(ksTempKey, ksRecNoKey) Do Begin
    GetARec(Playergr_no); // Store the record
    { First row stores the group }
    SGGroups.Cells[iCol, 0] := Playergr.GROUP;
    { Second row stores the group number }
    SGGroups.Cells[iCol, 1] := Playergr.GROUP_NO;
    { Go to the next record in the table }
    NextKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
    { Increment the column count for the next available column in the string grid }
    iCol := iCol + 1;
  End;
End;

Procedure TfrmPlayers.F3KeyExecute(Sender: TObject);
Begin
  FilNo := Playergr_no;
  HelpContext := 206;
  pGBGroups(True);
  SGGroups.Cells[0, 0];
  SGGroups.SetFocus;
  iSGGroupsCol := SGGroups.Col;
  Top_Record;
  sgButtAll(SGGroups.Cells[0, 0] <> '');
End;

Procedure TfrmPlayers.sgButtAll(AllButtAvail: Boolean);
Begin
  GCopy.Enabled := AllButtAvail;
  GDelete.Enabled := AllButtAvail;
  GEdit.Enabled := AllButtAvail;
  GLast.Enabled := AllButtAvail;
  GNext.Enabled := AllButtAvail;
  GPrev.Enabled := AllButtAvail;
  GTop.Enabled := AllButtAvail;
End;

Procedure TfrmPlayers.pFindGroup(sGroupCode, sGroupNo: String);
Var
  ksRecNoKey, ksTempKey: keystr;
Begin
  Top_Record;
  If (trim(Playergr.GROUP) = Trim(sGroupCode)) And (trim(Playergr.GROUP_NO) = Trim(sGroupNo)) Then
    GetARec(Playergr_no) // Store the record
  Else Begin
    ClearKey(IdxKey[Filno, 1]^);
    ksRecNoKey := GetKey(Playern_no, 3); // Key 3 = RECORD_NO
    ksTempKey := ksRecNoKey;
    { Search for records in the player group table that match the Player Number key of the current Player Number record }
    SearchKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
    { While the Player Number Key matches, store this players groups to the string grid for viewing }
    While ok And CompareKey(ksTempKey, ksRecNoKey) Do Begin
      GetARec(Playergr_no); // Store the record
      If (trim(Playergr.GROUP) = Trim(sGroupCode)) And (trim(Playergr.GROUP_NO) = Trim(sGroupNo)) Then Begin
        break;
      End;
      { Go to the next record in the table }
      NextKey(IdxKey[Playergr_no, 1]^, Recno[Playergr_no], ksTempKey);
    End;
  End;
End;

Procedure TfrmPlayers.GF1Execute(Sender: TObject);
Begin
  wState := WindowState;
  //Application.HelpCommand(HELP_CONTENTS, 0);
  pRunExtProg(frmPlayers, cfg.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
  Scrno := 1;
  WindowState := wState;
  Position := poScreenCenter;
End;

Procedure TfrmPlayers.ButtonF1KeyClick(Sender: TObject);
Begin
  keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Procedure TfrmPlayers.ODEStartDatePopupOpen(Sender: TObject);
Begin
  FreeHintWin;
End;

Function TfrmPlayers.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
  If data < 500000 Then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
End;

procedure TfrmPlayers.FormDestroy(Sender: TObject);
begin
  mHHelp.Free;
  HHCloseAll;
end;

End.

