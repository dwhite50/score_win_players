Unit formPlayerFind;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, Buttons, ActnList, XPStyleActnCtrls,
  ActnMan;

Type
  TfrmPlayerFind = Class(TForm)
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    BitBtnQuit: TBitBtn;
    Panel2: TPanel;
    MESearch: TMaskEdit;
    Label1: TLabel;
    ActionManager1: TActionManager;
    F2_Key: TAction;
    Button4: TButton;
    Label2: TLabel;
    Esc: TAction;
    Procedure BitBtnQuitClick(Sender: TObject);
    Procedure F2_KeyExecute(Sender: TObject);
    Procedure EscExecute(Sender: TObject);
    Procedure MESearchExit(Sender: TObject);
    Procedure FormShow(Sender: TObject);
    Procedure MESearchKeyPress(Sender: TObject; Var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  End;

Var
  frmPlayerFind         : TfrmPlayerFind;

Implementation

Uses DB33, Dbase, Util1, Util2, YesNoBoxU, formTPDatabase;

{$R *.dfm}

Procedure TfrmPlayerFind.BitBtnQuitClick(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPlayerFind.F2_KeyExecute(Sender: TObject);
Begin
  If pos('number', LowerCase(Label1.Caption)) > 0 Then Begin
    Label1.Caption := 'Enter Last Name, or ESC to abort';
    Label2.Caption := 'to search by ACBL Number...';
  End
  Else Begin
    Label1.Caption := 'Enter ACBL number, or ESC to abort';
    Label2.Caption := 'to search by Last name...';
  End;
  Button4.Hint := Label2.Caption;
End;

Procedure TfrmPlayerFind.EscExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmPlayerFind.MESearchExit(Sender: TObject);
Var
  Find_Ok           : Boolean;
  sSearchString     : KeyStr;
Begin
  frmPlayerFind.Hide;
  sSearchString := trim(MESearch.Text);
  Filno := Playern_no;
  If pos('name', LowerCase(Label1.Caption)) > 0 Then Begin
    { If searching by Player Name }
    If sSearchString <> '' Then Begin
      dbase.Filno := Playern_no;
      Find_Ok := Dbase.Find_Record(sSearchString, GetKey(Playern_no, 1),' Select Player to View ', 1);
      close;
    End;
  End
  Else Begin
    { If searching by Player Number }
    If sSearchString <> '' Then Begin
      dbase.Filno := Playern_no;
      Find_Ok := Dbase.Find_Record(sSearchString, GetKey(Playern_no, 2),' Select Player to View ', 1);
      close;
    End;
  End;
End;

Procedure TfrmPlayerFind.FormShow(Sender: TObject);
Begin
  MESearch.SetFocus;
End;

Procedure TfrmPlayerFind.MESearchKeyPress(Sender: TObject; Var Key: Char);
Begin
  If Key = #13 Then BitBtnQuit.SetFocus;
End;

End.

