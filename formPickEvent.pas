Unit formPickEvent;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB33, formPlayers, Dbase;

Type
  TfrmPickEvent = Class(TForm)
    ListBox1: TListBox;
    Label1: TLabel;
    Procedure ListBox1DblClick(Sender: TObject);
    Procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  End;

Var
  frmPickEvent: TfrmPickEvent;
  ksTRcNoKy, ksTTmpKy: KeyStr;
  ksERcNoKy, ksETmpKy: KeyStr;
  SaveRecn: LongInt;
  SavK2F: KeyStr;
  bEOK: Boolean;

Implementation

Uses Util2, formAttendance;

{$R *.dfm}

Procedure TfrmPickEvent.ListBox1DblClick(Sender: TObject);
Var
  sSanctionNo, sEventCode: String;
  bTrue: Boolean;
Begin
  frmAttendance.sPickEvent := ListBox1.Items.Strings[ListBox1.ItemIndex];
  // -----------------------------------------------------------------------
  sSanctionNo := Copy(ListBox1.Items.Strings[ListBox1.ItemIndex], 1, 10);
  sEventCode := Copy(ListBox1.Items.Strings[ListBox1.ItemIndex], 36, 4);
  // -----------------------------------------------------------------------
  If frmPlayers.sPlayMode = 'T' Then
    ksTRcNoKy := GetKey(Tourn_no, 1)
  Else
    ksTRcNoKy := GetKey(ClubSanc_no, 1);
  ksTTmpKy := ksTRcNoKy;
  // -----------------------------------------------------------------------
  If frmPlayers.sPlayMode = 'T' Then
    db33.SearchKey(IdxKey[TournEv_no, 1]^, Recno[TournEv_no], ksTTmpKy)
  Else
    db33.SearchKey(IdxKey[ClubGame_no, 1]^, Recno[ClubGame_no], ksTTmpKy);
  OK := OK And CompareKey(ksTTmpKy, ksTRcNoKy);
  While OK Do Begin
    If frmPlayers.sPlayMode = 'T' Then Begin
      { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
      dBase.Filno := TournEv_no;
      dBase.GetARec(TournEv_no);
    End
    Else Begin
      { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
      dBase.Filno := ClubGame_no;
      dBase.GetARec(ClubGame_no);
    End;
    { Must run Align_Rec to align children records for this record }
    dBase.Align_Rec(RecNo[Filno]);

    If frmPlayers.sPlayMode = 'T' Then
      bTrue := dbase.Tournev.EVENT_CODE = sEventCode
    Else
      bTrue := dbase.CLUBGAME.EVENT_NCODE = sEventCode;

    If bTrue Then Begin
      If frmPlayers.sPlayMode = 'T' Then
        ksERcNoKy := dbase.GetKey(TournEv_no, 3)
      Else
        ksERcNoKy := dbase.GetKey(ClubGame_no, 3);
      ksETmpKy := ksERcNoKy;
      { Search for records in the Attendance table that match the Player Number key of the current
        Player Number record }
      db33.SearchKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksETmpKy);
      bEOK := OK And CompareKey(ksETmpKy, ksERcNoKy);
      { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
      dBase.Filno := Attend_no;
      dBase.GetARec(Attend_no);
      { Must run Align_Rec to align children records for this record }
      dBase.Align_Rec(RecNo[Filno]);
      Break;
    End;
    // ============================================================================================
    If frmPlayers.sPlayMode = 'T' Then
      NextKey(IdxKey[TournEv_no, 1]^, Recno[TournEv_no], ksTTmpKy)
    Else
      NextKey(IdxKey[ClubGame_no, 1]^, Recno[ClubGame_no], ksTTmpKy);
    OK := OK And CompareKey(ksTTmpKy, ksTRcNoKy);
  End;
  Close;
End;

Procedure TfrmPickEvent.FormShow(Sender: TObject);
Begin
  {
    LINK FOR TOURNAMENT RECORDS IS:
    ---------------------------------------------------------
    Master = Tourn (LINK TO TOURNEV BY SANCTION_NO)
      Fields = Sanction_no
             Child  = TournEv (LINK TO ATTEND BY HASH)
               Fields = Event_Date, Event_Code, Event_Name
                      Child  = Attend
                        Fields = Session_No, MPS, MP_Color
    ---------------------------------------------------------
  }
  If frmPlayers.sPlayMode = 'T' Then Begin
    SaveRecn := RecNo[TournEv_no];
    SavK2F := GetKey(TournEv_no, 1);
  End
  Else Begin
    SaveRecn := RecNo[ClubGame_no];
    SavK2F := GetKey(ClubGame_no, 1);
  End;
  // -----------------------------------------------------------------------------------------
  ListBox1.Clear;
  // T O U R N A M E N T S   R E C O R D S
  If frmPlayers.sPlayMode = 'T' Then
    ksTRcNoKy := GetKey(Tourn_no, 1)
  Else
    ksTRcNoKy := GetKey(ClubSanc_no, 1);
  ksTTmpKy := ksTRcNoKy;
  { Search for records in the Attendance table that match the Player Number key of the current
    Player Number record }
  If frmPlayers.sPlayMode = 'T' Then
    db33.SearchKey(IdxKey[TournEv_no, 1]^, Recno[TournEv_no], ksTTmpKy)
  Else
    db33.SearchKey(IdxKey[ClubGame_no, 1]^, Recno[ClubGame_no], ksTTmpKy);
  OK := OK And CompareKey(ksTTmpKy, ksTRcNoKy);
  While OK Do Begin
    If frmPlayers.sPlayMode = 'T' Then Begin
      { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
      dBase.Filno := dbase.TournEv_no;
      dBase.GetARec(TournEv_no);
    End
    Else Begin
      { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
      dBase.Filno := dbase.ClubGame_no;
      dBase.GetARec(ClubGame_no);
    End;
    { Must run Align_Rec to align children records for this record }
    dBase.Align_Rec(RecNo[Filno]);
    // ============================================================================================
    // ATTENDANCE RECORD

    If frmPlayers.sPlayMode = 'T' Then
      ksERcNoKy := GetKey(TournEv_no, 1)
    Else
      ksTRcNoKy := GetKey(ClubGame_no, 1);
    ksETmpKy := ksERcNoKy;
    { Search for records in the Attendance table that match the Player Number key of the current
      Player Number record }
    db33.SearchKey(IdxKey[Attend_no, 1]^, Recno[Attend_no], ksETmpKy);
    bEOK := OK And CompareKey(ksETmpKy, ksERcNoKy);
    { Set the Dbase.pas variable Filno to Table Number we are working with, Filno is used Globally }
    dBase.Filno := dbase.Attend_no;
    dBase.GetARec(Attend_no);
    { Must run Align_Rec to align children records for this record }
    dBase.Align_Rec(RecNo[Filno]);
    // ============================================================================================
    If frmPlayers.sPlayMode = 'T' Then
      ListBox1.Items.Add(tourn.SANCTION_NO + '  ' + Slash(copy(Tournev.EVENT_DATE, 1, 8)) + '  ' +
        Attend.SESSION_NO + '  ' + Attend.MPS + '  ' + TournEv.EVENT_CODE + '  ' +
        TournEv.EVENT_NAME)
    Else
      ListBox1.Items.Add(CLUBGAME.SANCTION_NO + '  ' + Slash(copy(CLUBGAME.EVENT_DATE, 1, 8)) + '  '
        +
        Attend.SESSION_NO + '  ' + Attend.MPS + '  ' + CLUBGAME.EVENT_NCODE + '  ' +
        CLUBGAME.EVENT_NAME);
    // ============================================================================================
    If frmPlayers.sPlayMode = 'T' Then
      NextKey(IdxKey[TournEv_no, 1]^, Recno[TournEv_no], ksTTmpKy)
    Else
      NextKey(IdxKey[ClubGame_no, 1]^, Recno[ClubGame_no], ksTTmpKy);
    OK := OK And CompareKey(ksTTmpKy, ksTRcNoKy);
  End;
End;

End.

