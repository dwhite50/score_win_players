{$I debugdir.inc}
Unit Util3; {File MAINTENANCE}

Interface

Uses Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, db33, db_edit, tpstring, util1, util2, groups, history,
  csdef;

{$DEFINE mainfile}
{$I dbvers.dcl}
{$I officepa.dcl}

Type
  LBack = Record
    F: Byte;
    K: Byte;
  End;
Type
  TDelWarn = Record
    W: byte;
    M: String[50];
  End;

Var
  Escaped: boolean;
  ifunc: byte;
  len: byte;

Var
  MainWC,
    GroupWC: WindowCoordinates;

Const
  Edit_Add: boolean = True;
  Del_no_Ask: boolean = false;
  PrePend: PathStr = '';
  MaxFilno = 7;
  MaxKeyno = 3;
  DBNames: Array[1..MaxFilno, 0..MaxKeyno] Of String[12] =
  (
    ('PLAYERN.DAT', 'PLAYERN.K1', 'PLAYERN.K2', 'PLAYERN.K3'),
    ('PLAYERGR.DAT', 'PLAYERGR.K1', '', ''),
    ('CLUBDEF.DAT', 'CLUBDEF.K1', '', ''),
    ('CLUBGAME.DAT', 'CLUBGAME.K1', 'CLUBGAME.K2', 'CLUBGAME.K3'),
    ('ATTEND.DAT', 'ATTEND.K1', 'ATTEND.K2', 'ATTEND.K3'),
    ('PERCENT.DAT', 'PERCENT.K1', 'PERCENT.K2', 'PERCENT.K3'),
    ('CLUBSANC.DAT', 'CLUBSANC.K1', '', ''));
  MaxRec: Array[1..MaxFilno] Of LongInt =
  (
    2147483646,
    2147483646,
    2147483646,
    2147483646,
    2147483646,
    2147483646,
    2147483646);
  KeyLen: Array[1..MaxFilno, 1..MaxKeyno] Of Byte =
  (
    (24, 7, 3),
    (6, 0, 0),
    (6, 0, 0),
    (19, 5, 3),
    (7, 6, 7),
    (11, 16, 3),
    (9, 0, 0));
  KeyMode: Array[1..MaxFilno, 1..MaxKeyno] Of Integer =
  (
    (1, 1, 0),
    (0, 0, 0),
    (0, 0, 0),
    (0, 1, 0),
    (1, 1, 1),
    (1, 1, 1),
    (0, 0, 0));
  KeySeg1Len: Array[1..MaxFilno, 1..MaxKeyno] Of Byte =
  (
    (16, 7, 3),
    (3, 0, 0),
    (6, 0, 0),
    (6, 4, 3),
    (3, 3, 3),
    (3, 6, 3),
    (6, 0, 0));
  KeyUse: Array[1..MaxFilno, 1..MaxKeyno] Of Char =
  (
    ('E', 'E', 'I'),
    ('I', ' ', ' '),
    ('E', ' ', ' '),
    ('I', 'I', 'I'),
    ('I', 'I', 'E'),
    ('I', 'E', 'I'),
    ('I', ' ', ' '));
  KeyType: Array[1..MaxFilno, 1..MaxKeyno] Of Char =
  (
    ('C', 'C', 'C'),
    ('C', ' ', ' '),
    ('C', ' ', ' '),
    ('C', 'C', 'C'),
    ('C', 'C', 'C'),
    ('C', 'C', 'C'),
    ('C', ' ', ' '));
  KeyFld: Array[1..MaxFilno, 1..MaxKeyno] Of Byte =
  (
    (1, 3, 0),
    (27, 0, 0),
    (1, 0, 0),
    (0, 0, 0),
    (0, 3, 0),
    (2, 5, 0),
    (0, 0, 0));
  FilInUse: Array[1..MaxFilno] Of Boolean =
  (True, True, True, True, True, True, False);
  LinkType: Array[1..MaxFilno, 1..MaxFilno] Of Char =
  (
    (' ', 'U', ' ', ' ', 'D', 'D', ' '),
    ('U', ' ', ' ', ' ', ' ', ' ', ' '),
    (' ', ' ', ' ', 'U', ' ', ' ', 'U'),
    (' ', ' ', 'U', ' ', ' ', ' ', ' '),
    ('U', ' ', ' ', ' ', ' ', ' ', ' '),
    ('U', ' ', ' ', ' ', ' ', ' ', ' '),
    (' ', ' ', 'U', ' ', ' ', ' ', ' '));
  LinkBack: Array[1..MaxFilno] Of LBack =
  ((F: 0; K: 0), (F: 1; K: 3), (F: 0; K: 0), (F: 3; K: 1),
    (F: 1; K: 3), (F: 1; K: 3), (F: 3; K: 1));
  KeyNamTab: Array[1..MaxFilno, 1..MaxKeyno] Of String[20] =
  (
    (
    'LAST_NAME',
    'PLAYER_NO',
    'RECORD_NO'),
    (
    'RECORD_NO',
    '',
    ''),
    (
    'CLUB_NO',
    '',
    ''),
    (
    'CLUB_NO',
    'EVENT_NCODE',
    'HASH'),
    (
    'RECORD_NO',
    'HASH',
    'HASH'),
    (
    'RECORD_NO',
    'CLUB_NO',
    'HASH'),
    (
    'CLUB_NO',
    '',
    ''));
  List_Len = 2;
  DupErr = 'Data base may be damaged.  Reindex data base.';
  ShowOnePick: boolean = true;
  MasterFile: word = 0;
  MenuDriven: Boolean = False;

Type
  RecnoTyp = Array[1..MaxFilno] Of LongInt;

Var
  RecTab: Array[1..List_Len] Of
  Record
    Ref: LongInt;
    Dsp: String[80];
  End;
  DBSize: Array[1..MaxFilno] Of Integer;
  DBFileOpen: Array[1..MaxFilno] Of boolean;
  DBNeedToOpen: Array[1..MaxFilno] Of boolean;
  DBFReadOnly: Array[1..MaxFilno] Of boolean;
  _Trace: Integer;
  Link: (Up_N_Down, Down, No_Link);
  FOK,
    Scrn_Active: Boolean;
  Func, Ch: Char;
  RecAvail: Array[1..MaxFilno] Of Boolean;
  DatF: Array[1..MaxFilno] Of ^DataFile;
  IdxKey: Array[1..MaxFilno, 1..MaxKeyno] Of ^IndexFile;
  TKeyTab: Array[1..MaxKeyno] Of KeyStr;
  TempKey,
    SKey: KeyStr;
  SaveKey,
    Key: KeyStr;
  KeyNum,
    Filno, Scrno: Integer;
  Recno: RecnoTyp;
  DataFile_Path: PathStr;
  PickIndex: Array[1..MaxFilno] Of byte;
  PickTable: Array[1..MaxKeyNo] Of word;
  SaveLnk: Array[1..MaxFilno] Of KeyStr;

Type
  TCustomEdit = Function(Const X, Y, Fld: Integer; Var Field: OpenString;
    Const Typ: Char; Const Pic: _PicStr; Const DoEdit: boolean): boolean;
  TPostDisplayRec = Procedure(Const Scrno, Filno: Integer);
  TFrameAdjust = Procedure(Const Scrno, Filno: Integer);
  TCheckOpen = Procedure(Const Fno, Kno: byte; Var KeyLen: byte);
  TCheckPutRecord = Procedure(Const Fno: byte);
  TCheckRecord = Procedure(Const Fno: byte);
  TPreDisplayRec = Procedure(Const Sno, Fno: Integer);
  TGetPrepend = Function(Const Fno: byte): PathStr;
  TPostEditProc = Procedure(Const Fno: byte);

Type
  _Str70 = String[70];

Const
  CustomEdit: TCustomEdit = Nil;
  PostDisplayRec: TPostDisplayRec = Nil;
  FrameAdjust: TFrameAdjust = Nil;
  CheckOpen: TCheckOpen = Nil;
  CheckPutRecord: TCheckPutRecord = Nil;
  CheckRecord: TCheckRecord = Nil;
  PreDisplayRec: TPreDisplayRec = Nil;
  PostEditProc: TPostEditProc = Nil;

Var last_pick_item: Word;
  pick_fno, pick_kno: Integer;
  pick_kstr: KEYSTR;
  maxl: byte;


  {$I PLAYERN.REC}
  {$I PLAYERGR.REC}
  {$I CLUBDEF.REC}
  {$I CLUBGAME.REC}
  {$I ATTEND.REC}
  {$I PERCENT.REC}
  {$I CLUBSANC.REC}

Function GetLink(Const Nummer: Integer): KeyStr;
Procedure TraceBack(Const Fno: Integer);
Function Status_OK(Const Fno: Integer): Boolean;
Procedure SkipNext(Const Fno: Integer);
Procedure SkipPrev(Const Fno: Integer);
Procedure GetARec(Const Fno: Integer);
Procedure PutARec(Const Fno: Integer);
Procedure Clear_Rec(Const Fno: Integer);
Function GetKeyFld(Const Fno, Kno: Integer): KeyStr;
Procedure Display_Frame;
Procedure Display_Rec;
Function List_N_Choose(Const Fno, Kno: Integer; Const Kstr: KeyStr;
  Const DefaultPick: KeyStr; Const Title: String;
  Const Location: byte): LongInt;
Function List(Const Fno, Kno: Integer; Var KS: KeyStr): KeyStr;
Function GetKey(Const Fno, Kno: Integer): KeyStr;
Procedure FindMulti(Const Fno: Integer; Var Recn: LongInt; Var Key2Find: String);
Procedure FixKey(Const Fno, Kno: Integer; Var KEYVAR: KeyStr);
Function Add_Record: boolean;
Procedure CloseFiles;
Procedure Edit_Rec(Const Fld: Integer);
Procedure Delete_Record;
Procedure Next_Record;
Procedure Prev_Record;
Procedure Last_Record;
Function Find_Record(Const KeyToFind, DefaultPick: KeyStr; Const Title: String;
  Const Location: byte; Const Display: boolean): boolean;
Procedure Top_Record;
Procedure Align_Rec(OldR: LongInt);
Procedure Switch_File(Const Mode: Char);

Procedure EditMenu;
Procedure Stat_Line;
Procedure ErrMsge(Const Msge: String; Const tofile: boolean);
Function Found(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
Function OpenDBFile(Const Fno: Integer): boolean;
Procedure OpenFiles;
Function Edit_Record: boolean;
Procedure Initialize;
Function Valid_Path: Boolean;
Function DuplicateKey(Const Fno, Kno: integer): boolean;
Function Custom_Key(Const Scr, Fno: Integer; Var Fld: Integer; Var Key: Char): Boolean;

Procedure UPDATE_Frm;
Procedure UPDATE_Clr(Fno: Integer);
Procedure UPDATE_Dsp(Fno: Integer);
Procedure UPDATE_Edt(Fno, Fld: Integer);
Function UPrePend(Const Fno: byte): PathStr;

Const
  GetPrepend: TGetPrePend = UPrepend;

Implementation

Uses version,
  I_UPDATE;

Function UPrePend(Const Fno: byte): PathStr;
Begin
  UPrepend := Prepend;
End;

Function CheckCopy(Const DefValue, OldValue: String): _Str70;
Begin
  If Func = 'C' Then Checkcopy := OldValue
  Else CheckCopy := Pad(DefValue, Length(OldValue));
End;

Procedure Edt_Fld(Const X, Y: Integer; Var Field: OpenString; Const Typ: Char;
  Const Pic: _PicStr; Const Attr: Byte);
Var
  Fld: Integer;
Begin
  If Assigned(CustomEdit) Then Begin
    Fld := FldNum;
    If CustomEdit(X, Y, Fld, Field, Typ, Pic, true) Then Begin
      db_edit.Edt_Fld(X, Y, Field, Typ, Pic, 0);
      HistName := '';
      HistType := 0;
      CustomEdit(X, Y, Fld, Field, Typ, Pic, false);
    End
    Else If DBEXcode <> Qit_key Then Begin
      DBexCode := Cr_Key;
      If _Upward Then Begin
        If FldNum > 1 Then Dec(FldNum)
        Else DBExCode := Xept_Key;
      End
      Else Inc(FldNum);
    End;
  End
  Else db_edit.Edt_Fld(X, Y, Field, Typ, Pic, 0);
End;

Function DuplicateKey(Const Fno, Kno: integer): boolean;
Var
  tempkey: keystr;
  trecno: longint;
Begin
  tempkey := GetKey(Fno, Kno);
  findkey(IdxKey[Fno, Kno]^, trecno, tempkey);
  If ok And ((trecno <> recno[Fno]) Or ((func = 'A') Or (func = 'C'))) Then
    DuplicateKey := true
  Else DuplicateKey := false;
  ok := true;
End;

Function sendidxname(Const idx: word): String; far;
Begin
  sendidxname := KEYNAMTAB[FILNO, PickTable[idx]];
End;

Function SendFileName(Const Item: Word): String; far;
{-pass each file name to the pick unit}
Var Y: integer;
Begin
  y := 1;
  ok := true;
  If Item > last_pick_item Then Repeat
      Inc(last_pick_item);
      NEXTKEY(IdxKey[pick_fno, pick_kno]^, Recno[pick_fno], pick_kstr);
      OK := OK And CompareKey(SaveKey, pick_kstr);
    Until (last_pick_item = Item) Or Not OK
  Else If Item < last_pick_item Then Repeat
      dec(last_pick_item);
      PrevKey(IdxKey[pick_fno, pick_kno]^, Recno[pick_fno], pick_kstr);
      OK := OK And CompareKey(SaveKey, pick_kstr);
    Until (last_pick_item = Item) Or Not OK;
  If ok Then Begin
    GetARec(Pick_FNO);
    RecTab[1].REF := RECNO[Pick_FNO];
    Case Pick_FNO Of
      1: RecTab[Y].Dsp := FindFormat(1, 2);
      3: RecTab[Y].Dsp := FindFormat(3, 2);
      4: RecTab[Y].Dsp := FindFormat(4, 2);
      5: RecTab[Y].Dsp := FindFormat(5, 2);
      6: RecTab[Y].Dsp := FindFormat(6, 2);
      7: RecTab[Y].Dsp := FindFormat(7, 2);
    Else If MAXKEYNO > 1 Then If Pick_KNO > 1 Then
        RecTab[1].DSP := GETKEYFLD(Pick_FNO, Pick_KNO) + ' ' + GETKEYFLD(Pick_FNO, 1)
      Else RecTab[1].DSP := GETKEYFLD(Pick_FNO, 1) + ' ' + GETKEYFLD(Pick_FNO, 2)
    Else RecTab[1].DSP := GETKEYFLD(Pick_FNO, 1);
    End;
  End
  Else Begin
    RecTab[1].Dsp := 'bad';
    rectab[1].ref := -1;
  End;
  SendFileName := RecTab[1].Dsp;
End;

Procedure ScrDsp(Const PR: String; Const M: byte; Const X, Y: Byte);
Begin
  If Length(PR) < 1 Then Exit;
  If (M = 240) And (X = 1) And (Y = 1) Then ErrBox(pr, BC, 0)
  Else db_edit.ScrDsp(PR, M, X, Y);
End;

{$I UPDATE.INC}

Function OpenDBFile(Const Fno: Integer): boolean;
Var
  FOK: boolean;
  J,
    FO_I, FO_J: Integer;
  OldFileMode: byte;
  P: Pchar;
  DBDrive: char;
  DBFileLoc: PathStr;
  dummy: byte;
Begin
  If DBFileOpen[Fno] Then Begin
    OpenDBFile := true;
    exit;
  End;
  DBFileLoc := GetPrepend(Fno);
  If (Length(DBFileLoc) < 2) Or (DBFileLoc[2] <> ':') Then Begin
    GetDir(0, DBFileLoc);
    DBDrive := DBFileLoc[1];
    DBFileLoc := GetPrepend(Fno);
  End
  Else DBDrive := DBFileLoc[1];
  FOK := true;
  FO_I := 0;
  New(DatF[Fno]);
  dummy := 0;
  If Assigned(CheckOpen) Then CheckOpen(Fno, 0, dummy);
  OldFileMode := FileMode;
  If {$IFDEF office}
  CompareKey(Upper(DBFileLoc), OfficePath[1]) Or
    CompareKey(Upper(DBFileLoc), OfficePath[2]) Or
    CompareKey(Upper(DBFileLoc), Upper(OfficeTournPath)) Or
    {$ENDIF}
  (GetDriveType(DBDrive) = dtCDROM)
    Or ((ACBLVector = 0) And (ParamCount < 1)) Then FileMode := 0;
  OpenFile(DatF[Fno]^, DBFileLoc + DBNames[Fno, 0], DBSize[Fno]);
  FOK := FOK And OK;
  If FOK Then Begin
    P := StrPos(FileRec(DatF[Fno]^.F).Name, 'PLAYERN.');
    If Not Assigned(P) Then With DatF[Fno]^ Do If Int3 > GetVers Then Begin
          RingClear(false);
          send_to_error_file('ACBLscore Version ' + Real2Str(Int3 / 100, 6, 2)
            + ' required for Data Base.');
          MsgBox('This data base requires ACBLscore Version '
            + Real2Str(Int3 / 100, 6, 2) + ' or newer.'#13
            + 'Install ACBLscore version ' + Real2Str(Int3 / 100, 6, 2)
            + ' or newer to access this data base.', intont, true, MC,
            CFG.WarnColor, 0);
          Halt;
        End;
    FO_I := Fno;
    FO_J := 0;
    For J := 1 To MaxKeyno Do If FOK And (KeyLen[Fno, J] > 0) Then Begin
        New(IdxKey[Fno, J]);
        If Assigned(CheckOpen) Then CheckOpen(Fno, J, KeyLen[Fno, J]);
        OpenIndex(IdxKey[Fno, J]^, GetPrepend(Fno) + DBNames[Fno, J], KeyLen[Fno, J], KeyMode[Fno,
          J]);
        FOK := FOK And OK;
        If FOK Then FO_J := J;
      End;
  End
  Else If FilInUse[Fno] And (FileMode > 0) Then Begin
    MakeFile(DatF[Fno]^, GetPrepend(Fno) + DBNames[Fno, 0], DBSize[Fno]);
    FOK := OK;
    If FOK Then Begin
      FlushFile(DatF[Fno]^);
      FO_I := Fno;
      FO_J := 0;
      For J := 1 To MaxKeyno Do If FOK And (KeyLen[Fno, J] > 0) Then Begin
          New(IdxKey[Fno, J]);
          MakeIndex(IdxKey[Fno, J]^, GetPrepend(Fno) + DBNames[Fno, J], KeyLen[Fno, J],
            KeyMode[Fno, J]);
          FOK := FOK And OK;
          If FOK Then Begin
            FlushIndex(IdxKey[Fno, J]^);
            FO_J := J;
          End;
        End
    End;
  End;
  If FileMode = 0 Then DBFReadOnly[Fno] := FOK;
  FileMode := OldFileMode;
  DBFileOpen[Fno] := FOK;
  OpenDBFile := FOK;
End;

Procedure OpenFiles;
Var
  j,
    I: integer;
  HandlesLeft: byte;
  HandlesNeeded: byte;
Begin
  HandlesNeeded := 2;
  For i := 1 To MaxFilno Do For j := 0 To MaxKeyno Do
      If Length(DBNames[i, j]) > 0 Then Inc(HandlesNeeded);
  HandlesLeft := FileHandlesLeft;
  If HandlesNeeded > HandlesLeft Then Begin
    RingClear(true);
    If CFG.Win32Ver > 4 Then MsgBox(
        'All the necessary files could not be opened for the data base.'#13 +
        'Increase the FILES= statement in \WINDOWS\SYSTEM32\CONFIG.NT by '
        + Long2Str(HandlesNeeded - HandlesLeft), intont, true, MC, CFG.WarnColor, 170)
    Else MsgBox(
        'All the necessary files could not be opened for the data base.'#13
        + 'Restart your computer by pressing Ctrl-Alt-Del.  If this problem'#13 +
        'persists, increase the FILES= statement in CONFIG.SYS by '
        + Long2Str(HandlesNeeded - HandlesLeft), intont, true, MC, CFG.WarnColor, 170);
    send_to_error_file('Increase CONFIG.SYS FILES= by '
      + Long2Str(HandlesNeeded - HandlesLeft));
    EraseBoxWindow;
    TextAttr := 7;
    ClrScr;
    Halt;
  End;
  DBSize[1] := PLAYERN_Size;
  DBSize[2] := PLAYERGR_Size;
  DBSize[3] := CLUBDEF_Size;
  DBSize[4] := CLUBGAME_Size;
  DBSize[5] := ATTEND_Size;
  DBSize[6] := PERCENT_Size;
  DBSize[7] := CLUBSANC_Size;
  If length(Prepend) < 2 Then Begin
    Prepend := Strip(DataFile_Path);
    If Length(PrePend) > 0 Then
      If Prepend[Length(Prepend)] <> '\' Then
        PrePend := PrePend + '\';
  End;
  If CheckDrive(Escaped, GetPrePend(1), false) <> 0 Then Halt;
  FOK := True;
  For I := 1 To MaxFilno Do If FOK And DBNeedtoOpen[I] Then
      FOK := OpenDBFile(I);
  If Not FOK Then For I := 1 To MaxFilno Do If DBFileOpen[I] Then Begin
        CloseFile(DatF[I]^);
        Dispose(DatF[I]);
        For J := 1 To MaxKeyno Do If (KeyLen[I, J] > 0) Then Begin
            CloseIndex(IdxKey[I, J]^);
            Dispose(IdxKey[I, J]);
          End;
      End;
End;


Function Edit_Record: boolean;
Var
  TRecno: LongInt;
  RKeyno: Integer;
  SavKey: Array[1..MaxKeyno] Of KeyStr;
  Savno: RecnoTyp;
  Fno, TF,
    Keyno: Integer;

  Procedure PutLink(Const Nummer: Integer);
  Var
    TKey: KeyStr;
    TRecno: LongInt;
    TF: Integer;
  Begin
    If (SaveLnk[Nummer] <> '') And FilInUse[Nummer] Then Begin
      Repeat
        Key := SaveLnk[Nummer];
        TKey := Key;
        SearchKey(IdxKey[Nummer, 1]^, Recno[Nummer], Key);
        OK := OK And CompareKey(TKey, Key);
        If OK Then Begin
          TRecno := Recno[Nummer];
          Link := No_Link;
          GetARec(Nummer);
          Link := Up_N_Down;
          Case Nummer Of
            2: PLAYERGR.RECORD_NO := PLAYERN.RECORD_NO;
            5: ATTEND.RECORD_NO := PLAYERN.RECORD_NO;
            6: PERCENT.RECORD_NO := PLAYERN.RECORD_NO;
            4: CLUBGAME.CLUB_NO := CLUBDEF.CLUB_NO;
          End; { case }
          PutARec(Nummer);
          TKey := GetKey(Nummer, 1);
          DeleteKey(IdxKey[Nummer, 1]^, TRecno, Key);
          AddKey(IdxKey[Nummer, 1]^, Recno[Nummer], TKey);
        End;
      Until Not OK;
      OK := True;
      {for TF := Nummer + 1 to MaxFilno do
        if (SaveLnk[TF] <> GetLink(TF)) then PutLink(TF);}
    End;
  End;

Begin
  If DBFReadOnly[Filno] Then exit;
  Edit_Record := false;
  If Status_OK(Filno) Then Begin
    GetARec(Filno);
    Display_Rec;
    Clr_Line(1);
    EditMenu;
    For Keyno := 1 To MaxKeyno Do
      SavKey[Keyno] := GetKey(Filno, Keyno);
    Savno := Recno;
    For Keyno := 1 To MaxFilno Do
      SaveLnk[Keyno] := GetLink(Keyno);
    Edit_Rec(0);
    If DBEXcode <> Qit_Key Then Begin
      If Scrn_Active Then PleaseWait;
      Keyno := 1;
      While (Keyno <= MaxKeyno) And (DBEXcode <> Qit_Key) Do Begin
        Repeat
          TKeyTab[Keyno] := GetKey(Filno, Keyno);
          If TKeyTab[Keyno] = SavKey[Keyno] Then
            OK := True
          Else Begin
            AddKey(IdxKey[Filno, Keyno]^, Recno[Filno], TKeyTab[Keyno]);
            If Not OK Then Begin
              If Scrn_Active Then EraseBoxWindow;
              ErrMsge('This field already exists in another record.', false);
              {ErrMsge(DupErr,true);}
              Edit_Rec(KeyFld[Filno, Keyno]);
              {DBEXcode := Qit_Key;}
              If Scrn_Active Then PleaseWait;
              OK := False;
            End;
          End;
        Until OK Or (DBEXcode = Qit_Key);
        If DBEXcode = Qit_Key Then Begin
          For RKeyno := 1 To Keyno - 1 Do Begin
            TRecno := Recno[Filno];
            If TKeyTab[Keyno] <> SavKey[Keyno] Then Begin
              DeleteKey(IdxKey[Filno, RKeyno]^, TRecno, TKeyTab[RKeyno]);
            End;
          End;
        End
        Else
          Inc(Keyno);
      End;
      If DBEXcode <> Qit_Key Then Begin
        For Keyno := 1 To MaxKeyno Do
          If TKeyTab[Keyno] <> SavKey[Keyno] Then Begin
            DeleteKey(IdxKey[Filno, Keyno]^, Recno[Filno], SavKey[Keyno]);
            Recno[Filno] := Savno[Filno]
          End;
        If Assigned(PostEditProc) Then PostEditProc(Filno);
        PutARec(Filno);
        For Fno := 1 To MaxFilno Do
          If (SaveLnk[Fno] <> GetLink(Fno)) And (LinkType[Fno, Filno] = 'U') Then
            PutLink(Fno);
        Edit_Record := true;
      End;
      If Scrn_Active Then EraseBoxWindow;
    End;
    GetARec(Filno); (* OK SHOULD BE True BEFORE GetARec IS CALLED *)
    setabn(false);
    Display_Rec;
  End;
End;

Procedure Initialize;
Var
  Serial: word;
  Ser: word;
Begin
  ClrScr;
  KeyNum := 1;
  Scrno := 1;
  _Trace := 0;
  Link := Up_N_Down;
  Flush_DF := True;
  Flush_IF := True;
  Scrn_Active := False;
  If MasterFile > 0 Then Begin
    Serial := DBGetFlag(DatF[MasterFile]^, 1);
    If Serial < 1 Then Begin
      Serial := Random($FFFE) + 1;
      DBPutFlag(DatF[MasterFile]^, Serial, 1);
      FlushFile(DatF[MasterFile]^);
    End;
  End;
  For Filno := 1 To MaxFilno Do If DBFileOpen[Filno] Then Begin
      PickIndex[Filno] := 1;
      Recno[Filno] := 0;
      If MasterFile > 0 Then Begin
        Ser := DBGetFlag(DatF[Filno]^, 1);
        If Ser < 1 Then Begin
          DBPutFlag(DatF[Filno]^, Serial, 1);
          FlushFile(DatF[Filno]^);
        End
        Else If Ser <> Serial Then Begin
          RingClear(true);
          MsgBox('Files ' + DBNames[Filno, 0] + ' and ' + DBNames[MasterFile, 0]
            + ' do not match.'#13
            + 'Select Data Base Maintenance Option 7 (Reinitialize data base),'#13 +
            'then Option ' + Long2Str(MaintOpt[Filno]) + ' to repair this problem.',
            intont, true, MC, CFG.WarnColor, 0);
          MsgBox('Closing Files - Please Wait', defattrt, false, MC, CFG.InformColor, 0);
          CloseFiles;
          EraseBoxWindow;
          TextAttr := 7;
          ClrScr;
          Halt;
        End;
      End;
      Clear_Rec(Filno);
      RecAvail[Filno] := (UsedRecs(DatF[Filno]^) > 0);
    End;
  Filno := 1;
End;

Function Valid_Path: Boolean;
Begin
  If ParamCount >= 1 Then Begin
    DataFile_Path := Strip(ParamStr(1));
    Parse_New_Path(DataFile_Path);
    Valid_Path := Path_OK(DataFile_Path);
  End
  Else Begin
    DataFile_Path := Get_DOS_Path;
    Valid_Path := True;
  End;
End;

Procedure Switch_File(Const Mode: Char);
Var
  Incr, Count: Integer;
Begin
  Scrn_Active := True;
  If Mode = '+' Then
    Incr := 1
  Else
    If Mode = '-' Then
      Incr := -1
    Else
      Incr := 0;
  Count := 0;
  Repeat
    Filno := Filno + Incr;
    Inc(Count);
    If Filno < 1 Then Filno := 7
    Else If (Filno > 7) Or (Count > 16) Then Filno := 1
  Until FilInUse[Filno] Or (Count > 16);
  Case Filno Of
    1: If Scrno <> 1 Then Begin
        Scrno := 1;
        Display_Frame
      End;
    2: If Scrno <> 1 Then Begin
        Scrno := 1;
        Display_Frame
      End;
    3: If Scrno <> 4 Then Begin
        Scrno := 4;
        Display_Frame
      End;
    4: If Scrno <> 4 Then Begin
        Scrno := 4;
        Display_Frame
      End;
    5: If Scrno <> 2 Then Begin
        Scrno := 2;
        Display_Frame
      End;
    6: If Scrno <> 3 Then Begin
        Scrno := 3;
        Display_Frame
      End;
  End;
  If Not Status_OK(Filno) Then Top_Record;
  Display_Rec
End;

Procedure TraceBack(Const Fno: Integer);
Var
  TK, SK: KeyStr;
  F, K: Integer;
Begin
  F := LinkBack[Fno].F;
  K := LinkBack[Fno].K;
  _Trace := F;
  If _Trace > 0 Then Begin
    If RecAvail[Fno] Then Begin
      TK := GetKey(Fno, 1); SK := TK;
      SearchKey(IdxKey[F, K]^, Recno[F], TK);
      OK := OK And CompareKey(SK, TK);
      If OK Then Begin
        GetARec(F);
        OK := Status_OK(Fno);
        If OK And (K > 1) Then Begin
          TK := GetKey(F, 1);
          FindMulti(F, Recno[F], TK);
        End;
      End;
    End
    Else OK := True;
    If Not (OK) Then Begin
      ErrMsge('Record trace back has failed. Data base may be damaged',
        true);
      Filno := 1;
      Switch_File(' ');
      Top_Record;
    End
    Else If (LinkBack[F].F > 0) Then TraceBack(F);
  End;
  _Trace := 0;
  OK := Status_OK(Fno);
End;

Procedure Align_Rec(OldR: LongInt);
Var
  TmpK: KeyStr;
Begin
  OK := True;
  _Trace := Filno;
  Recno[Filno] := OldR;
  {  Display_Frame;}
  Scrn_Active := False;
  If (OldR > 0) Then GetARec(Filno) Else Recavail[Filno] := False;
  If RecAvail[Filno] Then Begin
    TmpK := GetKey(Filno, 1);
    FindMulti(Filno, OldR, TmpK);
    If Not OK Then Begin
      ErrMsge('Index File mismatch.  Data base may be damaged.', true);
      Top_Record;
    End;
  End
  Else
    Top_Record;
  If Filno <> 1 Then
    TraceBack(Filno);
  _Trace := 0;
  Scrn_Active := True;
  Display_Rec;
End;

Procedure Top_Record;
Begin
  If LinkBack[Filno].F = 0 Then Begin
    ClearKey(IdxKey[Filno, 1]^);
    NextKey(IdxKey[Filno, 1]^, Recno[Filno], Key)
  End
  Else Begin
    TempKey := GetLink(Filno);
    SKey := TempKey;
    SearchKey(IdxKey[Filno, 1]^, Recno[Filno], TempKey);
    OK := OK And CompareKey(SKey, TempKey);
  End;
  If OK Then
    GetARec(Filno)
  Else Begin
    Clear_Rec(Filno);
    RecAvail[Filno] := False;
  End;
  Display_Rec;
End;

Procedure Next_Record;
Var
  SavR: LongInt;
Begin
  If Status_OK(Filno) Then Begin
    SavR := Recno[Filno];
    SkipNext(Filno);
    If Not OK Then Top_Record
    Else Begin
      GetARec(Filno);
      If Not RecAvail[Filno] Then
        Align_Rec(SavR)
      Else
        Display_Rec;
    End;
  End;
End;

Procedure Prev_Record;
Var
  SavR: LongInt;
Begin
  If Status_OK(Filno) Then Begin
    SavR := Recno[Filno];
    SkipPrev(Filno);
    If Not OK Then Top_Record
    Else Begin
      GetARec(Filno);
      If Not RecAvail[Filno] Then
        Align_Rec(SavR)
      Else
        Display_Rec;
    End;
  End;
End;

Procedure Last_Record;
Var
  SavR: LongInt;
Begin
  If Status_OK(Filno) Then Begin
    SavR := Recno[Filno];
    If LinkBack[Filno].F = 0 Then Begin
      ClearKey(IdxKey[Filno, 1]^);
      PrevKey(IdxKey[Filno, 1]^, Recno[Filno], Key)
    End
    Else Begin
      TempKey := GetLink(Filno);
      SKey := TempKey;
      SearchKey(IdxKey[Filno, 1]^, Recno[Filno], TempKey);
      OK := OK And CompareKey(SKey, TempKey);
      If OK Then Begin
        While OK Do Begin
          NextKey(IdxKey[Filno, 1]^, Recno[Filno], TempKey);
          OK := OK And CompareKey(SKey, TempKey);
        End;
        PrevKey(IdxKey[Filno, 1]^, Recno[Filno], TempKey);
      End;
    End;
    If Not OK Then Top_Record
    Else Begin
      GetARec(Filno);
      If Not RecAvail[Filno] Then
        Align_Rec(SavR)
      Else
        Display_Rec;
    End;
  End;
End;

Function Find_Record(Const KeyToFind, DefaultPick: KeyStr; Const Title: String;
  Const Location: byte; Const Display: boolean): boolean;
Label
  Find_Exit;
Var
  Temp_Pic: String[MaxKeyLen];
  TRecno,
    OldNum: LongInt;
  NumKey: Real;
  loop, ans: word;
  KeyPicks: word;
  semipos: byte;
  TempAltKey: String[20];

  Procedure Display_The_Find;
  Begin
    link := up_n_down;
    If TRecno <> 0 Then
      Align_Rec(TRecno)
    Else
      Align_Rec(OldNum);
  End;

Begin
  Find_Record := false;
  If (UsedRecs(DatF[Filno]^) > 0) {and (FilInUse[Filno])} Then Begin
    If RecAvail[Filno] Then OldNum := Recno[Filno] Else OldNum := 0;
    loop := 1;
    KeyPicks := 0;
    While loop <= MaxKeyNo Do Begin
      If Length(keynamtab[filno, loop]) > 0 Then Begin
        Inc(KeyPicks);
        PickTable[KeyPicks] := loop;
      End;
      Inc(Loop);
    End;
    If KeyPicks < 1 Then Goto Find_Exit;
    If PickIndex[Filno] > KeyPicks Then PickIndex[Filno] := 1;
    {Clr_Line(25);}
    Key := KeyToFind;
    Escaped := false;
    Ans := PickIndex[Filno];
    KeyNum := PickTable[ans];
    If Length(Key) < 1 Then Repeat
        ifunc := 0;
        If KeyPicks = 1 Then
          StrBox(Escaped, ifunc, 'Enter ' + KeyNamTab[Filno, KeyNum]
            + ', or ESC to Abort', Key, KeySeg1Len[Filno, KeyNum], len, 1, 0, DHelpTopic,
            BC, CFG.InformColor)
        Else Begin
          If AltKey[Filno, KeyNum] < 1 Then TempAltKey := 'a different key'
          Else TempAltKey := KeyNamTab[Filno, AltKey[Filno, KeyNum]];
          StrBoxField := 1;
          StrBox(Escaped, ifunc, 'Enter ' + KeyNamTab[Filno, KeyNum]
            + ', or ESC to abort'#13'Press F2 to search by ' + TempAltKey + '.',
            Key, KeySeg1Len[Filno, KeyNum], len, 21, 0, DHelpTopic, BC, CFG.InformColor);
        End;
        If ifunc = 2 Then Begin
          If KeyPicks = 2 Then ans := 3 - ans
          Else Begin
            If PickWindow(sendidxname, KeyPicks, 30, 19, 52, 25, true, CFG.Pickcolor,
              ' Select SEARCH KEY ', ans) Then ;
            Case pickcmdnum Of
              pksexit: Escaped := true;
            End;
          End;
          KeyNum := PickTable[ans];
          Key := '';
        End
        Else If (KeyNamTab[Filno, KeyNum] = 'ACBL number') And (len = 7)
          And Not player_check(Key, pnPoundACBL) Then Begin
          ErrMsge('Invalid Player Number: ' + Key, false);
          ifunc := 1;
        End;
        If Not Escaped Then PickIndex[Filno] := ans;
        semipos := Pos(';', Key);
        If semipos = 0 Then Key := Trim(Key)
        Else Key := Pad(Copy(Key, 1, semipos - 1), KeySeg1Len[Filno, KeyNum])
          + Copy(Key, semipos + 1, 20);
      Until Escaped Or ((ifunc = 0) And (len > 0));
    If Escaped Then DBexCode := Qit_Key;
    If DBEXCode = Qit_Key Then Goto Find_Exit;
    If (Length(Key) > 0) And Not Empty(Key) Then Begin
      If KeyType[Filno, KeyNum] In _NUMFLD Then Begin
        NumKey := Valu(Key);
        TempKey := GetKeyFld(Filno, KeyNum);
        If Pos('.', TempKey) = 0 Then Str(NumKey: Length(TempKey): 0, Key)
        Else Str(NumKey: Length(TempKey): Length(TempKey) - Pos('.', TempKey), Key);
      End
      Else FixKey(Filno, KeyNum, Key);
    End;
    If Empty(Key) Then Key := '';
    SaveKey := Key;
    {Clr_Line(1);}
    If Length(Key) = 0 Then Begin
      ClearKey(IdxKey[Filno, KeyNum]^);
      NextKey(IdxKey[Filno, KeyNum]^, Recno[Filno], Key);
      Key := SaveKey;
      TRecno := List_N_Choose(Filno, KeyNum, Key, DefaultPick, Title, Location);
      Find_Record := TRecno <> 0;
      If Display Then Display_The_Find;
    End
    Else Begin
      SearchKey(IdxKey[Filno, KeyNum]^, Recno[Filno], Key);
      OK := OK And CompareKey(SaveKey, Key);
      If Not OK Then Begin
        If Display Then Align_Rec(OldNum);
        ErrMsge(SaveKey + ' could not be found.', false);
      End
      Else Begin
        NextKey(IdxKey[Filno, KeyNum]^, Recno[Filno], Key);
        If ShowOnePick Or (OK And CompareKey(SaveKey, Key)) Then Begin
          Key := SaveKey;
          TRecno := List_N_Choose(Filno, KeyNum, Key, DefaultPick, Title, Location);
          Find_Record := TRecno <> 0;
          If Display Then Display_The_Find;
        End
        Else Begin
          PrevKey(IdxKey[Filno, KeyNum]^, Recno[Filno], Key);
          If Display Then Align_Rec(Recno[Filno]);
          Find_Record := true;
        End
      End
    End
  End;
  Find_Exit:
  {Stat_Line;}
End;

Procedure Delete_Record;
Var
  TRecno,
    NextRec: LongInt;
  NxtKey: KeyStr;
  TOK: Boolean;
  TF: Integer;

  Procedure FlushAFile(Const Fno: Integer);
  Var
    i, Keyno: Integer;
  Begin
    For i := 1 To MaxFilno Do
      If (LinkType[i, Fno] = 'U') And (LinkBack[i].F = Fno) And FilInUse[i] Then
        FlushAFile(i);
    FlushFile(DatF[Fno]^);
    For Keyno := 1 To MaxKeyno Do
      If (KeyLen[Fno, Keyno] > 0) Then FlushIndex(IdxKey[Fno, Keyno]^);
  End;

  Procedure DelARec(Const Fno: Integer);
  Var
    TKey,
      TKey2: KeyStr;
    SavOK: Boolean;
    i, Keyno: Integer;
    DelRecno: LongInt;
  Begin
    Link := No_Link;
    GetARec(Fno);
    Link := Up_N_Down;
    For i := 1 To MaxFilno Do Begin
      If (LinkType[i, Fno] = 'U') And (LinkBack[i].F = Fno) Then Begin
        If FilInUse[i] Then Begin
          TKey := GetLink(i);
          TKey2 := TKey;
          Repeat
            SearchKey(IdxKey[i, 1]^, Recno[i], TKey2);
            SavOK := OK And CompareKey(TKey, TKey2);
            If SavOK Then DelARec(i);
          Until Not SavOK;
        End;
      End;
    End;
    DelRecno := Recno[Fno];
    DeleteRec(DatF[Fno]^, DelRecno);
    For Keyno := 1 To MaxKeyno Do Begin
      If (KeyLen[Fno, Keyno] > 0) Then Begin
        DelRecno := Recno[Fno];
        TKey := GetKey(Fno, Keyno);
        DeleteKey(IdxKey[Fno, Keyno]^, DelRecno, TKey);
      End;
    End;
  End;

Var
  i: Integer;
  TKey,
    TKey2: KeyStr;
  Delok: boolean;
Begin
  If DBFReadOnly[Filno] Then exit;
  If Status_OK(Filno) Then Begin
    If Not Edit_Add Then Begin
      DelARec(Filno);
      Exit;
    End;
    GetARec(Filno);
    Display_Rec;
    If Del_no_ask Or YesNoBox(Escaped, ifunc, false,
      'Confirm deletion of this ' + DelMsg[Filno, 1] + #13 + DelMsg[Filno, 2],
      DHelpTopic, BC, CFG.InformColor) Then Begin
      TRecno := Recno[Filno];
      NextKey(IdxKey[Filno, 1]^, Recno[Filno], Key);
      If OK Then Begin
        NxtKey := Key;
        NextRec := Recno[Filno]
      End;
      TOK := OK;
      PrevKey(IdxKey[Filno, 1]^, Recno[Filno], Key);
      If Recno[Filno] <> TRecno Then Begin
        ErrMsge('Bad Index File, Cannot Delete Record. Data base is damaged',
          true);
        GetARec(Filno);
        Display_Rec
      End
      Else Begin
        If DelWarn[Filno].W > 0 Then Begin
          Link := No_Link;
          GetARec(Filno);
          Link := Up_N_Down;
          Delok := true;
          i := DelWarn[Filno].W;
          If (LinkType[i, Filno] = 'U') And (LinkBack[i].F = Filno) Then Begin
            If FilInUse[i] Then Begin
              TKey := GetLink(i);
              TKey2 := TKey;
              SearchKey(IdxKey[i, 1]^, Recno[i], TKey2);
              If OK And CompareKey(TKey, TKey2) Then Begin
                RingClear(false);
                Delok := YesNoBox(Escaped, ifunc, false, DelWarn[Filno].M + #13
                  + 'Confirm deletion of this ' + DelMsg[Filno, 1] + #13 + DelMsg[Filno, 2],
                  DHelpTopic, MC, CFG.WarnColor);
              End
              Else Delok := true;
            End;
          End;
          If Not Delok Then exit;
        End;
        If Scrn_Active Then PleaseWait;
        OK := True;
        Flush_DF := False;
        Flush_IF := False;
        DelARec(Filno);
        FlushAFile(Filno);
        If TOK Then Begin
          Recno[Filno] := NextRec;
          FindMulti(Filno, Recno[Filno], NxtKey);
          If Scrn_Active Then EraseBoxWindow;
          If OK Then Begin
            GetARec(Filno);
            If Status_OK(Filno) Then
              Display_Rec
            Else
              Top_Record;
          End
          Else
            Top_Record;
        End
        Else Begin
          If Scrn_Active Then EraseBoxWindow;
          Top_Record;
        End;
        Flush_DF := True;
        Flush_IF := True;
      End;
    End;
  End;
  Func := '?';
End;

Procedure Edit_Rec(Const Fld: Integer);
Begin
  Repeat
    UPDATE_Edt(Filno, Fld)
  Until FldNum = MaxInt
End;

Procedure CloseFiles;
Var
  I, J: Integer;
Begin
  For I := 1 To MaxFilno Do If DBFileOpen[I] Then Begin
      CloseFile(DatF[I]^);
      Dispose(DatF[I]);
      For J := 1 To MaxKeyno Do
        If KeyLen[I, J] > 0 Then Begin
          CloseIndex(IdxKey[I, J]^);
          Dispose(IdxKey[I, J]);
        End;
    End;
  FillChar(DBFileOpen, SizeOf(DBFileOpen), false);
End;

Function Add_Record: boolean;
Var
  SavR,
    Recd: LongInt;
  TF, Keyno: Integer;
  R: Real;
  Rav: Boolean;

  Procedure Remove_Keys(Const BadKey: Integer);
  Var
    Recd: LongInt;
    Keyno: Integer;
  Begin
    For Keyno := 1 To BadKey - 1 Do Begin
      Recd := Recno[Filno];
      DeleteKey(IdxKey[Filno, Keyno]^, Recd, TKeyTab[Keyno]);
    End
  End;

  Procedure AddARec;
  Begin
    Case Filno Of
      1: Begin
          PLAYERN.RECSTAT := 0;
          AddRec(DatF[1]^, Recno[1], PLAYERN)
        End;
      2: Begin
          PLAYERGR.RECSTAT := 0;
          PLAYERGR.RECORD_NO := PLAYERN.RECORD_NO;
          AddRec(DatF[2]^, Recno[2], PLAYERGR)
        End;
      3: Begin
          CLUBDEF.RECSTAT := 0;
          AddRec(DatF[3]^, Recno[3], CLUBDEF)
        End;
      4: Begin
          CLUBGAME.RECSTAT := 0;
          CLUBGAME.CLUB_NO := CLUBDEF.CLUB_NO;
          AddRec(DatF[4]^, Recno[4], CLUBGAME)
        End;
      5: Begin
          ATTEND.RECSTAT := 0;
          ATTEND.RECORD_NO := PLAYERN.RECORD_NO;
          AddRec(DatF[5]^, Recno[5], ATTEND)
        End;
      6: Begin
          PERCENT.RECSTAT := 0;
          PERCENT.RECORD_NO := PLAYERN.RECORD_NO;
          AddRec(DatF[6]^, Recno[6], PERCENT)
        End;
    End
  End;

Begin
  If DBFReadOnly[Filno] Then exit;
  Add_Record := false;
  Rav := True;
  If LinkBack[Filno].F > 0 Then Rav := RecAvail[LinkBack[Filno].F];
  If (UsedRecs(DatF[Filno]^) >= MaxRec[Filno]) Or Not Rav Then Begin
    If Rav Then
      ErrMsge('Maximum Number of Records Reached, Cannot Add Any More', true)
    Else
      ErrMsge('Cannot Add Record, No Record Exists to Link Back to', true);
  End
  Else Begin
    If OK Then SavR := Recno[Filno] Else SavR := 0;
    If Scrn_Active Then Clr_Line(25);
    If Scrn_Active Then EditMenu;
    If (Func <> 'C') And Scrn_Active Then Clear_Rec(Filno);
    If Edit_Add Then
      Case Filno Of
        1: Begin
            PLAYERN.RECORD_NO := num2key(DBSer(DatF[1]^, true), 3);
            PLAYERN.TOT_POINTS := Num2Key(0, 4);
            PLAYERN.Y_T_D := num2key(0, 4);
            PLAYERN.M_T_D := num2key(0, 4);
            PLAYERN.TOURN_PTS := num2key(0, 4);
            PLAYERN.START_DATE := sysdate;
            PLAYERN.RAN_CHECK := Num2Key(Random(255), 1);
            PLAYERN.ELIG_PTS := Num2Key(0, 4);
            Display_Rec
          End;
        2: Begin
            PLAYERGR.RECORD_NO := PLAYERN.record_no;
            Display_Rec
          End;
        4: Begin
            CLUBGAME.STRAT1_MP := '   0';
            CLUBGAME.STRAT1_MP := FSTR(VALU(CLUBGAME.STRAT1_MP), 4, 0);
            CLUBGAME.STRAT2_MP := '   0';
            CLUBGAME.STRAT2_MP := FSTR(VALU(CLUBGAME.STRAT2_MP), 4, 0);
            CLUBGAME.STRAT3_MP := '   0';
            CLUBGAME.STRAT3_MP := FSTR(VALU(CLUBGAME.STRAT3_MP), 4, 0);
            CLUBGAME.STRATIFIED := CheckCopy('1', CLUBGAME.stratified);
            CLUBGAME.STRATIFIED := FSTR(VALU(CLUBGAME.STRATIFIED), 1, 0);
            CLUBGAME.HANDICAP := 'N';
            CLUBGAME.DIRECTOR := CLUBDEF.club_manager;
            CLUBGAME.PAIR_TEAM := 'P';
            CLUBGAME.GAME_TYPE := 'O';
            CLUBGAME.RATING := ' 1';
            CLUBGAME.SESSION_NO := '1';
            CLUBGAME.SESSION_NO := FSTR(VALU(CLUBGAME.SESSION_NO), 1, 0);
            CLUBGAME.EVENT_DATE := sysdate;
            Display_Rec
          End;
        5: Begin
            ATTEND.BOARDS := '  0';
            ATTEND.BOARDS := FSTR(VALU(ATTEND.BOARDS), 3, 0);
            ATTEND.RANK_HIGH := '  0';
            ATTEND.RANK_HIGH := FSTR(VALU(ATTEND.RANK_HIGH), 3, 0);
            ATTEND.RANK_LOW := '  0';
            ATTEND.RANK_LOW := FSTR(VALU(ATTEND.RANK_LOW), 3, 0);
            ATTEND.PAIR_NO := '  1';
            ATTEND.PAIR_NO := FSTR(VALU(ATTEND.PAIR_NO), 3, 0);
            ATTEND.DIRECTION := '1';
            ATTEND.DIRECTION := FSTR(VALU(ATTEND.DIRECTION), 1, 0);
            ATTEND.LINK_FILE := ' ';
            ATTEND.HASH := Num2Key(3, 0);
            ATTEND.SESSION_NO := CheckCopy('1', ATTEND.session_no);
            ATTEND.SESSION_NO := FSTR(VALU(ATTEND.SESSION_NO), 1, 0);
            ATTEND.CLUB_REPORT := CheckCopy('N', ATTEND.club_report);
            ATTEND.MP_COLOR := CheckCopy('1', ATTEND.mp_color);
            ATTEND.MP_TYPE := CheckCopy('1', ATTEND.mp_type);
            ATTEND.STRATUM := '1';
            ATTEND.STRATUM := FSTR(VALU(ATTEND.STRATUM), 1, 0);
            Display_Rec
          End;
        6: Begin
            PERCENT.PERCENT_DATE := CheckCopy(sysdate, PERCENT.percent_date);
            PERCENT.HASH := Num2Key(0, 3);
            Display_Rec
          End;
      End;
    If Scrn_Active And (DBEXCode <> Qit_Key) Then Edit_Rec(0);
    If DBEXcode <> Qit_Key Then Begin
      If Scrn_Active Then Begin
        If Assigned(PostEditProc) Then PostEditProc(Filno);
        PleaseWait;
      End;
      AddARec;
      Keyno := 1; OK := True;
      While (Keyno <= MaxKeyno) And OK And
        (DBEXcode <> Qit_Key) Do Begin
        If KeyLen[Filno, Keyno] > 0 Then Begin
          TKeyTab[Keyno] := GetKey(Filno, Keyno);
          AddKey(IdxKey[Filno, Keyno]^, Recno[Filno], TKeyTab[Keyno]);
          If Not OK And (KeyUse[Filno, Keyno] <> 'E') Then Begin
            ErrMsge('A key field already exists in another record. Record NOT added.',
              false);
            Recd := Recno[Filno];
            DeleteRec(DatF[Filno]^, Recd);
            Remove_Keys(Keyno);
            Recno[Filno] := SavR;
          End
          Else While Not OK And (DBEXcode <> Qit_Key) Do Begin
              If Scrn_Active Then EraseBoxWindow;
              ErrMsge(DupErr, true);
              {Edit_Rec(KeyFld[Filno,Keyno]);}
              DBEXcode := Qit_Key;
              If Scrn_Active Then PleaseWait;
              If DBEXcode = Qit_Key Then Begin
                Recd := Recno[Filno];
                DeleteRec(DatF[Filno]^, Recd);
                Remove_Keys(Keyno);
                Recno[Filno] := SavR;
              End
              Else Begin
                TKeyTab[Keyno] := GetKey(Filno, Keyno);
                AddKey(IdxKey[Filno, Keyno]^, Recno[Filno], TKeyTab[Keyno]);
                If OK Then PutARec(Filno)
              End
            End
        End;
        Inc(Keyno);
      End;
      If Scrn_Active Then EraseBoxWindow;
      Add_Record := true;
    End
    Else
      Recno[Filno] := SavR;
    GetARec(Filno);
    If Scrn_Active Then Begin
      Display_Rec;
      setabn(false);
    End;
  End;
End;

Procedure FixKey(Const Fno, Kno: Integer; Var KEYVAR: KeyStr);
Begin
  Case Fno Of
    1: Begin
        Case Kno Of
          1: KeyVar := Upper(KeyVar);
          2: KeyVar := player_key(KeyVar);
        End;
      End;
  End;
End;

Function GetKey(Const Fno, Kno: Integer): KeyStr;
Var
  TKey: KeyStr;
Begin
  TKey := '';
  Case Fno Of
    1: Begin
        Case Kno Of
          1: Begin
              SKey := Upper(PLAYERN.LAST_NAME);
              TKey := TKey + SKey;
              SKey := upper(PLAYERN.FIRST_NAME);
              SKey := Copy(SKey, 1, 8);
              TKey := TKey + SKey;
            End;
          2: Begin
              SKey := player_key(PLAYERN.PLAYER_NO);
              TKey := TKey + SKey;
            End;
          3: Begin
              SKey := PLAYERN.RECORD_NO;
              TKey := TKey + SKey;
            End;
        End;
      End;
    2: Begin
        Case Kno Of
          1: Begin
              SKey := PLAYERGR.RECORD_NO;
              TKey := TKey + SKey;
              SKey := PLAYERGR.GROUP;
              TKey := TKey + SKey;
            End;
        End;
      End;
    3: Begin
        Case Kno Of
          1: Begin
              SKey := CLUBDEF.CLUB_NO;
              TKey := TKey + SKey;
            End;
        End;
      End;
    4: Begin
        Case Kno Of
          2: Begin
              SKey := CLUBGAME.EVENT_NCODE;
              TKey := TKey + SKey;
              SKey := CLUBGAME.SESSION_NO;
              TKey := TKey + SKey;
            End;
          1: Begin
              SKey := CLUBGAME.CLUB_NO;
              TKey := TKey + SKey;
              SKey := IdxDate(CLUBGAME.EVENT_DATE);
              TKey := TKey + SKey;
              SKey := CLUBGAME.CLUB_SESS_NO;
              TKey := TKey + SKey;
              SKey := CLUBGAME.SESSION_NO;
              TKey := TKey + SKey;
              SKey := CLUBGAME.SECTION;
              TKey := TKey + SKey;
            End;
          3: Begin
              SKey := CLUBGAME.HASH;
              TKey := TKey + SKey;
            End;
        End;
      End;
    5: Begin
        Case Kno Of
          1: Begin
              SKey := ATTEND.RECORD_NO;
              TKey := TKey + SKey;
              SKey := ATTEND.HASH;
              TKey := TKey + SKey;
              SKey := ATTEND.SESSION_NO;
              TKey := TKey + SKey;
            End;
          2: Begin
              SKey := ATTEND.HASH;
              TKey := TKey + SKey;
              SKey := ATTEND.SESSION_NO;
              TKey := TKey + SKey;
              SKey := ATTEND.SECTION;
              TKey := TKey + SKey;
            End;
          3: Begin
              SKey := ATTEND.HASH;
              TKey := TKey + SKey;
              SKey := ATTEND.RECORD_NO;
              TKey := TKey + SKey;
              SKey := ATTEND.SESSION_NO;
              TKey := TKey + SKey;
            End;
        End;
      End;
    6: Begin
        Case Kno Of
          1: Begin
              SKey := PERCENT.RECORD_NO;
              TKey := TKey + SKey;
              SKey := IdxDate(PERCENT.PERCENT_DATE);
              TKey := TKey + SKey;
            End;
          2: Begin
              SKey := PERCENT.CLUB_NO;
              TKey := TKey + SKey;
              SKey := IdxDate(PERCENT.PERCENT_DATE);
              TKey := TKey + SKey;
              SKey := PERCENT.CLUB_SESS_NO;
              TKey := TKey + SKey;
            End;
          3: Begin
              SKey := PERCENT.HASH;
              TKey := TKey + SKey;
            End;
        End;
      End;
    7: Begin
        Case Kno Of
          1: Begin
              SKey := CLUBSANC.CLUB_NO;
              TKey := TKey + SKey;
              SKey := CLUBSANC.CLUB_SESS_NO;
              TKey := TKey + SKey;
              SKey := CLUBSANC.GAME_TYPE;
              TKey := TKey + SKey;
            End;
        End;
      End;
  End;
  GetKey := TKey
End;

Procedure FindMulti(Const Fno: Integer; Var Recn: LongInt; Var Key2Find: OpenString);
Var
  SaveRecn: LongInt;
  SavK2F: KeyStr;
Begin
  SaveRecn := Recn;
  SavK2F := Key2Find;
  SearchKey(IdxKey[Fno, 1]^, Recn, Key2Find);
  OK := OK And CompareKey(SavK2F, Key2Find);
  While (Recn <> SaveRecn) And OK Do Begin
    NextKey(IdxKey[Fno, 1]^, Recn, Key2Find);
    OK := OK And CompareKey(SavK2F, Key2Find)
  End
End;

Procedure EditMenu;
Begin
  Clr_Line(25);
  ScrDsp('ESC:Abort, F9: Accept Record, F5:ClrEol, F1:Help, Ins, Del, Home, End, Tab,     ',
    GetTColor(defattrt), 1, 25);
  ScrDsp('ESC', _PV, 1, 25); ScrDsp('F9', _PV, 12, 25);
  ScrDsp('F5', _PV, 31, 25); ScrDsp('F1', _PV, 42, 25);
  ScrDsp('Ins', _PV, 51, 25); ScrDsp('Del', _PV, 56, 25);
  ScrDsp('Home', _PV, 61, 25); ScrDsp('End', _PV, 67, 25);
  ScrDsp('Tab', _PV, 72, 25); ScrDsp(#27#26#24#25, _PV, 77, 25);
End;

Procedure Stat_Line;
Begin
  Clr_Line(1);
  ScrDsp(Pdd(GetPrePend(Filno) + DBNAMES[Filno, 0] + ' - Contains ' +
    Strip(IStr(UsedRecs(DatF[Filno]^), 10)) + ' Records.', 80, 1),
    GetTColor(defattrt), 1, 1);
End;

Procedure ErrMsge(Const Msge: String; Const tofile: boolean);
Begin
  RingClear(false);
  ErrBox(Pad(Msge, 23), BC, 0);
  If tofile Then send_to_error_file(Msge);
End;

Function Custom_Key(Const Scr, Fno: Integer; Var Fld: Integer; Var Key: Char): Boolean;
Begin
  Custom_Key := False; {Make True if you wish to abort Edit}
End;

Procedure Clear_Rec(Const Fno: Integer);
Var
  i: Integer;
Begin
  For i := 1 To MaxFilno Do
    If LinkBack[i].F = Fno Then
      Clear_Rec(i);
  UPDATE_Clr(Fno);
End;

Function GetLink(Const Nummer: Integer): KeyStr;
Var
  TKey: KeyStr;
Begin
  TKey := '';
  Case Nummer Of
    2: Begin
        TKey := Copy(PLAYERN.RECORD_NO, 1, 3);
      End;
    5: Begin
        TKey := Copy(PLAYERN.RECORD_NO, 1, 3);
      End;
    6: Begin
        TKey := Copy(PLAYERN.RECORD_NO, 1, 3);
      End;
    4: Begin
        TKey := Copy(CLUBDEF.CLUB_NO, 1, 6);
      End;
    7: Begin
        TKey := Copy(CLUBDEF.CLUB_NO, 1, 6);
      End;
  Else TKey := ''
  End;
  GetLink := TKey
End;

Function Status_OK(Const Fno: Integer): Boolean;
Var
  TOK: Boolean;
Begin
  TOK := True;
  If LinkBack[Fno].F > 0 Then
    TOK := RecAvail[LinkBack[Fno].F];
  TOK := TOK And ((UsedRecs(DatF[Fno]^) > 0) And (UsedRecs(DatF[1]^) > 0));
  Case Fno Of
    2: TOK := TOK And (PLAYERGR.RECORD_NO = PLAYERN.RECORD_NO);
    5: TOK := TOK And (ATTEND.RECORD_NO = PLAYERN.RECORD_NO);
    6: TOK := TOK And (PERCENT.RECORD_NO = PLAYERN.RECORD_NO);
    4: TOK := TOK And (CLUBGAME.CLUB_NO = CLUBDEF.CLUB_NO);
    7: TOK := TOK And (CLUBSANC.CLUB_NO = CLUBDEF.CLUB_NO);
  End;
  Status_OK := TOK
End;

Procedure SkipNext(Const Fno: Integer);
Begin
  NextKey(IdxKey[Fno, 1]^, Recno[Fno], Key);
  OK := OK And CompareKey(GetLink(Fno), Key);
  If Not OK Then Begin
    RingClear(false);
    PrevKey(IdxKey[Fno, 1]^, Recno[Fno], Key);
  End;
End;

Procedure SkipPrev(Const Fno: Integer);
Begin
  PrevKey(IdxKey[Fno, 1]^, Recno[Fno], Key);
  OK := OK And CompareKey(GetLink(Fno), Key);
  If Not OK Then Begin
    RingClear(false);
    NextKey(IdxKey[Fno, 1]^, Recno[Fno], Key);
  End;
End;

Procedure GetARec;
Var
  i: Integer;
  TKey: KeyStr;
  Ans: Char;
Begin
  TKey := '';
  RecAvail[Fno] := OK;
  If OK Then Begin
    Case Fno Of
      1: GetRec(DatF[1]^, Recno[1], PLAYERN);
      2: GetRec(DatF[2]^, Recno[2], PLAYERGR);
      3: GetRec(DatF[3]^, Recno[3], CLUBDEF);
      4: GetRec(DatF[4]^, Recno[4], CLUBGAME);
      5: GetRec(DatF[5]^, Recno[5], ATTEND);
      6: GetRec(DatF[6]^, Recno[6], PERCENT);
      7: GetRec(DatF[7]^, Recno[7], CLUBSANC);
    End; { case }
    If Assigned(CheckRecord) Then CheckRecord(Fno);
  End;
  If Not (RecAvail[Fno]) Or
    ((Link <> No_Link) And (_Trace <> Fno) And Not (Status_Ok(Fno))) Then Begin
    Clear_Rec(Fno);
    RecAvail[Fno] := False
  End;
  If (Link <> No_Link) Then
    For i := 1 To MaxFilno Do
      If (LinkBack[i].F = Fno) Then
        If Not Status_OK(i) Then Begin
          TKey := GetLink(i);
          SearchKey(IdxKey[i, 1]^, Recno[i], TKey);
          Ok := Ok And CompareKey(GetLink(i), TKey);
          GetARec(i);
        End;
  If (Fno = Filno) Then OK := Recavail[Fno];
End;

Procedure PutARec;
Begin
  If DBFReadOnly[Fno] Then exit;
  If Assigned(CheckPutRecord) Then CheckPutRecord(Fno);
  Case Fno Of
    1: PutRec(DatF[1]^, Recno[1], PLAYERN);
    2: PutRec(DatF[2]^, Recno[2], PLAYERGR);
    3: PutRec(DatF[3]^, Recno[3], CLUBDEF);
    4: PutRec(DatF[4]^, Recno[4], CLUBGAME);
    5: PutRec(DatF[5]^, Recno[5], ATTEND);
    6: PutRec(DatF[6]^, Recno[6], PERCENT);
  End;
End;

Function GetKeyFld(Const Fno, Kno: Integer): KeyStr;
Var
  TKey: KeyStr;
Begin
  TKey := '';
  Case Fno Of
    1: Begin
        Case Kno Of
          1: TKey := PLAYERN.LAST_NAME;
          2: TKey := PLAYERN.PLAYER_NO;
          3: TKey := PLAYERN.RECORD_NO;
        End;
      End;
    2: Begin
        Case Kno Of
          1: TKey := PLAYERGR.RECORD_NO;
        End;
      End;
    3: Begin
        Case Kno Of
          1: TKey := CLUBDEF.CLUB_NO;
        End;
      End;
    4: Begin
        Case Kno Of
          2: TKey := CLUBGAME.EVENT_NCODE;
          1: TKey := CLUBGAME.CLUB_NO;
          3: TKey := CLUBGAME.HASH;
        End;
      End;
    5: Begin
        Case Kno Of
          1: TKey := ATTEND.RECORD_NO;
          2: TKey := ATTEND.HASH;
          3: TKey := ATTEND.HASH;
        End;
      End;
    6: Begin
        Case Kno Of
          1: TKey := PERCENT.RECORD_NO;
          2: TKey := PERCENT.CLUB_NO;
          3: TKey := PERCENT.HASH;
        End;
      End;
    7: Begin
        Case Kno Of
          1: TKey := CLUBSANC.CLUB_NO;
        End;
      End;
  End;
  GetKeyFld := TKey
End;

Procedure Display_Frame;
Begin
  If Scrn_Active Then Begin
    UPDATE_Frm;
    If Assigned(FrameAdjust) Then FrameAdjust(Scrno, Filno);
  End;
End;

Procedure Display_Rec;
Begin
  If Scrn_Active Then Begin
    If Assigned(PreDisplayRec) Then PreDisplayRec(Scrno, Filno);
    UPDATE_Dsp(Filno);
    If Assigned(PostDisplayRec) Then PostDisplayRec(Scrno, Filno);
    Stat_Line;
  End;
End;

Function List_N_Choose(Const Fno, Kno: Integer; Const Kstr: KeyStr;
  Const DefaultPick: KeyStr; Const Title: String;
  Const Location: byte): LongInt;
Var
  FVAL: LONGINT;
  t_kstr: keystr;
  l_hdr: String[80];
  Y, vertcent,
    ans: word;
  TotWidth,
    XLow, XHigh,
    YLow, YHigh: byte;
  oldpickmatrix: byte;
Begin
  Link := No_Link;
  pick_fno := Fno;
  pick_kno := Kno;
  pick_kstr := Kstr;
  t_kstr := Kstr;
  SaveKey := Kstr;
  FVAL := recno[fno];
  SCRN_ACTIVE := False;
  If Length(Title) > 0 Then L_Hdr := Title
  Else Case FNO Of
      1: L_Hdr := FindFormat(1, 1);
      3: L_Hdr := FindFormat(3, 1);
      4: L_Hdr := FindFormat(4, 1);
      5: L_Hdr := FindFormat(5, 1);
      6: L_Hdr := FindFormat(6, 1);
      7: L_Hdr := FindFormat(7, 1);
    Else L_HDR := 'Displayed in ' + KEYNAMTAB[FNO, KNO] + ' Sequence.';
    End; { CASE }
  y := 0;
  SearchKey(IdxKey[fno, kno]^, recno[fno], t_kstr);
  prevKey(IdxKey[fno, kno]^, recno[fno], t_kstr);
  ok := true;
  If Length(DefaultPick) < 1 Then Ans := 1
  Else Ans := 0;
  While ok Do Begin
    NEXTKEY(IdxKey[fno, kno]^, recno[fno], t_kstr);
    OK := OK And CompareKey(Kstr, t_kstr);
    If Not macro Then If KeyPressed Then
        If UpCase(ReadKb) = Qit_Key Then OK := false;
    If ok Then Begin
      Inc(y);
      If (Ans < 1) And CompareKey(t_kstr, DefaultPick) Then Ans := y;
    End;
  End;
  prevKEY(IdxKey[fno, kno]^, recno[fno], t_kstr);
  If Ans < 1 Then Ans := 1;
  If y <= 16 Then vertcent := y Div 2
  Else vertcent := 8;
  ok := true;
  last_pick_item := y;
  TotWidth := length(SendFileName(y));
  pick_kstr := Kstr;
  If TotWidth > 76 Then TotWidth := 76;
  If TotWidth < length(l_hdr) Then TotWidth := length(l_hdr);
  SetPickSize(YLow, XLow, YHigh, XHigh, TotWidth + 1, y, Location);
  oldpickmatrix := PickMatrix;
  PickMatrix := 1;
  If PickWindow(SendFileName, y, XLow, YLow, XHigh, YHigh, True, CFG.PickColor, l_hdr, Ans) Then
    Case pickcmdnum Of
      PKSselect: Begin
          FVAL := RecTab[1].Ref;
          Link := No_Link;
        End;
      PKSexit: FVAL := 0;
    End;
  PickMatrix := oldpickmatrix;
  Scrn_Active := True;
  List_N_Choose := FVAL;
End;

Function List(Const Fno, Kno: Integer; Var KS: KeyStr): KeyStr;
Var
  TRecno: LongInt;
  FldLength: Integer;
Begin
  If (Fno <> Filno) And (KeyLen[Fno, Kno] > 0) Then Begin
    Scrn_Active := False;
    FldLength := Length(KS);
    KS := '';
    SaveKey := KS;
    ClearKey(IdxKey[Fno, Kno]^);
    NextKey(IdxKey[Fno, Kno]^, Recno[Fno], KS);
    KS := '';
    TRecno := List_N_Choose(Fno, Kno, KS, '', '', MC);
    OK := True;
    If TRecno <> 0 Then Begin
      Recno[Fno] := TRecno;
      Link := No_Link;
      GetARec(Fno);
      Link := Up_N_Down;
      KS := GetKeyFld(Fno, Kno);
    End
    Else Begin
      FillStr(KS, FldLength, ' ');
      Clear_Rec(Fno);
    End;
    Scrn_Active := True;
    {Display_Frame;
    Display_Rec;
    EditMenu;}
  End;
  List := ''
End;


Function Found(Const Fno, Kno: Integer; ChkKey: KeyStr): Boolean;
Begin
  If (Fno <> Filno) And (KeyLen[Fno, Kno] > 0) Then Begin
    FindKey(IdxKey[Fno, Kno]^, Recno[Fno], ChkKey);
    If OK Then Case Fno Of
        1: GetRec(DatF[1]^, Recno[1], PLAYERN);
        2: GetRec(DatF[2]^, Recno[2], PLAYERGR);
        3: GetRec(DatF[3]^, Recno[3], CLUBDEF);
        4: GetRec(DatF[4]^, Recno[4], CLUBGAME);
        5: GetRec(DatF[5]^, Recno[5], ATTEND);
        6: GetRec(DatF[6]^, Recno[6], PERCENT);
        7: GetRec(DatF[7]^, Recno[7], CLUBSANC);
      End; {case}
    Found := OK;
  End
  Else
    Found := False;
  OK := True; (* RESET OK for SUBSEQUENT File ALIGNMENT *)
End;

Begin
  FillChar(DBFileOpen, SizeOf(DBFileOpen), false);
  FillChar(DBNeedToOpen, SizeOf(DBNeedToOpen), true);
  FillChar(DBFReadOnly, SizeOf(DBFReadOnly), false);
End.

