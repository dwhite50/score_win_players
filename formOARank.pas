Unit formOARank;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ResizeKit, ExtCtrls, ActnList,
  XPStyleActnCtrls, ActnMan, Mask, LMDCustomComponent, LMDCustomHint,
  LMDHint, LMDCustomButton, LMDButton, LMDControl, LMDBaseControl,
  LMDBaseGraphicControl, LMDGraphicControl, LMDCustomGraphicLabel,
  LMDGraphicLabel, ovceditf, ovcedpop, ovcedcal, Grids, ovcbase, ovcef,
  ovcsf, ovccmbx, ovcpb, ovcpf, dBase, DB33, vListBox, PVio, LibUtil,
  STSTRS, MPDef, MTables, NatMps,hh;

Type
  TfrmOARank = Class(TForm)
    ResizeKit1: TResizeKit;
    StatusBar1: TStatusBar;
    StaticText1: TStaticText;
    GBMenu: TGroupBox;
    Shape23: TShape;
    ButNext: TButton;
    ButPrev: TButton;
    ButFind: TButton;
    ButTop: TButton;
    ButLast: TButton;
    ButEdit: TButton;
    ButAdd: TButton;
    ButCopy: TButton;
    ButDelete: TButton;
    ButQuit: TButton;
    ButtonF5Key: TButton;
    ButtonF7Key: TButton;
    StaticTextF5Key: TStaticText;
    StaticTextF7Key: TStaticText;
    ButtonF1Key: TButton;
    StaticTextF1Key: TStaticText;
    ActionManager1: TActionManager;
    ButNextAction: TAction;
    ButPrevAction: TAction;
    ButFindAction: TAction;
    ButTopAction: TAction;
    ButLastAction: TAction;
    ButEditAction: TAction;
    ButAddAction: TAction;
    ButCopyAction: TAction;
    ButDeleteAction: TAction;
    ButQuitAction: TAction;
    F5Key: TAction;
    F7Key: TAction;
    F1Key: TAction;
    gbOARank: TGroupBox;
    Label1: TLabel;
    MELastName: TMaskEdit;
    Label2: TLabel;
    MEFirstName: TMaskEdit;
    Label3: TLabel;
    MEPlayerNo: TMaskEdit;
    MERecordNumber: TMaskEdit;
    Label5: TLabel;
    Shape1: TShape;
    Shape2: TShape;
    Shape4: TShape;
    LMDGraphicLabel1: TLMDGraphicLabel;
    LMDButOARankDone: TLMDButton;
    LMDHint1: TLMDHint;
    ActionManager2: TActionManager;
    ButOARankDone: TAction;
    Label4: TLabel;
    MEGameLink: TMaskEdit;
    Shape3: TShape;
    sgOARank: TStringGrid;
    Label8: TLabel;
    Label9: TLabel;
    OSFEventSessNo: TOvcSimpleField;
    OSFSection: TOvcSimpleField;
    Label6: TLabel;
    OSFRankLow: TOvcSimpleField;
    Label7: TLabel;
    OSFRankType: TOvcSimpleField;
    OSFRankHigh: TOvcSimpleField;
    Label11: TLabel;
    Label12: TLabel;
    OSFStratum: TOvcSimpleField;
    LabelDirection: TLabel;
    OSFDirection: TOvcSimpleField;
    LabelDirectionText: TLabel;
    Action1: TAction;
    Action2: TAction;
    Procedure F5KeyExecute(Sender: TObject);
    Procedure F7KeyExecute(Sender: TObject);
    Procedure pSetFunctionKeys;
    Procedure ButQuitActionExecute(Sender: TObject);
    Procedure FormActivate(Sender: TObject);
    Procedure F1KeyExecute(Sender: TObject);
    Procedure pFillOARankFields;
    Procedure pGetOARankRecord;
    Procedure ODEDateExit(Sender: TObject);
    Procedure pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pAbortOARankChanges;
    Procedure pSaveOARankChanges;
    Procedure pGetClubDefRecord;
    Procedure ButNextActionExecute(Sender: TObject);
    Procedure pNextPerRec;
    Procedure ButPrevClick(Sender: TObject);
    Procedure pPrevPerRec;
    Procedure pFillOARankGrid;
    Procedure pClearPerRecords;
    Procedure pColPerHeadings;
    Procedure pGetNextPerRec;
    Procedure sgOARankClick(Sender: TObject);
    Procedure pMoveToPerRecord;
    Procedure ButEditActionExecute(Sender: TObject);
    Procedure LMDButOARankDoneClick(Sender: TObject);
    Procedure sgOARankDblClick(Sender: TObject);
    Procedure pPrintRank;
    Procedure ButAddActionExecute(Sender: TObject);
    Procedure ButCopyActionExecute(Sender: TObject);
    Procedure pClearPerFields;
    Procedure pCopyOARankRecData;
    Procedure pCheckOARankEditFields;
    Procedure pOARankDone;
    Procedure sgOARankKeyPress(Sender: TObject; Var Key: Char);
    Procedure ButPrevActionExecute(Sender: TObject);
    Procedure ButTopActionExecute(Sender: TObject);
    Procedure ButLastActionExecute(Sender: TObject);
    Procedure ButDeleteActionExecute(Sender: TObject);
    Procedure ButFindActionExecute(Sender: TObject);
    Procedure OSFDirectionExit(Sender: TObject);
    Procedure Action1Execute(Sender: TObject);
    Procedure Action2Execute(Sender: TObject);
    Procedure pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
    Procedure pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
    Procedure pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
    Procedure pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
    Procedure pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
    Procedure pResetFldAttrib;
    Procedure OSFEventSessNoEnter(Sender: TObject);
    Procedure OSFEventSessNoExit(Sender: TObject);
    Procedure OSFSectionEnter(Sender: TObject);
    Procedure OSFSectionExit(Sender: TObject);
    Procedure OSFRankLowEnter(Sender: TObject);
    Procedure OSFRankLowExit(Sender: TObject);
    Procedure OSFRankHighEnter(Sender: TObject);
    Procedure OSFRankHighExit(Sender: TObject);
    Procedure OSFRankTypeEnter(Sender: TObject);
    Procedure OSFRankTypeExit(Sender: TObject);
    Procedure OSFStratumEnter(Sender: TObject);
    Procedure OSFStratumExit(Sender: TObject);
    Procedure OSFDirectionEnter(Sender: TObject);
    Procedure ButtonF1KeyClick(Sender: TObject);
    Function FormHelp(Command: Word; Data: Integer;
      Var CallHelp: Boolean): Boolean;
  private
    { Private declarations }
  public
    { Public declarations }
    bPerRecFound: Boolean;
    bClubDefRecFound: Boolean;
    bLinked: Boolean;
  End;

Var
  frmOARank: TfrmOARank;
  iRow: integer; // String Grid Row Number
  iChildRecs: Integer;
  ksERecNoKey, ksETempKey: keystr;
  ksARecNoKey, ksATempKey: keystr;
  ksPN, ksPNTemp: keystr;
  aOK: Boolean;

Implementation

Uses formPlayers, Util1, Util2, CSDef, LibData, YesNoBoxu;

{$I dbnames.dcl}
{$I dbfiles.dcl}
{$I E_Extra.dcl}
{$I STSTRS.dcl}
{$I DFNDFORM.INC}
{$I i_update.inc}

{$R *.dfm}

Procedure TfrmOARank.F5KeyExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmOARank.F7KeyExecute(Sender: TObject);
Begin
  pPrintRank;
End;

Procedure TfrmOARank.pPrintRank;
Var
  oldrecno: longint;
  tempkey, savekey: keystr;
  tsavekey: keystr;
  last_link: char;
Begin
  oldrecno := recno[Filno];
  tsavekey := getkey(Filno, 1);
  tempkey := getkey(Playern_no, 3);
  savekey := tempkey;
  SearchKey(IdxKey[Filno, 1]^, recno[Filno], tempkey);
  ok := ok And CompareKey(tempkey, savekey);
  If Not ok Then With Playern Do Begin
      ErrBox('No rank records found for ' + Trim(First_Name) + ' '
        + Trim(Last_Name), MC, 0);
      exit;
    End;
  PleaseWait;
  OpenDestination(2, 55, 80);
  SetLineSpace(6);
  ReportLine(0, ppica, 1, '');
  With Playern Do ReportLine(1, pnone, 1, 'RANKINGS for '
      + Trim(Player_no) + ' ' + Trim(First_Name) + ' ' + Trim(Last_Name) + ' as of '
      + Slash(SysDate));
  last_link := ' ';
  While ok Do With OArank Do Begin
      getarec(Filno);
      If last_link <> link_file[1] Then Begin
        last_link := link_file[1];
        Case last_link Of
          'T': ReportLine(2, pnone, 1,
              '    DATE     SANCTION  SESS RANK  SCORE  EVENT CODE & NAME');
          'C': ReportLine(2, pnone, 1, '    DATE     CLUB  SESS  MPs   %   SECTION & EVENT');
        End;
      End;
      //ReportLine(1, pnone, 1, '  ' + show_link(last_link, hash, 75, true));
      If cfg.Tournament Then
        ReportLine(1, pnone, 1, '  ' + show_link(True, last_link, hash))
      Else
        ReportLine(1, pnone, 1, '  ' + show_link(False, last_link, hash));
      nextkey(IdxKey[Filno, 1]^, recno[Filno], tempkey);
      ok := ok And CompareKey(tempkey, savekey);
    End;
  ok := true;
  recno[Filno] := oldrecno;
  If Status_Ok(Filno) And (oldrecno > 0) Then Begin
    findmulti(Filno, recno[Filno], tsavekey);
    getarec(Filno);
  End;
  Close_Destination;
  PVIO.EraseBoxWindow;
End;

Procedure TfrmOARank.pSetFunctionKeys;
Var
  iComponentLoop: Integer;
  tcTemp: TComponent;
  iButAdd, iButQuit, iButLMD, iPercentNo, iPlayerNo: Integer;
  bTurnOff: Boolean;
  sButName: String;
Begin
  { F1 Key }
  ButtonF1Key.Hint := frmPlayers.sStandFuncKeyHint + StaticTextF1Key.Caption;
  { F5 Key }
  ButtonF5Key.Hint := frmPlayers.sStandFuncKeyHint + StaticTextF5Key.Caption;
  { F7 Key }
  ButtonF7Key.Hint := frmPlayers.sStandFuncKeyHint + StaticTextF7Key.Caption;
  { If there is not a record for this player in the Percent Table, only allow for Adding a record
    or Quiting }
  iPercentNo := Key2Num(OARank.RECORD_NO);
  iPlayerNo := Key2Num(PLAYERN.RECORD_NO);
  { bPerRecFound = was a Percent Record found for this Player? }
  bLinked := (iPercentNo <> iPlayerNo);
  { If there is no Percent Record for this Player }
  If bLinked Then Begin
    { Loop thru all components looking for TButtons, certain buttons should not be available,
      if there is no existing Percent Record }
    For iComponentLoop := (ComponentCount - 1) Downto 0 Do Begin
      tcTemp := Components[iComponentLoop];
      If (tcTemp Is TButton) And (Pos('KEY', UpperCase((tcTemp As TButton).Name)) = 0) Then Begin
        { Don't turn off the Add, Quit Buttons or the Function Keys }
        sButName := UpperCase((tcTemp As TButton).Name);
        // ==================================================================
        { Check to see if the Current Button is not the Add or Quit buttons }
        iButAdd := Pos('BUTADD', sButName);
        iButQuit := Pos('BUTQUIT', sButName);
        { Check to see if the Current Button is not an LMD button }
        iButLMD := Pos('LMD', sButName);
        { If adding the three values together is greater than zero, then it is one of the buttons
          we do not want to turn off }
        bTurnOff := ((iButAdd + iButQuit + iButLMD) = 0);
        If bTurnOff Then Begin
          (tcTemp As TButton).Enabled := False;
          (tcTemp As TButton).Action.OnExecute := Nil;
          (tcTemp As TButton).OnClick := Nil;
        End; { bTurnOff is True }
      End; { Is TButton and name does not contain the word "KEY" }
    End; { For Loop thru all form components looking for TButtons }
  End; { If there is no Percent Record for this Player }
End;

Procedure TfrmOARank.ButQuitActionExecute(Sender: TObject);
Begin
  Close;
End;

Procedure TfrmOARank.FormActivate(Sender: TObject);
Begin
  FilNo := frmPlayers.iRanking;
  { Must be set to true for Win32 applications. Used in Dbase and Fdbase }
  UseDBLinkages := True;
  Scrno := 3;
  HelpContext := 208;
  { frmPlayers.iRanking possible values:
    10 = using OARank.Dat
     8 = using Percent.Dat }
  pSetFunctionKeys;
  LMDGraphicLabel1.Caption := '';
  { Flush_DF & Flush_IF: Must be set to True for so that everytime a change is made to a "Record",
    dbase re-writes the table header and in Playern.dat, updates dbLastImpDate entry. Set them to
    False when you Delete a record, then immediately set them back to True }
  { This must be done as a standard whenever a table is opened for modifications }
  Flush_DF := True;
  Flush_IF := True;
  // -----------------------------------------------------------------------------
  If CSDef.CFG.Tournament Then StatusBar1.Panels[0].Text := 'Tournament'
  Else StatusBar1.Panels[0].Text := 'Club';
  { Check to make sure the drive letter is used in the Prepend variable. If it is not, the program
    will use the drive that the executable is running from! }
  //If Copy(dbase.Prepend, 1, 1) = '\' Then dbase.Prepend := 'C:' + dbase.Prepend;
  { Show the path to the table in use and the table name in StatusBar1. }
  StatusBar1.Panels[1].Text := UpperCase(dbase.Prepend + 'OARANK.DAT');
  { Store the number of records in iChildRecs }
  iChildRecs := db33.UsedRecs(DatF[Filno]^);
  StatusBar1.Panels[1].Text := StatusBar1.Panels[1].Text + ' - Contains ' +
    Trim(IntToStr(iChildRecs)) + ' Records.';
  // --------------------------------------------------------------------------
  ksPN := GetKey(Playern_no, 3);
  ksPNTemp := ksPN;
  //ClearKey(IdxKey[frmPlayers.iRanking, 1]^);
    { Search for records in the Percent table that match the Player Number key of the current
      Player Number record }
  db33.SearchKey(IdxKey[Filno, 1]^, Recno[Filno], ksPNTemp);
  //If (Ok And CompareKey(ksPNTemp, ksPN)) Then Begin
  pFillOARankFields;
  pFillOARankGrid;
  //End;
End;

Procedure TfrmOARank.F1KeyExecute(Sender: TObject);
Begin
  //Application.HelpCommand(HELP_CONTENTS, 0);
  pRunExtProg(frmOARank, cfg.LoadPath + 'WUtility', 'MANUAL\CONTENTS');
End;

Procedure TfrmOARank.pFillOARankFields;
Begin
  { Load Club Player information to the appropriate MaskEdit field from the current record }
  MELastName.Text := Playern.LAST_NAME;
  MEFirstName.Text := Playern.FIRST_NAME;
  MEPlayerNo.Text := Playern.PLAYER_NO;
  MERecordNumber.Text := IntToStr(Key2Num(PLAYERN.RECORD_NO));
  // ------------------------------------------------------------------
  pGetOARankRecord; // Get the percentage record for this player
  // ------------------------------------------------------------------
  If bPerRecFound Then Begin
    With OARank Do Begin
      MEGameLink.Text := LINK_FILE;
      // ------------------------------------------------------------------
      OSFEventSessNo.Text := SESSION_NO;
      OSFSection.Text := SECTION;
      OSFRankLow.Text := POS_LOW;
      OSFRankHigh.Text := POS_HIGH;
      OSFRankType.Text := RANK_TYPE;
      OSFStratum.Text := STRATUM;
      OSFDirection.Text := Direction;
    End;
  End
  Else Begin
    MEGameLink.Text := '';
    // ------------------------------------------------------------------
    OSFEventSessNo.Text := '';
    OSFSection.Text := '';
    OSFRankLow.Text := '0';
    OSFRankHigh.Text := '0';
    OSFRankType.Text := '';
    OSFStratum.Text := '';
    OSFDirection.Text := '1';
    // ------------------------------------------------------------------
  End;
  LabelDirectionText.Caption := LibData.fShowdir(OARANK.Direction);
End;

Procedure TfrmOARank.pGetOARankRecord;
Var
  ksXRecNoKey, ksXTempKey: keystr;
  bCompare: Boolean;
Begin
  ksXRecNoKey := GetKey(Playern_no, 3);
  ksXTempKey := ksXRecNoKey;
  //ClearKey(IdxKey[frmPlayers.iRanking, 1]^);
  { Search for records in the table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[Filno, 1]^, Recno[Filno], ksXTempKey);
  bCompare := (CompareKey(ksXTempKey, ksXRecNoKey));
  bPerRecFound := ((ok) And bCompare);
  If bPerRecFound Then Begin
    dBase.GetARec(Filno); // Store the record
  End;
End;

Procedure TfrmOARank.ODEDateExit(Sender: TObject);
Begin
  { Replace all blanks with zeros in the date }
  (Sender As TOvcDateEdit).Text := fFillDateBlanks((Sender As TOvcDateEdit).Text);
End;

Procedure TfrmOARank.pGenericKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { Map the Return key to the Tab key to move from one field to the next field to mimic DOS mode
    features }
  If key = VK_RETURN Then Begin
    key := 0;
    If iDataMode > 0 Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
  End;
  { If the F1 Key was pressed }
  If key = VK_F1 Then Begin
    If HelpContext > 0 Then Begin
      bF1KeyUsed := True;
    End
    Else bF1KeyUsed := False;
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortOARankChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Begin
    Key := 0;
    bF1KeyUsed := False;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then Begin
    FreeHintWin;
    If iDataMode > 0 Then pSaveOARankChanges;
  End;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmOARank.pGenericKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  { If the F1 Key was pressed }
  If key = VK_F1 Then Begin
    If HelpContext > 0 Then Begin
      bF1KeyUsed := True;
    End
    Else bF1KeyUsed := False;
  End;
  { If the ESCape key was pressed, Abort the record or record changes... }
  If (Key = VK_ESCAPE) And (Not bF1KeyUsed) Then Begin
    FreeHintWin;
    bEscKeyUsed := True;
    If iDataMode > 0 Then pAbortOARankChanges;
  End
  Else If (Key = VK_ESCAPE) And (bF1KeyUsed) Then Begin
    Key := 0;
    bF1KeyUsed := False;
  End;
  { If the F9 key was pressed, save the record or the changes... }
  If Key = VK_F9 Then If iDataMode > 0 Then pSaveOARankChanges;
  If ((Key = VK_UP) Or (Key = VK_Down)) And (GBMenu.Enabled) Then Begin
    If (Key = VK_UP) Then Begin
      ButPrev.Click;
      ButPrev.SetFocus;
    End
    Else Begin
      ButNext.Click;
      ButNext.SetFocus;
    End;
    Key := 0;
  End;
End;

Procedure TfrmOARank.pAbortOARankChanges;
Begin
  pOARankDone;
  { Reload the data from the table into the record }
  //getarec(OArank_no);
  Top_Record;
  If UsedRecs(DatF[OARank_no]^) > 0 Then Begin
    With OARank Do Begin
      MEGameLink.Text := LINK_FILE;
      // ------------------------------------------------------------------
      OSFEventSessNo.Text := SESSION_NO;
      OSFSection.Text := SECTION;
      OSFRankLow.Text := POS_LOW;
      OSFRankHigh.Text := POS_HIGH;
      OSFRankType.Text := RANK_TYPE;
      OSFStratum.Text := STRATUM;
      OSFDirection.Text := Direction;
      LabelDirectionText.Caption := LibData.fShowdir(Direction);
    End;
  End;
End;

Procedure TfrmOARank.pSaveOARankChanges;
Begin
  // Data Mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  Case iDataMode Of
    1, 3: Begin // Add or Copy Modes
        pCopyOARankRecData;
        dbase.Add_Record;
      End;
    2: Begin // Edit Mode
        pCheckOARankEditFields;
        frmPlayers.bKeyIsUnique := PostEditFixKeys;
        If Not frmPlayers.bKeyIsUnique Then Begin
          If (MessageDlg('This record already exists in this database.', mtError,
            [mbOK], 0) = mrOK) Then Begin
            OSFEventSessNo.SetFocus;
            Exit;
          End
          Else pAbortOARankChanges;
        End
        Else dbase.PutARec(Filno); { If everything is ok, then save the record }
      End;
  End;
  pOARankDone;
  pFillOARankGrid;
  bEscKeyUsed := False;
End;

Procedure TfrmOARank.pCopyOARankRecData;
Begin
  With OARank Do Begin
    SESSION_NO := Pad(OSFEventSessNo.Text,1);
    SECTION := Pad(OSFSection.Text,2);
    POS_LOW := LeftPad(Trim(OSFRankLow.Text),4);
    POS_HIGH := LeftPad(Trim(OSFRankHigh.Text),4);
    RANK_TYPE := Pad(OSFRankType.Text,1);
    STRATUM := Pad(OSFStratum.Text,1);
    Direction := Pad(OSFDirection.Text,1);
  End;
End;

Procedure TfrmOARank.pCheckOARankEditFields;
Begin
  With OARank Do Begin
    SESSION_NO := Pad(OSFEventSessNo.Text,1);
    SECTION := Pad(OSFSection.Text,2);
    POS_LOW := LeftPad(Trim(OSFRankLow.Text),4);
    POS_HIGH := LeftPad(Trim(OSFRankHigh.Text),4);
    RANK_TYPE := Pad(OSFRankType.Text,1);
    STRATUM := Pad(OSFStratum.Text,1);
    Direction := Pad(OSFDirection.Text,1);
  End;
End;

Procedure TfrmOARank.pGetClubDefRecord;
Var
  ksXRecNoKey, ksXTempKey: keystr;
Begin
  ksXRecNoKey := GetKey(Filno, 2);
  ksXTempKey := ksXRecNoKey;
  { Search for records in the table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[ClubDef_no, 1]^, Recno[ClubDef_no], ksXTempKey);
  bClubDefRecFound := ((ok) And (CompareKey(ksXTempKey, ksXRecNoKey)));
  If bClubDefRecFound Then Begin
    DBUseLinks := False;
    dBase.GetARec(ClubDef_no);
    DBUseLinks := True;
  End;
End;

Procedure TfrmOARank.ButNextActionExecute(Sender: TObject);
Begin
  If sgOARank.Row <> (sgOARank.RowCount - 1) Then sgOARank.Row := sgOARank.Row + 1;
  pMoveToPerRecord;
  If ButNext.Enabled Then ButNext.SetFocus;
End;

Procedure TfrmOARank.pNextPerRec;
Var
  FndKey: KeyStr;
Begin
  { Move the record pointer in the table to the next available record and load the data in the screen fields }
  LibData.SkipNext(Filno, FndKey);
  { Read data base file Playern_no.  Record number in Recno[Playern_no]}
  GetARec(Filno);
  { Load information to the appropriate field from the current record }
  pFillOARankFields;
End;

Procedure TfrmOARank.ButPrevClick(Sender: TObject);
Begin
  pPrevPerRec; // Move the record pointer in the table to the previous record and load the data in the screen fields
  If ButPrev.Enabled Then ButPrev.SetFocus;
End;

Procedure TfrmOARank.pPrevPerRec;
Var
  FndKey: KeyStr;
Begin
  { Move the record pointer in the table to the previous record and load the data in the screen fields }
  PrevKey(IdxKey[Filno, 1]^, Recno[Filno], FndKey);
  GetARec(Filno);
  { Must run Align_Rec to align children records for this record }
  dBase.Align_Rec(RecNo[Filno]);
  { Load information to the appropriate field from the current record }
  pFillOARankFields;
End;

Procedure TfrmOARank.pFillOARankGrid;
Begin
  { Fill the StringGrids with the Records for this Player }
  { pClearGroups: Clears the entries in the StringGrid }
  pClearPerRecords;
  // ---------------------------------------------------------------------------
  { Store the current records data to the string grid }
  iRow := 0;
  //==============================================================================================
  ksARecNoKey := GetKey(Playern_no, 3);
  ksATempKey := ksARecNoKey;
  { Search for records in the table that match the Player Record Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[Filno, 1]^, Recno[Filno], ksATempKey);
  aOK := OK;
  //==============================================================================================
  pColPerHeadings;
  If ok And CompareKey(ksATempKey, ksARecNoKey) Then Begin
    // Increment the Row Count for the next available Row in the string grid
    iRow := iRow + 1;
    If sgOARank.RowCount < iRow + 1 Then sgOARank.RowCount := sgOARank.RowCount + 1;
    dBase.GetARec(Filno); // Store the record
  End;

  While ok And CompareKey(ksATempKey, ksARecNoKey) Do Begin
    sgOARank.Cells[0, iRow] := OARank.SESSION_NO;
    sgOARank.Cells[1, iRow] := OARank.SECTION;
    sgOARank.Cells[2, iRow] := OARANK.POS_LOW;
    sgOARank.Cells[3, iRow] := OARANK.POS_HIGH;
    sgOARank.Cells[4, iRow] := OARANK.RANK_TYPE;
    sgOARank.Cells[5, iRow] := OARANK.STRATUM;
    // Go to the next record in the table
    pGetNextPerRec;
    If (aOK) And (CompareKey(ksATempKey, ksARecNoKey)) Then Begin
      dBase.GetARec(Filno); // Store the record
      // Increment the Row Count for the next available Row in the string grid
      iRow := iRow + 1;
      If sgOARank.RowCount < iRow + 1 Then sgOARank.RowCount := sgOARank.RowCount + 1;
    End
    Else Begin
      //==========================================================================================
      { Need to place record pointer back on the first record. Move String Grid back to
        row 1 also. }
      { Reset ksATempKey back to the intial value to go to the first linked record for the
        child record }
      ksATempKey := ksARecNoKey;
      db33.SearchKey(IdxKey[Filno, 1]^, Recno[Filno], ksATempKey);
      dBase.GetARec(Filno); // Store the record
      Break;
    End;
  End;
  If sgOARank.RowCount > 1 Then sgOARank.Row := 1 Else sgOARank.Row := 0;
End;

Procedure TfrmOARank.pClearPerRecords;
Var
  iForLoop: integer;
Begin
  { Clear out all previous entries in the String Grid for Groups in preparation
    for any entries from the current player record }
  For iForLoop := 0 To (sgOARank.RowCount - 1) Do Begin
    sgOARank.Rows[iForLoop].Clear;
  End;
  sgOARank.RowCount := 1;
End;

Procedure TfrmOARank.pColPerHeadings;
Begin
  sgOARank.Cells[0, iRow] := 'SESSION #';
  sgOARank.Cells[1, iRow] := 'SECTION';
  sgOARank.Cells[2, iRow] := 'RANK LOW';
  sgOARank.Cells[3, iRow] := 'RANK HIGH';
  sgOARank.Cells[4, iRow] := 'RANK TYPE';
  sgOARank.Cells[5, iRow] := 'STRATUM';
End;

Procedure TfrmOARank.pGetNextPerRec;
Begin
  NextKey(IdxKey[Filno, 1]^, Recno[Filno], ksATempKey);
  aOK := OK;
  OK := True;
End;

Procedure TfrmOARank.sgOARankClick(Sender: TObject);
Begin
  pMoveToPerRecord;
End;

Procedure TfrmOARank.pMoveToPerRecord;
Var
  iMove, iCurrRow: Integer;
Begin
  //ClearKey(IdxKey[frmPlayers.iRanking, 1]^);
  //==============================================================================================
  iCurrRow := sgOARank.Row;
  ksARecNoKey := GetKey(Playern_no, 3);
  ksATempKey := ksARecNoKey;
  { Search for records in the table that match the Player Number key of the current
    Player Number record }
  db33.SearchKey(IdxKey[Filno, 1]^, Recno[Filno], ksATempKey);
  iMove := 1;
  If iCurrRow > 0 Then Begin
    While iMove < sgOARank.Row Do Begin
      pGetNextPerRec;
      Inc(iMove);
    End;
    If (aOK) And (CompareKey(ksATempKey, ksARecNoKey)) Then Begin
      dBase.GetARec(Filno); // Store the record
      MEGameLink.Text := OARANK.LINK_FILE;
      // ------------------------------------------------------------------
      OSFEventSessNo.Text := OARank.SESSION_NO;
      OSFSection.Text := OARank.SECTION;
      OSFRankLow.Text := OARANK.POS_LOW;
      OSFRankHigh.Text := OARANK.POS_HIGH;
      OSFRankType.Text := OARANK.RANK_TYPE;
      OSFStratum.Text := OARANK.STRATUM;
      OSFDirection.Text := OARANK.Direction;
      LabelDirectionText.Caption := LibData.fShowdir(OARANK.Direction);
    End;
  End;
End;

Procedure TfrmOARank.ButEditActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  { In module Dbase call the procedure PreEditSaveKeys to save the KEYS in all tables we are
    editing so the application will know if a KEY Field has been changed and Indexes need to be
    regenerated }
  PreEditSaveKeys;
  LMDGraphicLabel1.Caption := 'Edit Mode';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 2;
  sDataMode := 'E';
  // Save the forms Position and Size coordinates so they can be restored when editing is complete
  frmPlayers.pSaveFormInfo;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asNormal;
  GBMenu.Enabled := False;
  GBMenu.Hide;
  gbOARank.Enabled := True;
  // Turn the Done Button on and make it visible
  LMDButOARankDone.Enabled := True;
  LMDButOARankDone.Visible := True;
  OSFEventSessNo.SetFocus;
End;

Procedure TfrmOARank.LMDButOARankDoneClick(Sender: TObject);
Begin
  pSaveOARankChanges;
  pOARankDone;
  bEscKeyUsed := False;
End;

Procedure TfrmOARank.pOARankDone;
Begin
  // Turn the Done Button on and make it invisible
  LMDButOARankDone.Enabled := False;
  LMDButOARankDone.Visible := False;
  LMDGraphicLabel1.Caption := '';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 0;
  sDataMode := null;
  ActionManager1.State := asNormal;
  ActionManager2.State := asSuspended;
  GBMenu.Enabled := True;
  GBMenu.Show;
  gbOARank.Enabled := False;
  frmPlayers.pSetFormBack;
  pResetFldAttrib;
End;

Procedure TfrmOARank.sgOARankDblClick(Sender: TObject);
Begin
  ButEditActionExecute(ButEdit);
End;

Procedure TfrmOARank.ButAddActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  LMDGraphicLabel1.Caption := 'Add Mode';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 1;
  sDataMode := 'A';
  // Save the forms Position and Size coordinates so they can be restored when editing is complete
  frmPlayers.pSaveFormInfo;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asNormal;
  GBMenu.Enabled := False;
  GBMenu.Hide;
  gbOARank.Enabled := True;
  // Turn the Done Button on and make it visible
  LMDButOARankDone.Enabled := True;
  LMDButOARankDone.Visible := True;
  //pClearPerFields;
  InitRecord(filno);
  pFillOARankFields;
End;

Procedure TfrmOARank.pClearPerFields;
Begin
  MEGameLink.Text := '';
  // ------------------------------------------------------------------
  OSFEventSessNo.Text := '';
  OSFSection.Text := '';
  OSFRankLow.Text := '0';
  OSFRankHigh.Text := '0';
  OSFRankType.Text := '';
  OSFStratum.Text := '';
  OSFDirection.Text := '1';
  LabelDirectionText.Caption := LibData.fShowdir(OARANK.Direction);
  // ------------------------------------------------------------------
End;

Procedure TfrmOARank.ButCopyActionExecute(Sender: TObject);
Begin
  bEscKeyUsed := False;
  LMDGraphicLabel1.Caption := 'Copy Mode';
  // Set the data mode: 0 = None, 1 = Add, 2 = Edit, 3 = Copy
  iDataMode := 3;
  sDataMode := 'C';
  // Save the forms Position and Size coordinates so they can be restored when editing is complete
  frmPlayers.pSaveFormInfo;
  ActionManager1.State := asSuspended;
  ActionManager2.State := asNormal;
  GBMenu.Enabled := False;
  GBMenu.Hide;
  gbOARank.Enabled := True;
  // Turn the Done Button on and make it visible
  LMDButOARankDone.Enabled := True;
  LMDButOARankDone.Visible := True;
  Add_Init;
  pFillOARankFields;
End;

Procedure TfrmOARank.sgOARankKeyPress(Sender: TObject; Var Key: Char);
Begin
  pMoveToPerRecord;
End;

Procedure TfrmOARank.ButPrevActionExecute(Sender: TObject);
Begin
  If sgOARank.Row > 1 Then sgOARank.Row := sgOARank.Row - 1;
  pMoveToPerRecord;
End;

Procedure TfrmOARank.ButTopActionExecute(Sender: TObject);
Begin
  If sgOARank.Row > 0 Then sgOARank.Row := 1
  Else
    MessageDlg('There are no Event Rank/Quals for this player....', mtInformation, [mbOK], 0);
End;

Procedure TfrmOARank.ButLastActionExecute(Sender: TObject);
Begin
  If sgOARank.Row > 0 Then sgOARank.Row := (sgOARank.RowCount - 1)
  Else
    MessageDlg('There are no Event Rank/Quals for this player....', mtInformation, [mbOK], 0);
End;

Procedure TfrmOARank.ButDeleteActionExecute(Sender: TObject);
Begin
  Delete_record(True, frmOARank.HelpContext);
  pFillOARankGrid;
End;

Procedure TfrmOARank.ButFindActionExecute(Sender: TObject);
Begin
  // --------------------------------------------------
  frmOARank.Enabled := False;
  ActionManager1.State := asSuspended;
  // --------------------------------------------------
  pGenericFind;
  // --------------------------------------------------
  frmOARank.Enabled := True;
  ActionManager1.State := asNormal;
  // --------------------------------------------------
End;

Procedure TfrmOARank.Action1Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmOARank.Action2Execute(Sender: TObject);
Begin
  pPrintForm(Self, ExtractFilePath(Application.ExeName));
End;

Procedure TfrmOARank.pOnFieldLastStep(bWasOnEnter: Boolean; oSender: TObject; sType: String);
Begin
  If UpperCase(sType) = 'S' Then
    pOSFieldAvail(bDoedit, (oSender As TOvcSimpleField))
  Else If UpperCase(sType) = 'P' Then
    pOPFieldAvail(bDoedit, (oSender As TOvcPictureField))
  Else If UpperCase(sType) = 'C' Then
    pOCBFieldAvail(bDoedit, (oSender As TOvcComboBox))
  Else If UpperCase(sType) = 'D' Then
    pODEFieldAvail(bDoedit, (oSender As TOvcDateEdit))
  Else {MessageDlg('No Procedure created for that type!', mtError, [mbOK], 0)};
  // -------------------------------------------------------------------------------------------------------
  If bDisplay_Rec Then Begin
    { Load all information to the appropriate field from the current record }
    pFillOARankFields;
  End;
  // -------------------------------------------------------------------------------------------------------
  If bWasOnEnter Then Begin
    If UpperCase(sType) = 'S' Then Begin
      If Not (oSender As TOvcSimpleField).TabStop Then keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'P') Then Begin
      If Not (oSender As TOvcPictureField).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'D') Then Begin
      If Not (oSender As TOvcDateEdit).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End
    Else If (UpperCase(sType) = 'C') Then Begin
      If Not (oSender As TOvcComboBox).TabStop Then
        keybd_event(VK_TAB, MapVirtualKey(VK_TAB, 0), 0, 0);
    End;
  End;
End;

Procedure TfrmOARank.pOSFieldAvail(bAvail: Boolean; oOSField: TOVCSimpleField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmOARank.pOPFieldAvail(bAvail: Boolean; oOSField: TOVCPictureField);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.Options := [efoCaretToEnd, efoReadOnly];
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.Options := [efoCaretToEnd];
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmOARank.pOCBFieldAvail(bAvail: Boolean; oOSField: TOVCComboBox);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmOARank.pODEFieldAvail(bAvail: Boolean; oOSField: TOVCDateEdit);
Begin
  If Not bAvail Then Begin
    { Disallow editing of the Field }
    oOSField.ReadOnly := True;
    oOSField.TabStop := False;
    //    oOSField.Color := clBtnShadow;
  End
  Else Begin
    { Allow editing of the Field }
    oOSField.ReadOnly := False;
    oOSField.TabStop := True;
    oOSField.Color := clWindow;
  End;
  Refresh;
End;

Procedure TfrmOARank.pResetFldAttrib;
Var
  iComponent: integer;
Begin
  With Self Do Begin
    For iComponent := 0 To ComponentCount - 1 Do Begin
      If Components[iComponent].ClassType = TOvcSimpleField Then
        pOSFieldAvail(True, (Components[iComponent] As TOvcSimpleField))
      Else If Components[iComponent].ClassType = TOvcPictureField Then
        pOPFieldAvail(True, (Components[iComponent] As TOvcPictureField))
      Else If Components[iComponent].ClassType = TOvcDateEdit Then
        pODEFieldAvail(True, (Components[iComponent] As TOvcDateEdit))
      Else If Components[iComponent].ClassType = TOvcComboBox Then
        pOCBFieldAvail(True, (Components[iComponent] As TOvcComboBox));
    End;
  End;
End;

Procedure TfrmOARank.OSFEventSessNoEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.SESSION_NO := Pad(OSFEventSessNo.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 4, OARANK.SESSION_NO, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFEventSessNoExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFEventSessNo.Text) >= 1) And (Ival(OSFEventSessNo.Text) <= 9)) Then Begin
        ErrBox('Session number must be between 1 and 9 inclusive', mc, 0);
        OSFEventSessNo.Text := '1';
        OSFEventSessNo.SetFocus;
      End
      Else Begin
        OARANK.SESSION_NO := Pad(OSFEventSessNo.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 4, OARANK.SESSION_NO, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.OSFSectionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.SECTION := Pad(OSFSection.Text,2);
  bDoEdit := CustomPLEdit(Scrno, 5, OARANK.SECTION, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFSectionExit(Sender: TObject);
Var
  sString: ShortString;
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      sString := OSFSection.Text;
      If Not (valid_section(sString)) Then Begin
        ErrBox('Invalid Section', mc, 0);
        OSFSection.Text := '';
        OSFSection.SetFocus;
      End
      Else Begin
        OARANK.SECTION := Pad(OSFSection.Text,2);
        bDoEdit := CustomPLEdit(Scrno, 5, OARANK.SECTION, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.OSFRankLowEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.POS_LOW := LeftPad(Trim(OSFRankLow.Text),4);
  bDoEdit := CustomPLEdit(Scrno, 6, OARANK.POS_LOW, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFRankLowExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFRankLow.Text) >= 0)) Then Begin
        ErrBox('Must not be negative', mc, 0);
        OSFRankLow.Text := '0';
        OSFRankLow.SetFocus;
      End
      Else Begin
        OARANK.POS_LOW := LeftPad(Trim(OSFRankLow.Text),4);
        bDoEdit := CustomPLEdit(Scrno, 6, OARANK.POS_LOW, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.OSFRankHighEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.POS_HIGH := LeftPad(Trim(OSFRankHigh.Text),4);
  bDoEdit := CustomPLEdit(Scrno, 7, OARANK.POS_HIGH, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFRankHighExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFRankHigh.Text) = 0) Or (Ival(OSFRankHigh.Text) >= Ival(OSFRankLow.Text))) Then Begin
        ErrBox('High rank must be zero or >= Low rank', mc, 0);
        OSFRankHigh.Text := '0';
        OSFRankHigh.SetFocus;
      End
      Else Begin
        OARANK.POS_HIGH := LeftPad(Trim(OSFRankHigh.Text),4);
        bDoEdit := CustomPLEdit(Scrno, 7, OARANK.POS_HIGH, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.OSFRankTypeEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.RANK_TYPE := Pad(OSFRankType.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 8, OARANK.RANK_TYPE, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFRankTypeExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (OSFRankType.Text[1] In [' ', 'O', 'S']) Then Begin
        ErrBox('Rank Type must be blank, (O)verall, or (S)ession', mc, 0);
        OSFRankType.Text := '';
        OSFRankType.SetFocus;
      End
      Else Begin
        OARANK.RANK_TYPE := Pad(OSFRankType.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 8, OARANK.RANK_TYPE, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.OSFStratumEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.STRATUM := Pad(OSFStratum.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 8, OARANK.STRATUM, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFStratumExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not (Ival(OSFStratum.Text) In [1, 2, 3]) Then Begin
        ErrBox('Stratum must be 1 for top stratum, 2 for second, 3 for lowest', mc, 0);
        OSFStratum.Text := '';
        OSFStratum.SetFocus;
      End
      Else Begin
        OARANK.STRATUM := Pad(OSFStratum.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 8, OARANK.STRATUM, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.OSFDirectionEnter(Sender: TObject);
Begin
  bDoEdit := True;
  OARANK.Direction := Pad(OSFDirection.Text,1);
  bDoEdit := CustomPLEdit(Scrno, 10, OARANK.Direction, True, bDisplay_Rec, sDataMode, Sender);
  pOnFieldLastStep(True, Sender, 'S');
End;

Procedure TfrmOARank.OSFDirectionExit(Sender: TObject);
Begin
  If bDoEdit Then Begin
    If Not bEscKeyUsed Then Begin
      If Not ((Ival(OSFDirection.Text) In [1, 2, 3, 4])) Then Begin
        ErrBox('Direction must be 1, 2, 3, or 4', mc, 0);
        OSFDirection.Text := '1';
        OSFDirection.SetFocus;
      End
      Else Begin
        OARANK.Direction := Pad(OSFDirection.Text,1);
        bDoEdit := CustomPLEdit(Scrno, 10, OARANK.Direction, False, bDisplay_Rec, sDataMode, Sender);
        pOnFieldLastStep(False, Sender, 'S');
      End;
    End;
  End;
End;

Procedure TfrmOARank.ButtonF1KeyClick(Sender: TObject);
Begin
  If HelpContext > 0 Then keybd_event(VK_F1, MapVirtualKey(VK_F1, 0), 0, 0);
End;

Function TfrmOARank.FormHelp(Command: Word; Data: Integer; Var CallHelp: Boolean): Boolean;
Begin
  if data < 500000 then
    HtmlHelp(GetDesktopWindow,DefChmFile,HH_HELP_CONTEXT,Data);
//  If data < 500000 Then WinHelp(Application.Handle, 'ACBLSCORE.HLP', Help_Context, Data);
End;

End.

