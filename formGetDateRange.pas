Unit formGetDateRange;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ovcbase, ovceditf, ovcedpop, ovcedcal, ResizeKit, LibData;

Type
  TfrmGetDateRange = Class(TForm)
    GBDateRange: TGroupBox;
    ResizeKit1: TResizeKit;
    OvcDateEdit1: TOvcDateEdit;
    OvcDateEdit2: TOvcDateEdit;
    OvcDateEdit2Label1: TOvcAttachedLabel;
    OvcDateEdit1Label1: TOvcAttachedLabel;
    OvcController1: TOvcController;
    Button1: TButton;
    Procedure OvcDateEdit1Exit(Sender: TObject);
    Procedure OvcDateEdit2Exit(Sender: TObject);
    Procedure pKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
    Procedure pKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  End;

Var
  frmGetDateRange: TfrmGetDateRange;

Implementation

Uses formAttendance;

{$R *.dfm}

Procedure TfrmGetDateRange.OvcDateEdit1Exit(Sender: TObject);
Var
  sDateText: String;
  iDateText: Integer;
Begin
  sDateText := OvcDateEdit1.Text;
  iDateText := Length(Trim(OvcDateEdit1.Text));
  { Use the length of 9 since the Opheus Date Field will make the first character a blank instead
    of a zero. Example:  " 4/ 6/2005" }
  If Length(Trim(sDateText)) < 9 Then Begin
    OvcDateEdit1.Text := '';
    OvcDateEdit2.Text := '';
    frmAttendance.iStartDate := 0;
    frmAttendance.iEndDate := 0;
    Close;
  End
  Else Begin
    OvcDateEdit1.Text := fFillDate(sDateText);
    If Length(Trim(OvcDateEdit2.Text)) < 9 Then
      OvcDateEdit2.Text := DateToStr(StrToDate(OvcDateEdit1.Text) + 5);
    frmAttendance.iStartDate := OvcDateEdit1.Date;
  End;
End;

Procedure TfrmGetDateRange.OvcDateEdit2Exit(Sender: TObject);
Var
  sDateText: String;
  iDateText: Integer;
Begin
  sDateText := OvcDateEdit2.Text;
  iDateText := Length(Trim(OvcDateEdit2.Text));
  { Use the length of 9 since the Opheus Date Field will make the first character a blank instead
    of a zero. Example:  " 4/ 6/2005" }
  If Length(Trim(OvcDateEdit2.Text)) < 9 Then Begin
    OvcDateEdit2.Text := '';
    frmAttendance.iStartDate := 0;
    frmAttendance.iEndDate := 0;
    Close;
  End
  Else Begin
    OvcDateEdit2.Text := fFillDate(sDateText);
    frmAttendance.iEndDate := OvcDateEdit2.Date;
  End;
End;

Procedure TfrmGetDateRange.pKeyDown(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If ((Key = VK_ESCAPE) Or (Key = VK_F9)) Then Close;
End;

Procedure TfrmGetDateRange.pKeyUp(Sender: TObject; Var Key: Word; Shift: TShiftState);
Begin
  If ((Key = VK_ESCAPE) Or (Key = VK_F9)) Then Close;
End;

procedure TfrmGetDateRange.Button1Click(Sender: TObject);
begin
  Close;
end;

End.

