unit formMergeNonMembers;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, LMDRTFBase, LMDRTFLabel, StdCtrls, LMDCustomListBox,
  LMDCustomImageListBox, LMDCustomCheckListBox, LMDCheckListBox, ResizeKit,
  LMDCustomButton, LMDButton;

type
  TfrmMergeNonMembers = class(TForm)
    LMDCheckListBox1: TLMDCheckListBox;
    LMDRichLabel1: TLMDRichLabel;
    ResizeKit1: TResizeKit;
    LMDButPlayerInfoDone: TLMDButton;
    procedure LMDButPlayerInfoDoneClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMergeNonMembers: TfrmMergeNonMembers;

implementation

{$R *.dfm}

procedure TfrmMergeNonMembers.LMDButPlayerInfoDoneClick(Sender: TObject);
begin
  Close;
end;

end.
