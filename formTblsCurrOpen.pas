Unit formTblsCurrOpen;

Interface

Uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ResizeKit, LMDGroupBox, StdCtrls;

Type
  TfrmTblsCurrOpen = Class(TForm)
    ResizeKit1: TResizeKit;
    Memo1: TMemo;
    Procedure FormActivate(Sender: TObject);
    Procedure FormKeyDown(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    Procedure FormKeyUp(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    Procedure Memo1KeyDown(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
    Procedure Memo1KeyUp(Sender: TObject; Var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  End;

Var
  frmTblsCurrOpen: TfrmTblsCurrOpen;

Implementation

Uses Dbase, formPlayers;

{$R *.dfm}

Procedure TfrmTblsCurrOpen.FormActivate(Sender: TObject);
Var
  iForLoop: Integer;
  sToAdd: String;
Begin
  { Display a list of all open tables. }
  {
    Tables are opened by there assigned table numbers
    Their order is defined in DBNames.Dcl as:
      Playern_no      = 1;
      Playergr_no     = 2;
      Tourn_no        = 3;
      Tournev_no      = 4;
      ClubDef_no      = 5;
      ClubGame_no     = 6;
      Attend_no       = 7;
      Percent_no      = 8;
      ClubSanc_no     = 9;
      OArank_no       =10;
      PlChange_no     =11;

    dbase.DBFileOpen is an boolean array of which tables are open.
    dbase include file dbnames.dcl creates the array DBNames that contains the table names and
    indexes
  }
  ResizeKit1.Enabled := False;
  { Clear any items in the Memo }
  Memo1.Lines.Clear;
  Memo1.Lines.Add('[ Tables Currently Opened ]');
  Memo1.Lines.Add('');
  Memo1.Alignment := taLeftJustify;
  { Fill the Memo with the names of the open tables }
  For iForLoop := 1 To iLenArray Do Begin
    If dbase.DBFileOpen[iForLoop] Then Begin
      Memo1.Lines.Add(dbase.DBNames[iForLoop, 0]);
    End;
  End;
  ResizeKit1.Enabled := True;
End;

Procedure TfrmTblsCurrOpen.FormKeyDown(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

Procedure TfrmTblsCurrOpen.FormKeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

Procedure TfrmTblsCurrOpen.Memo1KeyDown(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

Procedure TfrmTblsCurrOpen.Memo1KeyUp(Sender: TObject; Var Key: Word;
  Shift: TShiftState);
Begin
  If Key = VK_ESCAPE Then Close;
End;

End.

